import { Http, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Location } from '@angular/common';
import { of, throwError } from 'rxjs';
import { AppDecryptMe } from './app.DecryptMe';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class CallServiceFunction {
  ip: string;
  errorMsg: string;
  constructor(
    public http: Http,
    private httpget: HttpClient,
    public decryptMe: AppDecryptMe,
    public router: Router,
    private location: Location
  ) {
    // this.ip = 'localhost';
    // this.ip = "10.10.64.180";
    // this.ip = '10.10.64.147';
    this.ip = '10.10.62.34';
    // this.ip = "localhost";
    // console.log(this.ip);
  }

  private getServerErrorMessage(error: HttpErrorResponse): string {
    // console.log(JSON.stringify(error));
    switch (error.status) {
      case 404: {
        return `Not Found: ${error.status}`;
      }
      case 403: {
        return `Access Denied: ${error.status}`;
      }
      case 500: {
        return `Internal Server Error: ${error.status}`;
      }
      case 0: {
        return `invalid token : ${error.status}`;
      }
      default: {
        return `Unknown Server Error: ${error.status}`;
      }
    }
  }

  getData(type): Observable<any> {
    let accessToken;
    if (
      sessionStorage.getItem('ekrptd1') != null &&
      sessionStorage.getItem('ekrtdp2') != null &&
      sessionStorage.getItem('usdt') != null
    ) {
      var aesDecryptIV = this.decryptMe.aesDecryptIV(
        sessionStorage.getItem('ekrptd1')
      );
      var aesDecryptSalt = this.decryptMe.aesDecryptSalt(
        sessionStorage.getItem('ekrtdp2')
      );
      var aesDecrypt = this.decryptMe.aesDecrypt(
        sessionStorage.getItem('usdt'),
        aesDecryptIV,
        aesDecryptSalt
      );
      accessToken = JSON.parse(aesDecrypt).access_token;
    }
    console.log('get barer : ' + accessToken);
    const url = 'http://' + this.ip + ':8881/bvnextgen/api/';
    var header = {
      headers: new HttpHeaders().set('Authorization', `Bearer ` + accessToken),
    };
    // console.log(url + type +" "+accessToken);

    return this.httpget.get<any>(url + type, header).pipe(
      catchError((error) => {
        let errorMsg: string;
        if (error.error instanceof ErrorEvent) {
          this.errorMsg = `Error: ${error.message}`;
        } else {
          this.errorMsg = this.getServerErrorMessage(error);
        }
        console.log('get method : ' + this.errorMsg);
        if (this.errorMsg == 'invalid token : 0') {
          sessionStorage.clear();
          this.router.navigateByUrl('/');
          return throwError(errorMsg);
        }
      })
    );
  }

  postData(jsonInput): Observable<any> {
    let accessToken = '';
    if (
      sessionStorage.getItem('ekrptd1') != null &&
      sessionStorage.getItem('ekrtdp2') != null &&
      sessionStorage.getItem('usdt') != null
    ) {
      var aesDecryptIV = this.decryptMe.aesDecryptIV(
        sessionStorage.getItem('ekrptd1')
      );
      var aesDecryptSalt = this.decryptMe.aesDecryptSalt(
        sessionStorage.getItem('ekrtdp2')
      );
      var aesDecrypt = this.decryptMe.aesDecrypt(
        sessionStorage.getItem('usdt'),
        aesDecryptIV,
        aesDecryptSalt
      );
      accessToken = JSON.parse(aesDecrypt).access_token;
    }
    // console.log(accessToken);
    let apiName = jsonInput.url;
    // jsonInput["randomString"] =  ""+sessionStorage.getItem("randomString");
    let body = JSON.stringify(jsonInput);

    // console.log("body utama "+ body);

    let headers = new Headers({
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: 'Bearer ' + accessToken,
    });

    let options = new RequestOptions({
      headers: headers,
      withCredentials: true,
    });
    // console.log("body utama "+ body );
    //return this.http.post('http://10.10.68.32:8881/bvnextgen/api/' + apiName, body2, options);
    return this.http
      .post(
        'http://' + this.ip + ':8881/bvnextgen/api/' + apiName,
        body,
        options
      )
      .pipe(
        catchError((error) => {
          let errorMsg: string;
          if (error.error instanceof ErrorEvent) {
            this.errorMsg = `Error: ${error.message}`;
          } else {
            this.errorMsg = this.getServerErrorMessage(error);
          }
          console.log('post method : ' + this.errorMsg);
          if (this.errorMsg == 'invalid token : 0') {
            sessionStorage.clear();
            this.router.navigateByUrl('/');
            return throwError(errorMsg);
          }
        })
      );
  }

  putData(jsonInput): Observable<any> {
    let accessToken;
    if (
      sessionStorage.getItem('ekrptd1') != null &&
      sessionStorage.getItem('ekrtdp2') != null &&
      sessionStorage.getItem('usdt') != null
    ) {
      var aesDecryptIV = this.decryptMe.aesDecryptIV(
        sessionStorage.getItem('ekrptd1')
      );
      var aesDecryptSalt = this.decryptMe.aesDecryptSalt(
        sessionStorage.getItem('ekrtdp2')
      );
      var aesDecrypt = this.decryptMe.aesDecrypt(
        sessionStorage.getItem('usdt'),
        aesDecryptIV,
        aesDecryptSalt
      );
      accessToken = JSON.parse(aesDecrypt).access_token;
    }
    let apiName = jsonInput.url;
    // jsonInput["randomString"] =  ""+sessionStorage.getItem("randomString");
    let body = JSON.stringify(jsonInput);

    // console.log("body utama " + body);

    let headers = new Headers({
      'Content-Type': 'application/json',
      Accept: 'application/json',
      Authorization: 'Bearer ' + accessToken,
    });

    let options = new RequestOptions({
      headers: headers,
      withCredentials: true,
    });

    //return this.http.post('http://10.10.68.32:8881/bvnextgen/api/' + apiName, body2, options);
    return this.http
      .put(
        'http://' + this.ip + ':8881/bvnextgen/api/' + apiName,
        body,
        options
      )
      .pipe(
        catchError((error) => {
          let errorMsg: string;
          if (error.error instanceof ErrorEvent) {
            this.errorMsg = `Error: ${error.message}`;
          } else {
            this.errorMsg = this.getServerErrorMessage(error);
          }
          console.log('put method : ' + this.errorMsg);
          if (this.errorMsg == 'invalid token : 0') {
            sessionStorage.clear();
            this.router.navigateByUrl('/');
            return throwError(errorMsg);
          }
        })
      );
  }

  getRestResultLogin(jsonInput): Observable<any> {
    let apiName = jsonInput.url;
    let body = JSON.stringify(jsonInput);

    let headers = new Headers({
      'Content-Type': 'application/json',
      Accept: 'application/json',
      withCredentials: true,
    });

    let options = new RequestOptions({
      headers: headers,
    });

    return this.http
      .post(
        'http://' + this.ip + ':8881/bvnextgen/api/' + apiName,
        body,
        options
      )
      .pipe(
        catchError((error) => {
          let errorMsg: string;
          if (error.error instanceof ErrorEvent) {
            this.errorMsg = `Error: ${error.message}`;
          } else {
            this.errorMsg = this.getServerErrorMessage(error);
          }
          console.log('post login method : ' + this.errorMsg);
          if (this.errorMsg == 'invalid token : 0') {
            sessionStorage.clear();
            this.router.navigateByUrl('/');
            return throwError(errorMsg);
          }
        })
      );
  }

  getRestUpload(formData: FormData, url): Observable<any> {
    //token
    let accessToken;
    if (
      sessionStorage.getItem('ekrptd1') != null &&
      sessionStorage.getItem('ekrtdp2') != null &&
      sessionStorage.getItem('usdt') != null
    ) {
      var aesDecryptIV = this.decryptMe.aesDecryptIV(
        sessionStorage.getItem('ekrptd1')
      );
      var aesDecryptSalt = this.decryptMe.aesDecryptSalt(
        sessionStorage.getItem('ekrtdp2')
      );
      var aesDecrypt = this.decryptMe.aesDecrypt(
        sessionStorage.getItem('usdt'),
        aesDecryptIV,
        aesDecryptSalt
      );
      accessToken = JSON.parse(aesDecrypt).access_token;
    }

    let apiName = url;

    let headers = new Headers({
      Accept: 'application/json',
      Authorization: 'Bearer ' + accessToken,
    });

    let options = new RequestOptions({
      headers: headers,
      withCredentials: true,
    });

    return this.http
      .post(
        'http://' + this.ip + ':8881/bvnextgen/api/' + apiName,
        formData,
        options
      )
      .pipe(
        catchError((error) => {
          let errorMsg: string;
          if (error.error instanceof ErrorEvent) {
            this.errorMsg = `Error: ${error.message}`;
          } else {
            this.errorMsg = this.getServerErrorMessage(error);
          }
          console.log('post upload method : ' + this.errorMsg);
          if (this.errorMsg == 'invalid token : 0') {
            sessionStorage.clear();
            this.router.navigateByUrl('/');
            return throwError(errorMsg);
          }
        })
      );
  }
}
