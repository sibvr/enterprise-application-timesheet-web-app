import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { NavbarSideComponent } from './navbar-side/navbar-side.component';
import { DailyTaskComponent } from './daily-task/daily-task.component';
import { WeeklyTaskComponent } from './weekly-task/weekly-task.component';
import { CreateDailyTaskComponent } from './daily-task/create-daily-task/create-daily-task.component';
import { MyTeamComponent } from './my-team/my-team.component';
import { WeeklyTeamComponent } from './my-team/weekly-team/weekly-team.component';
import { CreateTeamComponent } from './admin-page/create-team/create-team.component';
import { CreateProjectComponent } from './admin-page/create-project/create-project.component';
import { InquiryUserManagementComponent } from './user-management/user-management/inquiry-user-management/inquiry-user-management.component';
import { InquiryRoleManagementComponent } from './user-management/role-management/inquiry-role-management/inquiry-role-management.component';
import { CreateRoleManagementComponent } from './user-management/role-management/create-role-management/create-role-management.component';
import { UpdateRoleManagementComponent } from './user-management/role-management/update-role-management/update-role-management.component';
import { SettingsComponent } from './user-management/settings/settings.component';
import { ChangePasswordComponent } from './user-management/change-password/change-password.component';
import { DashboardManagerComponent } from './dashboard-manager/dashboard-manager.component';
import { HolidayCalendarComponent } from './admin-page/holiday-calendar/holiday-calendar.component';
import { WeeklyCalendarComponent } from './admin-page/weekly-calendar/weekly-calendar.component';
import { ConfigurationComponent } from './admin-page/configuration/configuration.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
    outlet: 'login',
  },

  {
    path: 'dailytask',
    children: [
      { path: '', component: DailyTaskComponent },
      { path: '', component: HeaderComponent, outlet: 'header' },
      { path: '', component: NavbarSideComponent, outlet: 'navbarside' },
      { path: '', component: FooterComponent, outlet: 'footer' },
    ],
  },

  {
    path: 'createdailytask',
    children: [
      { path: '', component: CreateDailyTaskComponent },
      { path: '', component: HeaderComponent, outlet: 'header' },
      { path: '', component: NavbarSideComponent, outlet: 'navbarside' },
      { path: '', component: FooterComponent, outlet: 'footer' },
    ],
  },

  {
    path: 'weeklytask',
    children: [
      { path: '', component: WeeklyTaskComponent },
      { path: '', component: HeaderComponent, outlet: 'header' },
      { path: '', component: NavbarSideComponent, outlet: 'navbarside' },
      { path: '', component: FooterComponent, outlet: 'footer' },
    ],
  },
  {
    path: 'myteam',
    children: [
      { path: '', component: MyTeamComponent },
      { path: '', component: HeaderComponent, outlet: 'header' },
      { path: '', component: NavbarSideComponent, outlet: 'navbarside' },
      { path: '', component: FooterComponent, outlet: 'footer' },
    ],
  },
  {
    path: 'weeklyteam/:idMember/:fullname',
    children: [
      { path: '', component: WeeklyTeamComponent },
      { path: '', component: HeaderComponent, outlet: 'header' },
      { path: '', component: NavbarSideComponent, outlet: 'navbarside' },
      { path: '', component: FooterComponent, outlet: 'footer' },
    ],
  },
  {
    path: 'createteam',
    children: [
      { path: '', component: CreateTeamComponent },
      { path: '', component: HeaderComponent, outlet: 'header' },
      { path: '', component: NavbarSideComponent, outlet: 'navbarside' },
      { path: '', component: FooterComponent, outlet: 'footer' },
    ],
  },
  {
    path: 'createproject',
    children: [
      { path: '', component: CreateProjectComponent },
      { path: '', component: HeaderComponent, outlet: 'header' },
      { path: '', component: NavbarSideComponent, outlet: 'navbarside' },
      { path: '', component: FooterComponent, outlet: 'footer' },
    ],
  },
  {
    path: 'usermanagement',
    children: [
      { path: '', component: InquiryUserManagementComponent },
      { path: '', component: HeaderComponent, outlet: 'header' },
      { path: '', component: NavbarSideComponent, outlet: 'navbarside' },
      { path: '', component: FooterComponent, outlet: 'footer' },
    ],
  },
  // {
  //   path: 'usermanagement', children: [
  //     { path: '', component: InquiryUserManagementComponent },
  //     { path: '', component: HeaderComponent, outlet: 'header' },
  //     { path: '', component: NavbarSideComponent, outlet: 'navbarside' },
  //     { path: '', component: FooterComponent, outlet: 'footer' }
  //   ]
  // },
  {
    path: 'configuration',
    children: [
      { path: '', component: ConfigurationComponent },
      { path: '', component: HeaderComponent, outlet: 'header' },
      { path: '', component: NavbarSideComponent, outlet: 'navbarside' },
      { path: '', component: FooterComponent, outlet: 'footer' },
    ],
  },
  {
    path: 'rolemanagementcreate',
    children: [
      { path: '', component: CreateRoleManagementComponent },
      { path: '', component: HeaderComponent, outlet: 'header' },
      { path: '', component: NavbarSideComponent, outlet: 'navbarside' },
      { path: '', component: FooterComponent, outlet: 'footer' },
    ],
  },
  {
    path: 'rolemanagementupdate/:roleId',
    children: [
      { path: '', component: UpdateRoleManagementComponent },
      { path: '', component: HeaderComponent, outlet: 'header' },
      { path: '', component: NavbarSideComponent, outlet: 'navbarside' },
      { path: '', component: FooterComponent, outlet: 'footer' },
    ],
  },
  {
    path: 'settings',
    children: [
      { path: '', component: SettingsComponent },
      { path: '', component: HeaderComponent, outlet: 'header' },
      { path: '', component: NavbarSideComponent, outlet: 'navbarside' },
      { path: '', component: FooterComponent, outlet: 'footer' },
    ],
  },
  {
    path: 'changepassword',
    children: [
      { path: '', component: ChangePasswordComponent },
      { path: '', component: HeaderComponent, outlet: 'header' },
      { path: '', component: NavbarSideComponent, outlet: 'navbarside' },
      { path: '', component: FooterComponent, outlet: 'footer' },
    ],
  },
  {
    path: 'dasboardmanager',
    children: [
      { path: '', component: DashboardManagerComponent },
      { path: '', component: HeaderComponent, outlet: 'header' },
      { path: '', component: NavbarSideComponent, outlet: 'navbarside' },
      { path: '', component: FooterComponent, outlet: 'footer' },
    ],
  },
  {
    path: 'holidaycalendar',
    children: [
      { path: '', component: HolidayCalendarComponent },
      { path: '', component: HeaderComponent, outlet: 'header' },
      { path: '', component: NavbarSideComponent, outlet: 'navbarside' },
      { path: '', component: FooterComponent, outlet: 'footer' },
    ],
  },
  {
    path: 'weeklycalendar',
    children: [
      { path: '', component: WeeklyCalendarComponent },
      { path: '', component: HeaderComponent, outlet: 'header' },
      { path: '', component: NavbarSideComponent, outlet: 'navbarside' },
      { path: '', component: FooterComponent, outlet: 'footer' },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
