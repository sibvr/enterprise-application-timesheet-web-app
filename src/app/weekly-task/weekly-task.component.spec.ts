import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { WeeklyTaskComponent } from './weekly-task.component';

describe('WeeklyTaskComponent', () => {
  let component: WeeklyTaskComponent;
  let fixture: ComponentFixture<WeeklyTaskComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ WeeklyTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeeklyTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
