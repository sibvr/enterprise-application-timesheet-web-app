import { Component, OnInit, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, RequestOptions } from '@angular/http';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { AppDecryptMe } from '../app.DecryptMe';
import { Observable } from 'rxjs';
import { CallServiceFunction } from './../callServiceFunction';
import { DatePipe } from '@angular/common';
import { CekCredential } from '../cekcredential';
import { printWeeklyxls } from './print-weekly-xls';
import { CheckusercredService } from '../checkusercred.service';

declare function showSwal(type, title, text): any;
declare var $: any;

@Component({
  selector: 'app-weekly-task',
  templateUrl: './weekly-task.component.html',
  styleUrls: ['./weekly-task.component.css'],
  providers: [DatePipe],
})
export class WeeklyTaskComponent implements OnInit {
  constructor(
    private cekCredential: CekCredential,
    public router: Router,
    public http: Http,
    public checkUserCred: CheckusercredService,
    private datePipe: DatePipe,
    private callService: CallServiceFunction,
    public decryptMe: AppDecryptMe,
    private printWeeklyxls: printWeeklyxls
  ) {}

  weeklyTaskForm = new FormGroup({
    week: new FormControl('1', [Validators.required]),
    period: new FormControl('', [Validators.required]),
  });

  userId: any;
  today: any;
  loadingData: boolean;
  loadingDetail: boolean;
  getReportService: any;
  loadingExport = false;
  options: any = {};
  dateRageToday = '';
  ngOnInit(): void {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        this.getReportService = {};
        //get user creed
        var aesDecryptIV = this.decryptMe.aesDecryptIV(
          sessionStorage.getItem('ekrptd1')
        );
        var aesDecryptSalt = this.decryptMe.aesDecryptSalt(
          sessionStorage.getItem('ekrtdp2')
        );
        var aesDecrypt = this.decryptMe.aesDecrypt(
          sessionStorage.getItem('usdt'),
          aesDecryptIV,
          aesDecryptSalt
        );

        var usercred = aesDecrypt;
        this.getReportService = JSON.parse(
          this.checkUserCred.checkUserCred(usercred, 'getReportService')
        );

        // console.log("usercreed : "+JSON.stringify(usercred) );
        this.getReportService = JSON.parse(
          this.checkUserCred.checkUserCred(usercred, 'getReportService')
        );
        // console.log("getReportService :  "+JSON.stringify(this.getReportService) );

        $('#datepicker-popup2').datepicker({
          format: 'mm-yyyy',
          viewMode: 'months',
          autoclose: true,
          minViewMode: 'months',
        });

        var aesDecryptIV = this.decryptMe.aesDecryptIV(
          sessionStorage.getItem('ekrptd1')
        );
        var aesDecryptSalt = this.decryptMe.aesDecryptSalt(
          sessionStorage.getItem('ekrtdp2')
        );
        var aesDecrypt = this.decryptMe.aesDecrypt(
          sessionStorage.getItem('usdt'),
          aesDecryptIV,
          aesDecryptSalt
        );
        this.userId = JSON.parse(aesDecrypt).userId;

        let date = new Date();
        let month = date.getMonth();
        let day = date.getDate();
        var sekarang = new Date(date.getFullYear(), month, day);
        this.dateRageToday =
          this.datePipe.transform(sekarang, 'yyyyMMdd') +
          '-' +
          this.datePipe.transform(sekarang, 'yyyyMMdd');
        this.today = this.datePipe.transform(sekarang, 'yyyy/MM/dd');

        //daterange
        this.options = {
          locale: { format: 'YYYY/MM/DD' },
          alwaysShowCalendars: false,
          showDropdowns: true,
          startDate: this.today,
          endDate: this.today,
          opens: 'left',
          drops: 'auto',
          buttonClasses: 'btn btn-lg',
          cancelClass: 'btn-danger',
        };

        //set value json date range
        this.dateRangeExport.start = this.datePipe.transform(
          sekarang,
          'yyyyMMdd'
        );
        this.dateRangeExport.end = this.datePipe.transform(
          sekarang,
          'yyyyMMdd'
        );
      }
    });
  }

  //date range
  dateRangeExport: any = {};
  public selectedDateExport(value: any, datepicker?: any) {
    datepicker.start = value.start;
    datepicker.end = value.end;

    this.dateRangeExport.start = this.datePipe.transform(
      this.dateRangeExport.start,
      'yyyyMMdd'
    );
    this.dateRangeExport.end = this.datePipe.transform(
      this.dateRangeExport.end,
      'yyyyMMdd'
    );

    // console.log("dateRangeExport start : " + this.dateRangeExport.start);
    // console.log("dateRangeExport end : " + this.dateRangeExport.end);
  }
  public calendarCanceledExport(e: any) {
    // console.log(e);
    // e.event
    // e.picker
  }

  drFilterExport = '';
  public applyDateExport(e: any) {
    this.drFilterExport =
      this.dateRangeExport.start + '-' + this.dateRangeExport.end;
  }

  dataExcel = {
    pid: [],
    summaryRekap: [],
    rekapBiweekly: [],
  };
  printWeekly() {
    this.loadingExport = true;

    //PID
    let serviceReturn: any;
    let statusService: string;

    let url = 'getProjectService';
    let params = '?userId=' + this.userId;

    this.callService.getData(url + params).subscribe(
      (res) => {
        serviceReturn = res;
        statusService = serviceReturn['0'].serviceContent.status;

        console.log('serviceReturn getProjectService: ' + statusService);
        console.log(
          'length 1 :' + serviceReturn['0'].serviceContent.serviceOutput.length
        );
        // console.log(serviceReturn)
        let excelPid = [];
        if (
          statusService == 'S' &&
          serviceReturn['0'].serviceContent.count > 0
        ) {
          if (serviceReturn['0'].serviceContent.serviceOutput.length > 0) {
            for (
              let i = 0;
              i < serviceReturn['0'].serviceContent.serviceOutput.length;
              i++
            ) {
              let data: any[] = [];
              data.push(serviceReturn['0'].serviceContent.serviceOutput[i].pid);
              data.push(
                serviceReturn['0'].serviceContent.serviceOutput[i].projectName
              );
              data.push(
                serviceReturn['0'].serviceContent.serviceOutput[i].category
              );
              data.push(
                serviceReturn['0'].serviceContent.serviceOutput[i].bill
              );
              data.push(
                serviceReturn['0'].serviceContent.serviceOutput[i].costumer
              );
              if (
                serviceReturn['0'].serviceContent.serviceOutput[i].hrCost ==
                null
              ) {
                data.push('');
              } else {
                data.push(
                  serviceReturn['0'].serviceContent.serviceOutput[i].hrCost
                );
              }
              if (
                serviceReturn['0'].serviceContent.serviceOutput[i]
                  .remainHRCost == null
              ) {
                data.push('');
              } else {
                data.push(
                  serviceReturn['0'].serviceContent.serviceOutput[i]
                    .remainHRCost
                );
              }
              data.push(
                serviceReturn['0'].serviceContent.serviceOutput[i].statusProject
              );
              data.push('');

              excelPid.push(data);
            }
          }

          // console.log(excelPid.toString());
          this.dataExcel['pid'] = excelPid;
          this.getSummaryRekap();
        } else {
          //console.log("masuk else printWeekly");
          showSwal(
            'warning',
            'Export Weekly',
            serviceReturn['0'].serviceContent.errorMessage
          );
          this.loadingExport = false;
        }
      },
      (err) => this.router.navigateByUrl('/error500')
    );
  }

  //get Summary Rekap
  getSummaryRekap() {
    let serviceReturn: any;
    let statusService: string;
    let dateRangeFilter = '';
    console.info(this.drFilterExport);
    if (this.drFilterExport == '') {
      dateRangeFilter = this.dateRageToday;
    } else {
      dateRangeFilter = this.drFilterExport;
    }

    let url = 'getSumReportService';
    let params =
      '?userId=' +
      this.userId +
      '&byTeam=' +
      this.userId +
      '&byDate=' +
      dateRangeFilter;
    console.log('param getSumReportService : ' + params);

    this.callService.getData(url + params).subscribe(
      (res) => {
        serviceReturn = res;
        statusService = serviceReturn['0'].serviceContent.status;

        console.log(
          'serviceReturn getSumReportService: ' +
            serviceReturn['0'].serviceContent
        );
        // console.log("length 2 :" + serviceReturn["0"].serviceContent.serviceOutput.length);

        let listType = serviceReturn['0'].serviceContent.listCate;
        let header = [];
        // console.log("list dinsmis : "+listType);

        let excelSummaryRekap = [];
        if (
          statusService == 'S' &&
          serviceReturn['0'].serviceContent.count > 0
        ) {
          if (
            serviceReturn['0'].serviceContent.serviceOutput.length > 0 &&
            listType.length > 0
          ) {
            //header
            for (let j = 0; j < listType.length; j++) {
              if (listType[j] != 'grandTotal') {
                header.push(listType[j]);
              }
            }

            for (
              let i = 0;
              i < serviceReturn['0'].serviceContent.serviceOutput.length;
              i++
            ) {
              let data: any[] = [];
              data.push(
                serviceReturn['0'].serviceContent.serviceOutput[i].fullname
              );

              //dynamic
              for (let e = 0; e < listType.length; e++) {
                let key = listType[e];
                // console.log("push key : "+key);
                if (key != 'grandTotal') {
                  data.push(
                    serviceReturn['0'].serviceContent.serviceOutput[i][key]
                  );
                }
              }

              // data.push(" ");
              // data.push(" ");
              data.push(
                serviceReturn['0'].serviceContent.serviceOutput[i].grandTotal
              );

              excelSummaryRekap.push(data);
            }
          }

          this.dataExcel['summaryRekap'] = excelSummaryRekap;
          this.dataExcel['summaryRekapHeader'] = header;
          this.getRekapBiweekly();
        } else {
          // console.log("masuk else getSummaryRekap");
          showSwal(
            'warning',
            'Export Weekly',
            serviceReturn['0'].serviceContent.errorMessage
          );
          this.loadingExport = false;
        }
      },
      (err) => this.router.navigateByUrl('/error500')
    );
  }

  //get Rekap Biweekly
  getRekapBiweekly() {
    let serviceReturn: any;
    let statusService: string;

    let dateRangeFilter = '';
    if (this.drFilterExport == '') {
      dateRangeFilter = this.dateRageToday;
    } else {
      dateRangeFilter = this.drFilterExport;
    }

    let url = 'getDetailReportService';
    let params =
      '?userId=' +
      this.userId +
      '&byTeam=' +
      this.userId +
      '&byDate=' +
      dateRangeFilter;
    // console.log("param : "+params);

    this.callService.getData(url + params).subscribe(
      (res) => {
        serviceReturn = res;
        statusService = serviceReturn['0'].serviceContent.status;

        // console.log("serviceReturn getDetailReportService: "+JSON.stringify(serviceReturn) );
        // console.log("length 3 :" + serviceReturn["0"].serviceContent.serviceOutput.length);

        let excelRekapBiweekly = [];
        if (
          statusService == 'S' &&
          serviceReturn['0'].serviceContent.count > 0
        ) {
          if (serviceReturn['0'].serviceContent.serviceOutput.length > 0) {
            for (
              let i = 0;
              i < serviceReturn['0'].serviceContent.serviceOutput.length;
              i++
            ) {
              let data: any[] = [];
              data.push(
                serviceReturn['0'].serviceContent.serviceOutput[i].fullname
              );
              data.push(serviceReturn['0'].serviceContent.serviceOutput[i].pid);
              data.push(
                serviceReturn['0'].serviceContent.serviceOutput[i].projectName
              );
              data.push(
                serviceReturn['0'].serviceContent.serviceOutput[i].taskDetail
              );
              data.push(' ');
              data.push(
                serviceReturn['0'].serviceContent.serviceOutput[i].period
              );
              data.push(
                serviceReturn['0'].serviceContent.serviceOutput[i].sumActual
              );
              data.push(
                serviceReturn['0'].serviceContent.serviceOutput[i]
                  .categoryProject
              );
              data.push(
                serviceReturn['0'].serviceContent.serviceOutput[i].billProject
              );
              data.push(' ');
              data.push(
                serviceReturn['0'].serviceContent.serviceOutput[i].statusProject
              );

              excelRekapBiweekly.push(data);
            }
          }

          // console.log(excelRekapBiweekly.toString());
          this.dataExcel['rekapBiweekly'] = excelRekapBiweekly;

          //name+date file
          let splittedDate = dateRangeFilter.split('-', 2);

          this.printWeeklyxls.generateExcel(
            this.dataExcel,
            'BV Rekap Biwikly_' + splittedDate[0] + '_' + splittedDate[1] + '_'
          );

          this.loadingExport = false;
        } else {
          // console.log("masuk else getRekapBiweekly");
          showSwal(
            'warning',
            'Export Weekly',
            serviceReturn['0'].serviceContent.errorMessage
          );
          this.loadingExport = false;
        }
      },
      (err) => this.router.navigateByUrl('/error500')
    );
  }

  flagPeriod: boolean;
  searchWeeklyTask() {
    this.loadingData = true;
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        this.flagPeriod = false;
        let flag = false;

        if (!$('#periodeSearch').first().val()) {
          // console.log("error period");
          flag = true;
          this.flagPeriod = true;
        }

        if (this.weeklyTaskForm.controls['week'].invalid) {
          flag = true;
        }

        if (!flag) {
          // console.log("submit");
          var week = this.weeklyTaskForm.controls['week'].value;

          var dateMonth1 = $('#periodeSearch')
            .first()
            .val()
            .toString()
            .substring(0, 2);
          var dateYear1 = $('#periodeSearch')
            .first()
            .val()
            .toString()
            .substring(3, 7);
          let period = dateYear1 + dateMonth1;

          let params =
            '?userId=' +
            this.userId +
            '&byUserId=' +
            this.userId +
            '&byDate=' +
            week +
            period;
          this.inquiryWeeklyTask(params);
        }
      }
    });
  }

  workingHourPlan: number;
  workingHourActual: number;
  datetimeTask = [];
  planOfDay = [];
  actualOfDay = [];
  dailyTaskData = [];
  count: any;
  inquiryWeeklyTask(params) {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        let serviceReturn: any;
        let statusService: string;
        let errMess: string;
        let url = 'getSumActiveDailyTasByWeekService';

        //refresh array
        this.datetimeTask = [];
        this.planOfDay = [];
        this.actualOfDay = [];
        this.dailyTaskData = [];
        // console.log(url + params);
        this.callService.getData(url + params).subscribe(
          (res) => {
            serviceReturn = res;
            // console.log(serviceReturn);
            statusService = serviceReturn['0'].serviceContent.status;
            // console.log("hasil status : " + statusService);

            if (statusService == 'S') {
              this.workingHourPlan =
                serviceReturn['0'].serviceContent.workingHourPlan;
              this.workingHourActual =
                serviceReturn['0'].serviceContent.workingHourActual;
              this.count = serviceReturn['0'].serviceContent.count;

              if (serviceReturn['0'].serviceContent.count > 0) {
                for (
                  let i = 0;
                  i < serviceReturn['0'].serviceContent.serviceOutput.length;
                  i++
                ) {
                  this.datetimeTask.push(
                    serviceReturn['0'].serviceContent.serviceOutput[i].startDate
                  );
                  this.planOfDay.push(
                    serviceReturn['0'].serviceContent.serviceOutput[i]
                      .sumPlanInDay
                  );
                  this.actualOfDay.push(
                    serviceReturn['0'].serviceContent.serviceOutput[i]
                      .sumActualInDay
                  );
                  this.dailyTaskData.push(
                    serviceReturn['0'].serviceContent.serviceOutput[i]
                      .detailTaskList
                  );
                }
              }
            } else {
            }
          },
          (err) => {
            this.router.navigateByUrl('/error500');
          }
        );

        this.loadingData = false;
      }
    });
  }

  listtaskWeekly = [];
  taskListdet = [];
  a: any;

  dateTask: string;
  // str.split('-').pop()
  inquiryTaskDetail(idxDaily: number) {
    this.loadingDetail = true;
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        let jsonTask = {};
        this.listtaskWeekly = [];

        this.dateTask = this.datetimeTask[idxDaily];
        // console.log("json task : " + JSON.stringify(this.dailyTaskData[idxDaily]));
        for (let j = 0; j < this.dailyTaskData[idxDaily].length; j++) {
          this.listtaskWeekly.push(this.dailyTaskData[idxDaily][j]);
        }

        this.loadingDetail = false;
      }
    });
  }
}
