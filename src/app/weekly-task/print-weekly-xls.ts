import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { Workbook } from 'exceljs';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable()
export class printWeeklyxls {

  constructor() { }

  public exportAsExcelFile(json: any[], excelFileName: string): void {

    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    // console.log('worksheet', worksheet);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['PDLGD-FL'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });

    FileSaver.saveAs(data, fileName + new Date().getTime() + EXCEL_EXTENSION);
  }

  public generateExcel(data: any, fileName: any) {

    let dataPid= data.pid;
    // console.log("dataPid : "+dataPid);

    let workbook = new Workbook();


    // ====== PID Sheet===================
    var worksheet = workbook.addWorksheet('PID');
    
    

    //Add Row and formatting
    let header = [];
    header.push('PID');
    header.push('Project Name'); 	
    header.push('Category'); 	
    header.push('Activities');	
    header.push('Customer'); 	
    header.push('HR Cost');	
    header.push('Remain HR Cost'); 	
    header.push('Status'); 	
    header.push('Valid/InValid');

    //Add Header Row
    let headerRow = worksheet.addRow(header);

    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'ffffff' },
        bgColor: { argb: 'ffffff' },
      }
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };
      cell.alignment = { vertical: 'middle', horizontal: 'left' };
      cell.font = { bold: true, color: { argb: '00000000' } };
    })

    // Add Data and Conditional Formatting
    dataPid.forEach(d => {
      // console.log(d);
      let row = worksheet.addRow(d);
      row.eachCell((cell, number) => {
        cell.fill = {
          type: 'pattern',
          //pattern:'darkTrellis',
          pattern: 'solid',
          fgColor: { argb: 'ffffff' },
          bgColor: { argb: 'ffffff' }
        }
        // cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
        cell.alignment = { vertical: 'middle', horizontal: 'left' };
      }
      )

    });

    worksheet.getColumn(1).width = 20;
    worksheet.getColumn(2).width = 30;
    worksheet.getColumn(3).width = 30;
    worksheet.getColumn(4).width = 20;
    worksheet.getColumn(5).width = 30;
    worksheet.getColumn(6).width = 10;
    worksheet.getColumn(7).width = 15;
    worksheet.getColumn(8).width = 10;
    worksheet.getColumn(9).width = 15;


    //========Summary Rekap===========================
    let dataSummaryRekap = data.summaryRekap;
    let dataSummaryRekapHeader = data.summaryRekapHeader;
    // console.log("dataSummaryRekap : "+dataSummaryRekap);

    let worksheet2 = workbook.addWorksheet("Summary Rekap");

    worksheet2.addRow([]);
    worksheet2.addRow([]);
    let tophead = worksheet2.addRow(['Sum of Duration (Hour)','Project Categori ']);

    // Cell Style : Fill and Border
    tophead.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'ffffff' },
        bgColor: { argb: 'ffffff' },
      }
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };
      cell.alignment = { vertical: 'middle', horizontal: 'left' };
      cell.font = { bold: true, color: { argb: '00000000' } };
    })
    worksheet2.mergeCells('C3:F3');


    //Add Row and formatting
    let header2 = [];
    header2.push('Nama');
    
    let total = []
    total.push('Grant Total');

    for(let a=0;a<dataSummaryRekapHeader.length;a++){
      header2.push(dataSummaryRekapHeader[a]);
      total.push('-');
    }

    header2.push('Grand Total');

    total.push(' ');
    total.push(' ');
    total.push('-');

    //Add Header Row
    let headerRow2 = worksheet2.addRow(header2);

    // Cell Style : Fill and Border
    headerRow2.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'ffffff' },
        bgColor: { argb: 'ffffff' },
      }
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };
      cell.alignment = { vertical: 'middle', horizontal: 'left' };
      cell.font = { bold: true, color: { argb: '00000000' } };
    })

    let lastColumn = 0;
    // console.log("add data and format");
    // Add Data and Conditional Formatting
    dataSummaryRekap.forEach(d => {
      
      lastColumn=d.length
      // console.log("d : "+d);
      let row = worksheet2.addRow(d);
      row.eachCell((cell, number) => {
        // console.log("no : "+number);
        cell.fill = {
          type: 'pattern',
          //pattern:'darkTrellis',
          pattern: 'solid',
          fgColor: { argb: 'ffffff' },
          bgColor: { argb: 'ffffff' }
        }
        // cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
        if(number==1){
          cell.alignment = { vertical: 'middle', horizontal: 'left' };
        }else{
          cell.alignment = { vertical: 'middle', horizontal: 'left' };
        }
       
      }
      )

    });


    worksheet2.getColumn(1).width = 30;
    for(let a=0;a<dataSummaryRekapHeader.length;a++){
      worksheet2.getColumn(a+2).width = 25;

      if(a = dataSummaryRekapHeader.length -1 ){
        worksheet2.getColumn(a+3).width = 25;
      }
    }
   


    //SUM FORMULA
    const firstRow = worksheet2.getRow(5);
    const lastRow = worksheet2.getRow(worksheet2.rowCount);
    const valueRow = worksheet2.getRow(worksheet2.rowCount+1);

    valueRow.getCell(1).value = 'Grand Total';
    
    for(let a=0;a<dataSummaryRekapHeader.length;a++){
      
      let addressFirstRow = firstRow.getCell(a+2).address;
      let addressLastRow = lastRow.getCell(a+2).address;
      let addressValueRow= valueRow.getCell(a+2).address;
      // console.log("addressValueRow : "+addressValueRow);
      // console.log("function : SUM("+addressFirstRow+":"+ addressLastRow+")");
      worksheet2.getCell(addressValueRow).value = { formula: 'SUM('+addressFirstRow+':'+ addressLastRow+')', date1904: false };

      if(a == dataSummaryRekapHeader.length-1){
        worksheet2.getCell(valueRow.getCell(a+3).address).value = { formula: 'SUM('+(firstRow.getCell(a+3).address)+':'+lastRow.getCell(a+3).address+')', date1904: false };
      }
      
    }

    valueRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'ffffff' },
        bgColor: { argb: 'ffffff' },
      }
      // cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };
      cell.alignment = { vertical: 'middle', horizontal: 'left' };
      cell.font = { bold: true, color: { argb: '00000000' } };
    })


    //========Rekap Biweekly=========================
    let dataRekapBiweekly = data.rekapBiweekly;
    // console.log("dataRekapBiweekly : "+dataRekapBiweekly);

    var worksheet3 = workbook.addWorksheet('Rekap Biweekly');

    //Add Row and formatting
    let header3 = [];
    header3.push('Nama');	
    header3.push('PID'); 	
    header3.push('Project Title');	
    header3.push('Deskripsi');	
    header3.push('Case #');	
    header3.push('Start Periods');	
    header3.push('Duration (Hour)');	
    header3.push('Project Categori'); 	
    header3.push('Project Billable / Unbillable'); 	
    header3.push('Billable / Unbillable'); 	
    header3.push('Status PID');


    //Add Header Row
    let headerRow3 = worksheet3.addRow(header3);

    // Cell Style : Fill and Border
    headerRow3.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'ffffff' },
        bgColor: { argb: 'ffffff' },
      }
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };
      cell.alignment = { vertical: 'middle', horizontal: 'left' };
      cell.font = { bold: true, color: { argb: '00000000' } };
    })

    // Add Data and Conditional Formatting
    dataRekapBiweekly.forEach(d => {
      // console.log(d);
      let row = worksheet3.addRow(d);
      row.eachCell((cell, number) => {
        cell.fill = {
          type: 'pattern',
          //pattern:'darkTrellis',
          pattern: 'solid',
          fgColor: { argb: 'ffffff' },
          bgColor: { argb: 'ffffff' }
        }
        // cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
        cell.alignment = { vertical: 'middle', horizontal: 'left' };
      }
      )

    });

    worksheet3.getColumn(1).width = 30;
    worksheet3.getColumn(2).width = 20;
    worksheet3.getColumn(3).width = 30;
    worksheet3.getColumn(4).width = 40;
    worksheet3.getColumn(5).width = 15;
    worksheet3.getColumn(6).width = 20;
    worksheet3.getColumn(7).width = 20;
    worksheet3.getColumn(8).width = 30;
    worksheet3.getColumn(9).width = 20;
    worksheet3.getColumn(10).width = 20;
    worksheet3.getColumn(11).width = 10;

    

    //Generate Excel File with given name
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      FileSaver.saveAs(blob, fileName + new Date().getTime() + EXCEL_EXTENSION);
    })
  }
}