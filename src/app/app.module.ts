import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { NavbarSideComponent } from './navbar-side/navbar-side.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { DailyTaskComponent } from './daily-task/daily-task.component';
import { WeeklyTaskComponent } from './weekly-task/weekly-task.component';
import { CreateDailyTaskComponent } from './daily-task/create-daily-task/create-daily-task.component';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { ClockComponent } from './header/clock/clock.component';
import { MyTeamComponent } from './my-team/my-team.component';
import { WeeklyTeamComponent } from './my-team/weekly-team/weekly-team.component';
import { startsWithPipe } from './daily-task/customstart.pipe';
import { CreateTeamComponent } from './admin-page/create-team/create-team.component';
import { CreateProjectComponent } from './admin-page/create-project/create-project.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { InquiryUserManagementComponent } from './user-management/user-management/inquiry-user-management/inquiry-user-management.component';
import { InquiryRoleManagementComponent } from './user-management/role-management/inquiry-role-management/inquiry-role-management.component';
import { CreateRoleManagementComponent } from './user-management/role-management/create-role-management/create-role-management.component';
import { UpdateRoleManagementComponent } from './user-management/role-management/update-role-management/update-role-management.component';
import { Rolemanagementfilterpipe } from './user-management/role-management/inquiry-role-management/inquiry-role-management-filterpipe';
import { SettingsComponent } from './user-management/settings/settings.component';
import { ChangePasswordComponent } from './user-management/change-password/change-password.component';
import { DashboardManagerComponent } from './dashboard-manager/dashboard-manager.component';
import { ChartsModule } from 'ng2-charts';
import { WorkingStatusfilterpipe } from './dashboard-manager/inquiry-working-status-filterpipe';
import { UserIdleModule } from 'angular-user-idle';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { MomentModule } from 'angular2-moment'; // optional, provides moment-style pipes for date formatting
import { ModalModule } from 'ngx-bootstrap/modal';
import { HolidayCalendarComponent } from './admin-page/holiday-calendar/holiday-calendar.component';
import { HolidayCalendarfilterpipe } from './admin-page/holiday-calendar/inquiry-holiday-calendar-filterpipe';
import { printUserListLxls } from './user-management/user-management/inquiry-user-management/print-user-list-xls';
import { Projectfilterpipe } from './admin-page/create-project/projectfilterpipe';
import { WeeklyCalendarComponent } from './admin-page/weekly-calendar/weekly-calendar.component';
import { ConfigurationComponent } from './admin-page/configuration/configuration.component';
import { Daterangepicker } from 'ng2-daterangepicker';
import { Inquiryuserfilterpipe } from './user-management/user-management/inquiry-user-management/inquiryuserfilterpipe';
import { printWeeklyxls } from './weekly-task/print-weekly-xls';
import { Summaryfilterpipe } from './dashboard-manager/inquiry-summary-task-filterpipe';
// import { HTTP_INTERCEPTORS } from '@angular/common/http';
// import { AuthInterceptor } from './authInterceptor';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarSideComponent,
    FooterComponent,
    HeaderComponent,
    DailyTaskComponent,
    WeeklyTaskComponent,
    CreateDailyTaskComponent,
    MyTeamComponent,
    WeeklyTeamComponent,
    startsWithPipe,
    CreateTeamComponent,
    CreateProjectComponent,
    InquiryUserManagementComponent,
    InquiryRoleManagementComponent,
    CreateRoleManagementComponent,
    UpdateRoleManagementComponent,
    Rolemanagementfilterpipe,
    SettingsComponent,
    ClockComponent,
    ChangePasswordComponent,
    DashboardManagerComponent,
    WorkingStatusfilterpipe,
    HolidayCalendarComponent,
    HolidayCalendarfilterpipe,
    Projectfilterpipe,
    WeeklyCalendarComponent,
    ConfigurationComponent,
    Inquiryuserfilterpipe,
    Summaryfilterpipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpModule,
    NgxPaginationModule,
    ChartsModule,
    NgIdleKeepaliveModule.forRoot(),
    MomentModule,
    RouterModule,
    ModalModule.forRoot(),
    UserIdleModule.forRoot({ idle: 10, timeout: 50, ping: 120 }),
    Daterangepicker
  ],
  providers: [
    { provide: LocationStrategy, 
      useClass: HashLocationStrategy },
    // {
    //     provide: HTTP_INTERCEPTORS,
    //     useClass: AuthInterceptor,
    //     multi: true
    // }, 
      printUserListLxls, 
      printWeeklyxls
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
