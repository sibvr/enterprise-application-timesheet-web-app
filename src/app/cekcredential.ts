import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { Http, Headers, RequestOptions } from '@angular/http';
import * as CryptoJS from 'crypto-js';
import { AppDecryptMe } from "./app.DecryptMe";
import { GetusercredService } from './getusercred.service';
import { CallServiceFunction } from './callServiceFunction';
import { EncryptComponent } from "./encrypt.component";

@Injectable({
    providedIn: 'root'
})
export class CekCredential {

    constructor(public encrypt: EncryptComponent, public router: Router, public http: Http, public decryptMe: AppDecryptMe, public getUserCred: GetusercredService, private callService: CallServiceFunction) { }

    usertoken: any;
    refreshToken: any;

    async getCredential(status) {
        this.usertoken = "";
        this.refreshToken = "";
        var usercred = this.getUserCred.getUserData();
        this.usertoken = JSON.parse(usercred).user_token;
        this.refreshToken = JSON.parse(usercred).refresh_token;
        let access_token = JSON.parse(usercred).access_token
        // console.log("user tkn : " + this.usertoken + " refresh tkn : " + this.refreshToken + " askes token : " + access_token);
        let body = {
            "refreshToken": this.refreshToken,
            "userToken": this.usertoken,
            "url": "cekUserLoginService"
        }

        // console.log(JSON.stringify(body));

        return await this.callService.postData(body).toPromise().then((res) => {
            // console.log(res.status);
            // status(res.status);
            let serviceReturn = res.json();
            let statusService = serviceReturn["0"].serviceContent.status;
            // console.log("return : " + JSON.stringify(serviceReturn));
            // console.log("status : " + statusService);
            if (statusService != 'S') {
                status("R");
            } else {
                status("K");
            }
            if (serviceReturn["0"].serviceContent.expires_in < 100) {
                this.refreshTokenapi();
            }
        }
        // , (err) => {
            // console.log(err);
            // this.refreshTokenapi();
            // sessionStorage.clear();
            // this.router.navigateByUrl("/");
        // }
        );
        // console.log("random String : "+this.randomString);
        // return this.randomString;

    }
    localstorageuser: {};
    stringLocalStorage: string;
    refreshTokenapi() {
        let body = {
            "refreshToken": this.refreshToken,
            "userToken": this.usertoken,
            "url": "refreshTokenService"
        }
        return this.callService.getRestResultLogin(body).subscribe((res) => {
            let serviceReturn = res.json();
            let statusService = serviceReturn["0"].serviceContent.status;
            // console.log("return : " + JSON.stringify(serviceReturn));
            if (statusService == 'S') {
                this.localstorageuser = {};
                var usercred = this.getUserCred.getUserData();
                let accToken = JSON.parse(usercred).access_token;
                let usernameResult = JSON.parse(usercred).usernameResult;
                let fullnameResult = JSON.parse(usercred).fullnameResult;
                let id = JSON.parse(usercred).userId;
                let refresh_token = JSON.parse(usercred).refresh_token;
                let user_token = JSON.parse(usercred).user_token;
                let rolenameResult = JSON.parse(usercred).roleNameResult;
                let roleTaskList = JSON.parse(usercred).roleTaskList;

                this.localstorageuser['usernameResult'] = usernameResult;
                this.localstorageuser['fullnameResult'] = fullnameResult;
                this.localstorageuser['userId'] = id;
                this.localstorageuser['access_token'] = serviceReturn["0"].serviceContent.access_token;
                this.localstorageuser['refresh_token'] = serviceReturn["0"].serviceContent.refresh_token;
                this.localstorageuser['user_token'] = serviceReturn["0"].serviceContent.user_token;
                this.localstorageuser['roleNameResult'] = rolenameResult;
                this.localstorageuser['roleTaskList'] = roleTaskList;

                this.stringLocalStorage = JSON.stringify(this.localstorageuser);
                var Encrypt: any;
                Encrypt = this.encrypt.aesEncrypt(this.stringLocalStorage);
                var decryptSalt = this.encrypt.aesEncryptSalt(this.encrypt.salt);
                var decryptIV = this.encrypt.aesEncryptIV(this.encrypt.iv);

                sessionStorage.setItem("usdt", Encrypt);
                sessionStorage.setItem("ekrptd1", decryptIV);
                sessionStorage.setItem("ekrtdp2", decryptSalt);
                // sessionStorage.setItem("usdt", (serviceReturn["0"].serviceContent.access_token))
                // this.logout();
                window.location.reload();
            }
            else {
                alert("refresh token gagal");
            }
        });
    }

    logout() {
        let body = {
            "url": "logoutService"
        }
        this.callService.postData(body).subscribe((data) => {
            let serviceReturn = data.json();
            let statusService = serviceReturn['0'].serviceContent.status;
            // console.log(JSON.stringify(serviceReturn));
            if (statusService == "S") {
                sessionStorage.clear();
                this.router.navigateByUrl("/");
            }
            else {
                alert("failed logout");
            }

        })
    }

}
