// import { Injectable } from "@angular/core";
// import {
//   HttpEvent,
//   HttpInterceptor,
//   HttpHandler,
//   HttpRequest,
//   HttpErrorResponse,
//   HttpResponse
// } from "@angular/common/http";
// import { throwError, Observable, BehaviorSubject, of } from "rxjs";
// import { map,catchError, filter, take, switchMap, finalize } from "rxjs/operators";
// import { AppDecryptMe } from "./app.DecryptMe";

// @Injectable()
// export class AuthInterceptor implements HttpInterceptor {
//   private AUTH_HEADER = "Authorization";
//   private ip = "localhost:8881";
//   private token = "";
//   private refreshTokenInProgress = false;
//   private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

//   constructor(public decryptMe: AppDecryptMe) {
//     console.log('interceptor');
//     if (sessionStorage.getItem("ekrptd1") != null && sessionStorage.getItem("ekrtdp2") != null && sessionStorage.getItem("usdt") != null) {
//       var aesDecryptIV = this.decryptMe.aesDecryptIV(sessionStorage.getItem("ekrptd1"));
//       var aesDecryptSalt = this.decryptMe.aesDecryptSalt(sessionStorage.getItem("ekrtdp2"));
//       var aesDecrypt = this.decryptMe.aesDecrypt(sessionStorage.getItem("usdt"), aesDecryptIV, aesDecryptSalt);
//       this.token = JSON.parse(aesDecrypt).access_token;
//     }
//   }

//   intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {


//     if (this.token) {
//       request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + this.token) });
//     }

//     if (!request.headers.has('Content-Type')) {
//       request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
//     }

//     request = request.clone({ headers: request.headers.set('Accept', 'application/json') });

//     console.log("masuk interceptor return");
//     return next.handle(request).pipe(
//       // map((event: HttpEvent<any>) => {
//       //   if (event instanceof HttpResponse) {
//       //     console.log('event--->>>', event);
//       //   }
//       //   return event;
//       // }),
//       catchError((err, caught: Observable<HttpEvent<any>>) => {
//         if (err instanceof HttpErrorResponse && err.status == 401) {
//           console.log("error 401");
//           // this.router.navigate(['login'], { queryParams: { returnUrl: request.url } });
//           return of(err as any);
//         }
//         throw err;
//       })
    
//     );

//     // if (!req.headers.has('Content-Type')) {
//     //   req = req.clone({
//     //     headers: req.headers.set('Content-Type', 'application/json')
//     //   });
//     // }

//     // req = this.addAuthenticationToken(req);

//     // return next.handle(req).
//     //   pipe(
//     //     catchError((error: HttpErrorResponse) => {

//     //       if (error && error.status === 401) {
//     // 401 errors are most likely going to be because we have an expired token that we need to refresh.
//     // if (this.refreshTokenInProgress) {
//     //   // If refreshTokenInProgress is true, we will wait until refreshTokenSubject has a non-null value
//     //   // which means the new token is ready and we can retry the request again
//     //   return this.refreshTokenSubject.pipe(
//     //     filter(result => result !== null),
//     //     take(1),
//     //     switchMap(() => next.handle(this.addAuthenticationToken(req)))
//     //   );


//     //   } else {
//     //     this.refreshTokenInProgress = true;

//     //     // Set the refreshTokenSubject to null so that subsequent API calls will wait until the new token has been retrieved
//     //     this.refreshTokenSubject.next(null);

//     //     return this.refreshAccessToken().pipe(
//     //       switchMap((success: boolean) => {
//     //         this.refreshTokenSubject.next(success);
//     //         return next.handle(this.addAuthenticationToken(req));
//     //       }),
//     // //       // When the call to refreshToken completes we reset the refreshTokenInProgress to false
//     // //       // for the next time the token needs to be refreshed
//     //       finalize(() => this.refreshTokenInProgress = false)
//     //     );
//     //   }

//     //       } else {
//     //         return throwError(error);
//     //       }

//     //     })
//     // );
//   }

//   // private refreshAccessToken(): Observable<any> {
//   //   return of("secret token");
//   // }

//   // private addAuthenticationToken(request: HttpRequest<any>): HttpRequest<any> {
//   //   // If we do not have a token yet then we should not set the header.
//   //   // Here we could first retrieve the token from where we store it.
//   //   if (!this.token) {
//   //     return request;
//   //   }
//   //   // If you are calling an outside domain then do not add the token.
//   //   if (!request.url.match('/'+this.ip+'\//')) {
//   //     return request;
//   //   }
//   //   return request.clone({
//   //     headers: request.headers.set(this.AUTH_HEADER, "Bearer " + this.token)
//   //   });
//   // }
// }