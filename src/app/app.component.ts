import {
  Component,
  ViewChild,
  TemplateRef,
  ElementRef,
  AfterViewInit,
} from '@angular/core';
import { Router } from '@angular/router';

import { Idle, DEFAULT_INTERRUPTSOURCES } from '@ng-idle/core';
import { Keepalive } from '@ng-idle/keepalive';
import { AppDecryptMe } from './app.DecryptMe';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AppService } from './app.service';
import { CallServiceFunction } from './callServiceFunction';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  idleState = 'Not started.';
  timedOut = false;
  lastPing?: Date = null;
  title = 'angular-idle-timeout';

  public modalRef: BsModalRef;

  @ViewChild('childModal') childModal: ModalDirective;

  constructor(
    private idle: Idle,
    private keepalive: Keepalive,
    public decryptMe: AppDecryptMe,
    public router: Router,
    private modalService: BsModalService,
    private appService: AppService,
    private callService: CallServiceFunction
  ) {
    // sets an idle timeout of 5 seconds, for testing purposes.
    idle.setIdle(1000);
    // sets a timeout period of 5 seconds. after 10 seconds of inactivity, the user will be considered timed out.
    idle.setTimeout(10);
    // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
    idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    idle.onIdleEnd.subscribe(() => {
      this.idleState = 'No longer idle.';
      this.reset();
    });

    idle.onTimeout.subscribe(() => {
      this.childModal.hide();
      this.idleState = 'Timed out!';
      this.timedOut = true;
      this.submitLogut();
    });

    idle.onIdleStart.subscribe(() => {
      this.idleState = "You've gone idle!";
      this.childModal.show();
    });

    idle.onTimeoutWarning.subscribe((countdown) => {
      this.idleState = 'You will time out in ' + countdown + ' seconds!';
      // console.log(this.idleState);
    });
    if (
      sessionStorage.getItem('ekrptd1') == null &&
      sessionStorage.getItem('ekrtdp2') == null &&
      sessionStorage.getItem('usdt') == null
    ) {
      this.router.navigateByUrl('/');
    }
    // sets the ping interval to 15 seconds
    keepalive.interval(15);

    keepalive.onPing.subscribe(() => (this.lastPing = new Date()));

    this.appService.getUserLoggedIn().subscribe((userLoggedIn) => {
      if (userLoggedIn) {
        idle.watch();
        this.timedOut = false;
      } else {
        idle.stop();
      }
    });
  }

  reset() {
    this.idle.watch();
    this.timedOut = false;
  }

  hideChildModal(): void {
    this.childModal.hide();
  }

  stay() {
    this.childModal.hide();
    this.reset();
  }

  logout() {
    this.childModal.hide();
    this.appService.setUserLoggedIn(false);
    this.submitLogut();
  }
  submitLogut() {
    let body = {
      url: 'logoutService',
    };
    this.callService.postData(body).subscribe((data) => {
      let serviceReturn = data.json();
      let statusService = serviceReturn['0'].serviceContent.status;
      if (statusService == 'S') {
        sessionStorage.clear();
        this.router.navigateByUrl('/');
      } else {
        alert('failed logout');
      }
    });
  }
}
