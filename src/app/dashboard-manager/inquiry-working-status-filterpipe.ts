import { Pipe, PipeTransform, Injectable } from '@angular/core';
@Pipe({
    name: 'filterworkingstatus'
})

export class WorkingStatusfilterpipe implements PipeTransform {
    transform(items: any[], inputName: string) {

        // console.log("items : "+JSON.stringify(items));

        if (inputName != "null") {
            // console.log("input date : "+inputName);

            if (items && items.length) {
                return items.filter(item => {
                    if (inputName.toLowerCase() && String(item.fullname).toLowerCase().indexOf(inputName.toLowerCase()) === -1) {
                       
                        if (inputName == "") {
                            return true;
                        }
                        else {
                            return false;
                        }
                    
                    }

                    return true;
                })
            }
            else {
                return items;
            }
        
        }else {
            // console.log("else");
            if (items && items.length) {
                return items.filter(item => {
                    if (inputName && String(item.createDatetime).indexOf(inputName) === -1) {

                        return true;
                    }



                    return true;
                })
            } else {
                return items;
            }
        }

    }

}