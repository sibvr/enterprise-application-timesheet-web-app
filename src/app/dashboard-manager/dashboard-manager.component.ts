import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label, Color, ThemeService } from 'ng2-charts';
import { CallServiceFunction } from '../callServiceFunction';
import { AppDecryptMe } from '../app.DecryptMe';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { CekCredential } from '../cekcredential';
import { Console } from 'console';
declare function showSwal(type, title, text): any;
declare var $;

@Component({
  selector: 'app-dashboard-manager',
  templateUrl: './dashboard-manager.component.html',
  styleUrls: ['./dashboard-manager.component.css'],
  providers: [DatePipe],
})
export class DashboardManagerComponent implements OnInit {
  constructor(
    private cekCredential: CekCredential,
    public router: Router,
    private datePipe: DatePipe,
    private callService: CallServiceFunction,
    public decryptMe: AppDecryptMe
  ) {}

  // FORM
  workingstatus = new FormGroup({
    wfoRadio: new FormControl('', [Validators.required]),
    sehatRadio: new FormControl('', [Validators.required]),
    reasonst: new FormControl('', [Validators.required]),
    reaLeave: new FormControl('', [Validators.required]),
    reasonSick: new FormControl('', [Validators.required]),
  });

  //CHART SUM PLAN ACTUAL
  public barChartType: ChartType = 'bar';
  public barChartColors: Color[] = [
    { backgroundColor: '#f38400' },
    { backgroundColor: '#875692' },
  ];
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];
  public barChartLabels: Label[] = [];
  public barChartData: ChartDataSets[] = [];
  public chartClicked(e: any): void {}
  public chartHovered(e: any): void {}
  public barChartOptions: ChartOptions = {
    legend: { position: 'bottom' },
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      xAxes: [{}],
      yAxes: [
        {
          ticks: {
            suggestedMin: 0,
          },
        },
      ],
    },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      },
    },
  };

  //Check WFH Manager
  cekStatusWFHorSehat() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        this.callService
          .getData('getWorkingService?userId=' + this.userId)
          .subscribe(
            (data) => {
              let serviceReturn = data;
              // console.log(JSON.stringify(serviceReturn));
              let statusService = serviceReturn['0'].serviceContent.status;
              if (statusService == 'S') {
                // console.log(this.statusSehat);
                if (
                  serviceReturn['0'].serviceContent.count == 0 &&
                  this.RoleName != 'SUPER-ADMIN'
                ) {
                  if (
                    this.statusSehat == null &&
                    (this.RoleName != 'STAFF' || this.RoleName != 'ADMIN-STAFF')
                  ) {
                    // console.log("masukk?");
                    document.getElementById('openModalButton').click();
                  }
                  this.getConfigWorking();
                } else {
                  this.getTeamMember();
                }
              }
            },
            (err) => {
              // console.log("error return : " + err);
              sessionStorage.clear();
              this.router.navigateByUrl('/');
            }
          );
      }
    });
  }
  listWorkingStatusConfig = [];
  getConfigWorking() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        this.listWorkingStatusConfig = [];
        this.callService
          .getData(
            'getConfigWorkingService?userId=' + this.userId + '&parameter=Y'
          )
          .subscribe((data) => {
            let serviceReturn = data;
            // console.log(JSON.stringify(serviceReturn));
            let statusService = serviceReturn['0'].serviceContent.status;
            if (statusService == 'S') {
              for (
                let i = 0;
                i < serviceReturn['0'].serviceContent.serviceOutput.length;
                i++
              ) {
                this.listWorkingStatusConfig.push(
                  serviceReturn['0'].serviceContent.serviceOutput[i]
                );
              }

              this.getTeamMember();
            }
          });
      }
    });
  }

  wfh: any;
  sehat: any;
  localstoragestatus = {};
  stringLocalStoragestatus: string;
  // statussehat
  submitStatusSehat() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        if (this.workingstatus.controls['sehatRadio'].value != '') {
          if (this.workingstatus.controls['sehatRadio'].value == 'SAKIT') {
            this.sehat = this.workingstatus.controls['sehatRadio'].value
              .concat(' - ')
              .concat(this.workingstatus.controls['reasonSick'].value);
          } else {
            this.sehat = this.workingstatus.controls['sehatRadio'].value;
          }
        }
        if (this.workingstatus.controls['wfoRadio'].value != '') {
          if (this.workingstatus.controls['wfoRadio'].value == 'Onsite') {
            this.wfh = this.workingstatus.controls['wfoRadio'].value
              .concat(' - ')
              .concat(this.workingstatus.controls['reasonst'].value);
          } else if (
            this.workingstatus.controls['wfoRadio'].value == 'Leave' &&
            this.workingstatus.controls['sehatRadio'].value == 'SEHAT'
          ) {
            this.wfh = this.workingstatus.controls['wfoRadio'].value
              .concat(' - ')
              .concat(this.workingstatus.controls['reaLeave'].value);
          } else {
            this.wfh = this.workingstatus.controls['wfoRadio'].value;
          }
        }

        // console.log(this.wfh, this.sehat);

        let body = {
          createUserId: this.userId,
          statusWFH: this.wfh,
          statusKesehatan: this.sehat,
          url: 'createWorkingService',
        };
        // console.log(JSON.stringify(body));
        this.callService.postData(body).subscribe(
          (data) => {
            let serviceReturn = data.json();
            let statusService = serviceReturn['0'].serviceContent.status;
            // console.log(JSON.stringify(serviceReturn));
            if (statusService == 'S') {
              this.localstoragestatus = {};
              this.localstoragestatus['statusWfh'] =
                serviceReturn['0'].serviceContent.serviceOutput.statusWFH;
              this.localstoragestatus['statusSehat'] =
                serviceReturn['0'].serviceContent.serviceOutput.statusKesehatan;
              this.stringLocalStoragestatus = JSON.stringify(
                this.localstoragestatus
              );
              sessionStorage.setItem('status', this.stringLocalStoragestatus);

              if (this.workingstatus.controls['wfoRadio'].value == 'Leave') {
                this.createLeave();
              } else {
                window.location.reload();
              }
            } else {
              showSwal('warning', 'Status kerja', 'Status kerja gagal ');
            }
          },
          (err) => {
            showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ');
          }
        );
      }
    });
  }

  listWorkingKesehatanConfig = ['SEHAT', 'SAKIT'];
  statusidsakit = 'false';
  statusonsite = 'false';
  statusLeave = 'false';
  ifsehat() {
    if (
      this.workingstatus.controls['sehatRadio'].value == 'SAKIT' &&
      this.workingstatus.controls['wfoRadio'].value == 'Leave'
    ) {
      this.statusidsakit = 'true';
      this.statusLeave = 'false';
    } else if (this.workingstatus.controls['sehatRadio'].value == 'SAKIT') {
      this.statusidsakit = 'true';
    } else if (
      this.workingstatus.controls['wfoRadio'].value == 'Leave' &&
      this.workingstatus.controls['sehatRadio'].value == 'SEHAT'
    ) {
      this.statusidsakit = 'false';
      this.statusLeave = 'true';
    } else {
      this.statusidsakit = 'false';
    }
  }
  ifwfh() {
    // console.log(this.workingstatus.controls['wfoRadio'].value);
    if (this.workingstatus.controls['wfoRadio'].value == 'Onsite') {
      this.statusonsite = 'true';
      this.statusLeave = 'false';
    } else if (
      this.workingstatus.controls['sehatRadio'].value == 'SAKIT' &&
      this.workingstatus.controls['wfoRadio'].value == 'Leave'
    ) {
      this.statusLeave = 'false';
      this.statusonsite = 'false';
    } else if (this.workingstatus.controls['wfoRadio'].value == 'Leave') {
      this.statusLeave = 'true';
      this.statusonsite = 'false';
    } else {
      this.statusLeave = 'false';
      this.statusonsite = 'false';
    }
  }
  ifsakit() {
    this.statusidsakit = 'true';
  }

  createLeave() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        let reacuti = this.workingstatus.controls['reaLeave'].value;
        if (reacuti == '') {
          reacuti = '-';
        }
        let date = new Date();
        let month = date.getMonth();
        let day = date.getDate();
        var sekarang = new Date(date.getFullYear(), month, day);
        let todayisthedaydate = this.datePipe.transform(sekarang, 'yMMdd');
        // 20201110
        let body = {
          createUserId: this.userId,
          taskList: [
            {
              projectId: 0,
              userId: this.userId,
              workingHourPlan: 8,
              taskDetail: reacuti,
              startDate: todayisthedaydate,
            },
          ],
          url: 'createLeaveService',
        };
        // console.log(body);
        this.callService.postData(body).subscribe(
          (data) => {
            let serviceReturn = data.json();
            let statusService = serviceReturn['0'].serviceContent.status;
            if (statusService == 'S') {
              window.location.reload();
            } else {
              showSwal(
                'warning',
                'status kerja',
                serviceReturn['0'].serviceContent.errorMessage.toString()
              );
            }
          },
          (err) => {
            showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ');
          }
        );
      }
    });
  }

  closemodal() {
    sessionStorage.setItem('corona', 'setCorona');
  }
  closesehat() {
    if (sessionStorage.getItem('corona') == null) {
      document.getElementById('openModalButtoncorona').click();
    }
  }

  //===============WORKING STATUS=========================
  filterTeamWorkingStatus = 'null';
  //Date Range
  public dateRangeWorkingStatus: any = {};
  public options: any = {};
  public selectedDateWorkingStatus(value: any, datepicker?: any) {
    datepicker.start = value.start;
    datepicker.end = value.end;
    // console.log(datepicker.start);
    this.dateRangeWorkingStatus.start = this.datePipe.transform(
      this.dateRangeWorkingStatus.start,
      'yyyyMMdd'
    );
    this.dateRangeWorkingStatus.end = this.datePipe.transform(
      this.dateRangeWorkingStatus.end,
      'yyyyMMdd'
    );

    // console.log("dateRangeWorkingStatus start : " + this.dateRangeWorkingStatus.start);
    // console.log("dateRangeWorkingStatus end : " + this.dateRangeWorkingStatus.end);
  }
  public calendarCanceledWorkingStatus(e: any) {
    // console.log(e);
    // e.event
    // e.picker
  }

  //filter chart weekly
  drFilterWorkingStatusTeam = '';
  resetWorkingStatusTeam(e: any) {
    this.drFilterWorkingStatusTeam =
      this.dateRangeWorkingStatus.start + '-' + this.dateRangeWorkingStatus.end;
    // console.log("drFilterWorkingStatusTeam : " + this.drFilterWorkingStatusTeam);
  }

  // inquiry working status
  filterNameWorkingStatus = '';
  arrayCreateDate = [];
  listWorkingStatus = [];
  inquiryWorkingStatusTeam(e: any) {
    // console.log("call 1");
    this.loadingWFH = true;
    this.loadingButtonWorkingStatus = true;

    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        //reset filter
        this.arrayCreateDate = [];
        this.listWorkingStatus = [];
        this.listWorkingStatus = [];

        let dateRangeFilter = '';
        if (e) {
          if (this.drFilterWorkingStatusTeam == '') {
            dateRangeFilter = this.dateRageToday;
          } else {
            dateRangeFilter = this.drFilterWorkingStatusTeam;
          }
        } else {
          dateRangeFilter = this.dateRageToday;
        }

        // console.log("dateRangeFilter : " + dateRangeFilter);

        // console.log('getWorkingByTeamService?userId=' + this.userId + '&byTeam=' + this.idTeam + '&byDate=' + dateRangeFilter);
        this.callService
          .getData(
            'getWorkingByTeamService?userId=' +
              this.userId +
              '&byTeam=' +
              this.idTeam +
              '&byDate=' +
              dateRangeFilter
          )
          .subscribe((data) => {
            let serviceReturn = data;

            let statusService = serviceReturn['0'].serviceContent.status;

            // console.log("listStaff : " + JSON.stringify(this.listStaff));
            // console.log("service return : "+JSON.stringify(serviceReturn));

            if (
              statusService == 'S' &&
              serviceReturn['0'].serviceContent.count > 0
            ) {
              //filtering
              for (let e = 0; e < this.listStaff.length; e++) {
                let dataRow = {};
                let detailTask = '';

                for (
                  let i = 0;
                  i < serviceReturn['0'].serviceContent.serviceOutput.length;
                  i++
                ) {
                  if (
                    this.listStaff[e].userId ==
                    serviceReturn['0'].serviceContent.serviceOutput[i].userId
                  ) {
                    detailTask =
                      detailTask +
                      '<b>' +
                      serviceReturn['0'].serviceContent.serviceOutput[i].pid +
                      '</b> - ' +
                      serviceReturn['0'].serviceContent.serviceOutput[i]
                        .taskDetail +
                      ', plan : ' +
                      serviceReturn['0'].serviceContent.serviceOutput[i]
                        .workingHourPlan +
                      ', actual : ' +
                      serviceReturn['0'].serviceContent.serviceOutput[i]
                        .workingHourActual +
                      '<br />';

                    dataRow = {
                      createDatetime:
                        serviceReturn['0'].serviceContent.serviceOutput[i]
                          .startDate,
                      fullname:
                        serviceReturn['0'].serviceContent.serviceOutput[i]
                          .fullname,
                      statusWFH:
                        serviceReturn['0'].serviceContent.serviceOutput[i]
                          .statusWFH,
                      statusKesehatan:
                        serviceReturn['0'].serviceContent.serviceOutput[i]
                          .statusKesehatan,
                      detailTask: '<p>' + detailTask + '</p>',
                    };
                  }
                }

                if (Object.keys(dataRow).length) {
                  this.listWorkingStatus.push(dataRow);
                }
              }
            }

            this.loadingWFH = false;
            this.loadingButtonWorkingStatus = false;

            if (e == null) {
              // console.log("dari on init");
              this.inquirySumTaskMember(null);
            }
          });
      }
    });
  }

  //===============Summary Plan Actual=====================
  //CHART Sum Plan Actual
  filterTeamSumPlan = 'null';
  public barChartType2: ChartType = 'bar';
  public barChartColors2: Color[] = [
    { backgroundColor: '#f38400' },
    { backgroundColor: '#875692' },
  ];
  public barChartLegend2 = true;
  public barChartPlugins2 = [pluginDataLabels];
  public barChartLabels2: Label[] = [];
  public barChartData2: ChartDataSets[] = [];
  public barChartOptions2: ChartOptions = {
    legend: { position: 'bottom' },
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      xAxes: [{}],
      yAxes: [
        {
          ticks: {
            suggestedMin: 0,
          },
        },
      ],
    },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      },
    },
  };
  //Date Range
  public dateRangeSumPlanActual: any = {};
  public selectedDateSumPlanActual(value: any, datepicker?: any) {
    // any object can be passed to the selected event and it will be passed back here
    datepicker.start = value.start;
    datepicker.end = value.end;
    this.dateRangeSumPlanActual.start = this.datePipe.transform(
      this.dateRangeSumPlanActual.start,
      'yyyyMMdd'
    );
    this.dateRangeSumPlanActual.end = this.datePipe.transform(
      this.dateRangeSumPlanActual.end,
      'yyyyMMdd'
    );

    // console.log("dateRangeSumPlanActual start : " + this.dateRangeSumPlanActual.start);
    // console.log("dateRangeSumPlanActual end : " + this.dateRangeSumPlanActual.end);
  }
  public calendarCanceledSumPlanActual(e: any) {}

  //filter chart weekly
  drFilterChartSumPlanActual = '';
  resetChartSumPlanActual(e: any) {
    this.drFilterChartSumPlanActual =
      this.dateRangeSumPlanActual.start + '-' + this.dateRangeSumPlanActual.end;
    // console.log("drFilterChartSumPlanActual : " + this.drFilterChartSumPlanActual);
  }

  inquiryChartSumPlanActual(e: any) {
    // console.log("call 3");
    this.loadingButtonSumPlanActual = true;

    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        //reset chart 2
        this.barChartLabels2 = [];
        this.barChartData2 = [];

        let dateRangeFilter = '';
        if (e) {
          // console.log("masuk e");
          if (this.drFilterChartSumPlanActual == '') {
            dateRangeFilter = this.dateRageToday;
          } else {
            dateRangeFilter = this.drFilterChartSumPlanActual;
          }
        } else {
          // console.log("gk masuk e");
          dateRangeFilter = this.dateRageToday;
        }

        let serviceReturn: any;
        let statusService: string;
        let errMess: string;

        let url = 'getSumActiveDailyTaskByDateService';
        let params =
          '?userId=' +
          this.userId +
          '&byTeamId=' +
          this.idTeam +
          '&byDate=' +
          dateRangeFilter;

        // console.log("hit  : " + url);
        // console.log("full url : " + url + params);
        this.callService.getData(url + params).subscribe((res) => {
          serviceReturn = res;
          statusService = serviceReturn['0'].serviceContent.status;

          // console.log("hasil return getSumActiveDailyTaskByDateService : " + JSON.stringify(serviceReturn));
          if (statusService == 'S') {
            if (
              serviceReturn['0'].serviceContent.count > 0 &&
              this.listStaff.length > 0
            ) {
              this.barChartLabels = [];
              let dataPlan = [];
              let dataActual = [];

              //filtering
              for (let e = 0; e < this.listStaff.length; e++) {
                let sumPlan = 0;
                let sumActual = 0;
                for (
                  let i = 0;
                  i < serviceReturn['0'].serviceContent.serviceOutput.length;
                  i++
                ) {
                  if (
                    serviceReturn['0'].serviceContent.serviceOutput[i]
                      .fullname == this.listStaff[e].userFullname
                  ) {
                    sumPlan =
                      sumPlan +
                      serviceReturn['0'].serviceContent.serviceOutput[i]
                        .sumPlanInDay;
                    sumActual =
                      sumActual +
                      serviceReturn['0'].serviceContent.serviceOutput[i]
                        .sumActualInDay;
                  }
                }

                this.barChartLabels.push(this.listStaff[e].userFullname);
                dataPlan.push(sumPlan);
                dataActual.push(sumActual);
              }

              this.barChartData = [
                { data: dataPlan, label: 'Rencana : Waktu (jam)' },
                { data: dataActual, label: 'Aktualisasi : Waktu (jam)' },
              ];
            }
          }

          this.loadingButtonSumPlanActual = false;
        });
      }
    });
  }

  //=================Summary Task Member=================
  filterTeamSumTaskMember = 'null';
  filterNameSumTaskMember = '';
  //Date Range
  public dateRangeSumTaskMember: any = {};
  public selectedDateSumTaskMember(value: any, datepicker?: any) {
    datepicker.start = value.start;
    datepicker.end = value.end;
    this.dateRangeSumTaskMember.start = this.datePipe.transform(
      this.dateRangeSumTaskMember.start,
      'yyyyMMdd'
    );
    this.dateRangeSumTaskMember.end = this.datePipe.transform(
      this.dateRangeSumTaskMember.end,
      'yyyyMMdd'
    );
    // console.log("dateRangeSumTaskMember start : " + this.dateRangeSumTaskMember.start);
    // console.log("dateRangeSumTaskMember end : " + this.dateRangeSumTaskMember.end);
  }
  public calendarCanceledSumTaskMember(e: any) {}

  //filter char weekly
  drFilterSumTaskMember = '';
  resetSumTaskMember(e: any) {
    this.drFilterSumTaskMember =
      this.dateRangeSumTaskMember.start + '-' + this.dateRangeSumTaskMember.end;
    // console.log("drFilterSumTaskMember : " + this.drFilterSumTaskMember);
  }

  listHeaderSumTaskMember = [];
  listDataSumTaskMember = [];
  inquirySumTaskMember(e: any) {
    // console.log("call 2");
    this.loadingButtonSumTaskMember = true;
    this.loadingSummaryTask = true;

    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        let dateRangeFilter = '';
        if (e) {
          if (this.drFilterSumTaskMember == '') {
            dateRangeFilter = this.dateRageToday;
          } else {
            dateRangeFilter = this.drFilterSumTaskMember;
          }
        } else {
          dateRangeFilter = this.dateRageToday;
        }

        let serviceReturn: any;
        let statusService: string;
        let errMess: string;

        let url = 'getSumWorkingByTeamService';
        let params =
          '?userId=' +
          this.userId +
          '&byTeamId=' +
          this.idTeam +
          '&byDate=' +
          dateRangeFilter;
        // console.log("hit : " + url);
        // console.log("full url : " + url + params);

        this.callService.getData(url + params).subscribe((res) => {
          serviceReturn = res;
          statusService = serviceReturn['0'].serviceContent.status;
          // console.log("hasil status  getSumWorkingByTeamService : " + JSON.stringify(serviceReturn));

          if (statusService == 'S') {
            this.listHeaderSumTaskMember = [];
            this.listDataSumTaskMember = [];
            if (serviceReturn['0'].serviceContent.count > 0) {
              this.listHeaderSumTaskMember =
                serviceReturn['0'].serviceContent.listKey;
              this.listDataSumTaskMember =
                serviceReturn['0'].serviceContent.serviceOutput;
            }
          }

          this.loadingSummaryTask = false;
          this.loadingButtonSumTaskMember = false;

          if (e == null) {
            // console.log("dari on init");
            this.inquiryChartSumPlanActual(null);
          }
        });
      }
    });
  }

  listTeam1 = [];
  listTeam2 = [];
  listTeam3 = [];
  idTeam: any;
  teamName: string;
  listStaff = [];
  getTeamMember() {
    // console.log("call pertama");
    this.listStaff = [];
    this.listTeam1 = [];
    this.listTeam2 = [];
    this.listTeam3 = [];

    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        if (
          sessionStorage.getItem('corona') == null &&
          sessionStorage.getItem('status') != null
        ) {
          document.getElementById('openModalButtoncorona').click();
        }
        // console.log("masuk get team");
        this.callService
          .getData(
            'getTeamService?userId=' + this.userId + '&parameter=' + this.userId
          )
          .subscribe((data) => {
            let serviceReturn = data;
            // console.log("get member : " + JSON.stringify(serviceReturn));
            let statusService = serviceReturn['0'].serviceContent.status;
            if (statusService == 'S') {
              this.idTeam = serviceReturn['0'].serviceContent.teamData.id;
              this.teamName =
                serviceReturn['0'].serviceContent.teamData.teamName;
              // console.log("id team : " + this.idTeam);
              // console.log("team name : " + this.teamName);

              for (
                let i = 0;
                i < serviceReturn['0'].serviceContent.serviceOutput.length;
                i++
              ) {
                this.listStaff.push(
                  serviceReturn['0'].serviceContent.serviceOutput[i]
                );
              }

              // console.log("masuk sec-head filter");
              for (
                let e = 0;
                e < serviceReturn['0'].serviceContent.teamList.length;
                e++
              ) {
                let dataTeam = {};
                dataTeam['idTeam'] =
                  serviceReturn['0'].serviceContent.teamList[e].id;
                dataTeam['teamCode'] =
                  serviceReturn['0'].serviceContent.teamList[e].teamCode;

                this.listTeam1.push(dataTeam);
                this.listTeam2.push(dataTeam);
                this.listTeam3.push(dataTeam);
              }

              this.filterTeamSumPlan = this.idTeam;
              this.filterTeamSumTaskMember = this.idTeam;
              this.filterTeamWorkingStatus = this.idTeam;

              // console.log("filterTeamSumPlan : " + this.filterTeamSumPlan);
              // console.log("filterTeamSumTaskMember : " + this.filterTeamSumTaskMember);
              // console.log("filterTeamWorkingStatus : " + this.filterTeamWorkingStatus);

              this.inquiryWorkingStatusTeam(null);
            }
          });
      }
    });
  }

  //pagination
  p = 1;
  o = 1;
  currMonth: any;
  userId: any;
  statusWfh: any;
  statusSehat: any;
  RoleName: any;
  loadingWFH = true;
  loadingSummaryTask = true;
  // loadingGraphDaily = true;
  loadingGraphWeekly = true;
  dateRageToday = '';
  loadingButtonSumPlanActual = false;
  loadingButtonSumTaskMember = false;
  loadingButtonWorkingStatus = false;
  today: string;
  ngOnInit(): void {
    this.loadingWFH = true;
    // this.loadingGraphDaily = true;
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        let date = new Date();
        let month = date.getMonth();
        let day = date.getDate();
        var sekarang = new Date(date.getFullYear(), month, day);
        // let lastMonth = new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000);

        //range today-lastmonth
        this.dateRageToday =
          this.datePipe.transform(sekarang, 'yyyyMMdd') +
          '-' +
          this.datePipe.transform(sekarang, 'yyyyMMdd');
        this.today = this.datePipe.transform(sekarang, 'yyyy/MM/dd');
        // console.log(" this.dateRageToday : " + this.dateRageToday);
        // console.log(" today : " + this.today);
        this.options = {
          locale: { format: 'YYYY/MM/DD' },
          alwaysShowCalendars: false,
          showDropdowns: true,
          startDate: this.today,
          endDate: this.today,
          opens: 'left',
          drops: 'auto',
          buttonClasses: 'btn btn-lg',
          cancelClass: 'btn-danger',
        };

        //sum plan
        this.dateRangeSumPlanActual.start = this.datePipe.transform(
          sekarang,
          'yyyyMMdd'
        );
        this.dateRangeSumPlanActual.end = this.datePipe.transform(
          sekarang,
          'yyyyMMdd'
        );
        //sum plan
        this.dateRangeSumTaskMember.start = this.datePipe.transform(
          sekarang,
          'yyyyMMdd'
        );
        this.dateRangeSumTaskMember.end = this.datePipe.transform(
          sekarang,
          'yyyyMMdd'
        );
        //sum plan
        this.dateRangeWorkingStatus.start = this.datePipe.transform(
          sekarang,
          'yyyyMMdd'
        );
        this.dateRangeWorkingStatus.end = this.datePipe.transform(
          sekarang,
          'yyyyMMdd'
        );

        // console.log("dateRangeSumPlanActual : " + this.dateRangeSumPlanActual.start);
        // console.log("dateRangeSumTaskMember : " + this.dateRangeSumTaskMember.start);
        // console.log("dateRangeWorkingStatus : " + this.dateRangeWorkingStatus.start);

        $('#datepicker-popup1').datepicker({
          format: 'dd-mm-yyyy',
          // viewMode: "months",
          todayHighlight: true,
          autoclose: true,
          // minViewMode: "months"
        });

        $('#datepicker-popup2').datepicker({
          format: 'mm-yyyy',
          viewMode: 'months',
          autoclose: true,
          minViewMode: 'months',
        });
        if (sessionStorage.getItem('status') != null) {
          var berhasilIV = sessionStorage.getItem('status');
          this.statusSehat = JSON.parse(berhasilIV).statusSehat;
          this.statusWfh = JSON.parse(berhasilIV).statusWfh;
          // console.log(this.statusSehat);
        }

        var aesDecryptIV = this.decryptMe.aesDecryptIV(
          sessionStorage.getItem('ekrptd1')
        );
        var aesDecryptSalt = this.decryptMe.aesDecryptSalt(
          sessionStorage.getItem('ekrtdp2')
        );
        var aesDecrypt = this.decryptMe.aesDecrypt(
          sessionStorage.getItem('usdt'),
          aesDecryptIV,
          aesDecryptSalt
        );
        this.userId = JSON.parse(aesDecrypt).userId;

        this.currMonth = new Date().getMonth();
        this.RoleName = JSON.parse(aesDecrypt).roleNameResult;

        this.cekStatusWFHorSehat();
      }
    });
  }
}
