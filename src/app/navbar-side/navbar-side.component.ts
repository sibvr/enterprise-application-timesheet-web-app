import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { CallServiceFunction } from '../callServiceFunction';
import { AppDecryptMe } from '../app.DecryptMe';
import { CheckusercredService } from '../checkusercred.service';
import { SafeUrl } from '@angular/platform-browser';
// import { ConsoleReporter } from 'jasmine';

@Component({
  selector: 'app-navbar-side',
  templateUrl: './navbar-side.component.html',
  styleUrls: ['./navbar-side.component.css'],
})
export class NavbarSideComponent implements OnInit {
  scrWidth: any;

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    // this.scrHeight = window.innerHeight;
    this.scrWidth = window.innerWidth;
    // console.log(this.scrHeight, this.scrWidth);
  }
  constructor(
    private router: Router,
    private callService: CallServiceFunction,
    public checkUserCred: CheckusercredService,
    public decryptMe: AppDecryptMe
  ) {
    this.getScreenSize();
  }

  statusSehat = '';
  statusWfh: any;
  statusWFHReason: any;
  userNameFull: any;
  roleNameResult: any;
  inquirymyTeam: any;
  userImageResult: any;
  teamCode: any;
  getUserManagementService: any;
  createProjectManagementService: any;
  getTeamManagementService: any;
  getDailyTaskService: any;
  getWeeklyTaskService: any;
  getRoleManagementService: any;
  getConfigService: any;
  image: string | SafeUrl = 'assets/faces/face28.png';

  logout() {
    let body = {
      url: 'logoutService',
    };
    this.callService.postData(body).subscribe((data) => {
      let serviceReturn = data.json();
      let statusService = serviceReturn['0'].serviceContent.status;
      if (statusService == 'S') {
        sessionStorage.clear();
        this.router.navigateByUrl('/');
      } else {
        alert('failed logout');
      }
    });
  }
  ngOnInit(): void {
    if (sessionStorage.getItem('status') != null) {
      var berhasilIV = sessionStorage.getItem('status');
      // console.log(JSON.parse(berhasilIV).statusSehat, " ",JSON.parse(berhasilIV).statusWfh );
      if (JSON.parse(berhasilIV).statusSehat == 'SEHAT') {
        this.statusSehat = 'Sehat';
      } else {
        this.statusSehat = JSON.parse(berhasilIV).statusSehat;
      }
      //  else if (JSON.parse(berhasilIV).statusSehat == "SAKIT") {
      // }
      // if (JSON.parse(berhasilIV).statusWfh == "WFH") {
      // console.log(JSON.parse(berhasilIV).statusWfh);
      if (JSON.parse(berhasilIV).statusWfh == 'WFO') {
        this.statusWfh = 'Work From Office';
      } else if (JSON.parse(berhasilIV).statusWfh == 'WFH') {
        this.statusWfh = 'Work From Home';
      } else {
        let splitwfh = JSON.parse(berhasilIV).statusWfh.split(' - ', 2);
        this.statusWFHReason = splitwfh[1];
        this.statusWfh = splitwfh[0];
      }
    }

    var aesDecryptIV = this.decryptMe.aesDecryptIV(
      sessionStorage.getItem('ekrptd1')
    );
    var aesDecryptSalt = this.decryptMe.aesDecryptSalt(
      sessionStorage.getItem('ekrtdp2')
    );
    var aesDecrypt = this.decryptMe.aesDecrypt(
      sessionStorage.getItem('usdt'),
      aesDecryptIV,
      aesDecryptSalt
    );
    var usercred = aesDecrypt;
    // console.log(JSON.parse(usercred));
    this.userNameFull = JSON.parse(aesDecrypt).fullnameResult;
    this.roleNameResult = JSON.parse(aesDecrypt).roleNameResult;
    this.userImageResult = JSON.parse(aesDecrypt).userImageResult;
    // console.log(this.userImageResult);
    if (this.userImageResult == '-') {
      this.userImageResult = this.image;
    }
    this.teamCode = JSON.parse(aesDecrypt).teamCode;
    // console.log(this.userImageResult);
    this.inquirymyTeam = {};
    this.getUserManagementService = {};
    this.createProjectManagementService = {};
    this.getTeamManagementService = {};
    this.getDailyTaskService = {};
    this.getWeeklyTaskService = {};
    this.getRoleManagementService = {};
    this.getConfigService = {};
    this.inquirymyTeam = JSON.parse(
      this.checkUserCred.checkUserCred(usercred, 'inquiryTeamService')
    );
    this.getUserManagementService = JSON.parse(
      this.checkUserCred.checkUserCred(usercred, 'getUserManagementService')
    );
    this.createProjectManagementService = JSON.parse(
      this.checkUserCred.checkUserCred(
        usercred,
        'createProjectManagementService'
      )
    );
    this.getTeamManagementService = JSON.parse(
      this.checkUserCred.checkUserCred(usercred, 'getTeamManagementService')
    );
    this.getDailyTaskService = JSON.parse(
      this.checkUserCred.checkUserCred(usercred, 'getDailyTaskService')
    );
    this.getWeeklyTaskService = JSON.parse(
      this.checkUserCred.checkUserCred(usercred, 'getWeeklyTaskService')
    );
    this.getRoleManagementService = JSON.parse(
      this.checkUserCred.checkUserCred(usercred, 'getRoleManagementService')
    );
    this.getConfigService = JSON.parse(
      this.checkUserCred.checkUserCred(usercred, 'getConfigService')
    );
  }
}
