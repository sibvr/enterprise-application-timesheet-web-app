import { Component, OnInit, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AppDecryptMe } from '../app.DecryptMe';
import { CallServiceFunction } from '../callServiceFunction';
import { CheckusercredService } from '../checkusercred.service';
import { AppService } from '../app.service';
import { SafeUrl } from '@angular/platform-browser';
declare var $: any;
@Injectable({
  providedIn: 'root',
})
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  constructor(
    private appService: AppService,
    private router: Router,
    public decryptMe: AppDecryptMe,
    public checkUserCred: CheckusercredService,
    private callService: CallServiceFunction
  ) {}

  toggle() {
    $('.sidebar-offcanvas').toggleClass('active');
  }
  logout() {
    let body = {
      url: 'logoutService',
    };
    this.callService.postData(body).subscribe((data) => {
      let serviceReturn = data.json();
      let statusService = serviceReturn['0'].serviceContent.status;
      if (statusService == 'S') {
        sessionStorage.clear();
        this.router.navigateByUrl('/');
      } else {
        alert('failed logout');
      }
    });
  }
  image: string | SafeUrl = 'assets/faces/face28.png';
  usernameResult: any;
  getUserManagementService: any;
  getRoleManagementService: any;
  getConfigService: any;
  statusSehat: any;
  userImageResult: any;
  userId: any;
  ngOnInit(): void {
    if (
      sessionStorage.getItem('ekrptd1') == null &&
      sessionStorage.getItem('ekrtdp2') == null &&
      sessionStorage.getItem('usdt') == null
    ) {
      this.router.navigateByUrl('/');
    }
    if (sessionStorage.getItem('status') != null) {
      let ssion = sessionStorage.getItem('status');
      this.statusSehat = JSON.parse(ssion).statusSehat;
    }
    // let ssion = sessionStorage.getItem("status");
    // this.statusSehat =JSON.parse(ssion).statusSehat
    if (this.statusSehat == null) {
      // console.log(this.statusSehat);
    }
    this.appService.setUserLoggedIn(true);

    var aesDecryptIV = this.decryptMe.aesDecryptIV(
      sessionStorage.getItem('ekrptd1')
    );
    var aesDecryptSalt = this.decryptMe.aesDecryptSalt(
      sessionStorage.getItem('ekrtdp2')
    );
    var aesDecrypt = this.decryptMe.aesDecrypt(
      sessionStorage.getItem('usdt'),
      aesDecryptIV,
      aesDecryptSalt
    );
    this.usernameResult = JSON.parse(aesDecrypt).usernameResult;
    this.userImageResult = JSON.parse(aesDecrypt).userImageResult;
    // console.log(this.userImageResult);
    if (this.userImageResult == '-') {
      this.userImageResult = this.image;
    }
    var usercred = aesDecrypt;
    // console.log("usercred : "+usercred);
    this.getUserManagementService = {};
    this.getRoleManagementService = {};
    this.getConfigService = {};

    this.getUserManagementService = JSON.parse(
      this.checkUserCred.checkUserCred(usercred, 'getUserManagementService')
    );
    this.getRoleManagementService = JSON.parse(
      this.checkUserCred.checkUserCred(usercred, 'getRoleManagementService')
    );
    this.getConfigService = JSON.parse(
      this.checkUserCred.checkUserCred(usercred, 'getConfigService')
    );
    this.userId = JSON.parse(aesDecrypt).userId;
    // console.log("getConfigService :"+ this.checkUserCred.checkUserCred(usercred, "getConfigService"))
    // this.notification();
  }
  lengthNotif: any;
  data = [];
  async notification() {
    await this.callService
      .getData('getCountDailyService?userId=' + this.userId)
      .toPromise()
      .then((data) => {
        let serviceReturn = data;
        this.data = [];
        console.log(JSON.stringify(serviceReturn));
        let statusService = serviceReturn['0'].serviceContent.status;
        if (statusService == 'S') {
          this.lengthNotif =
            serviceReturn['0'].serviceContent.serviceOutput.length;
          for (
            let i = 0;
            i < serviceReturn['0'].serviceContent.serviceOutput.length;
            i++
          ) {
            this.data.push(serviceReturn['0'].serviceContent.serviceOutput[i]);
          }
        }
      });
  }

  async gotoTask(date) {
    date = date;
    console.log(date);
  }
}
