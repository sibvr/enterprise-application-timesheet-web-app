import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.css'],
  providers: [DatePipe]
})
export class ClockComponent implements OnInit {
  constructor(private datePipe: DatePipe) { }

  hours: string;
  minutes: string;
  seconds: string;
  day: string;
  month: string;

  private timerId = null;

  ngOnInit() {
    this.setCurrentTime();
    this.timerId = this.updateTime();
  }

  ngOnDestroy() {
    clearInterval(this.timerId);
  }
  today: any
  sekarang:any;
  private setCurrentTime() {
    const time = new Date(Date.now());
    this.month = this.leftPadZero(time.getMonth());
    this.day = this.leftPadZero(time.getDate());
    this.hours = this.leftPadZero(time.getHours());
    this.minutes = this.leftPadZero(time.getMinutes());
    this.seconds = this.leftPadZero(time.getSeconds());
    this.sekarang = new Date(time.getFullYear(), time.getMonth(), time.getDate(), time.getHours(), time.getMinutes(), time.getSeconds());
    this.today = this.datePipe.transform(this.sekarang, 'EEEE, MMMM d, y');
    // this.hours = this.leftPadZero(time.getHours());
    // this.minutes = this.leftPadZero(time.getMinutes());
    // this.seconds = this.leftPadZero(time.getSeconds());
  }

  private updateTime() {
    setInterval(() => {
      this.setCurrentTime();
    }, 1000);
  }
  private leftPadZero(value: number) {
    return value < 10 ? `0${value}` : value.toString();
  }
}
