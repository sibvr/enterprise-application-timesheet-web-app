import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class AppDateChanger {
  
  dateChanger(date:String){
  	var year = date.substr(-4);
  	var fDate;
  	if (date.search('January') == 0) {
  		return year.concat("01"); 
  	} else if(date.search("February") == 0){
  		return year.concat("02");
  	} else if(date.search("March") == 0){
  		return year.concat("03");
  	} else if(date.search("April") == 0){
  		return year.concat("04");
  	} else if(date.search("May") == 0){
  		return year.concat("05");
  	} else if(date.search("June") == 0){
  		return year.concat("06");
  	} else if(date.search("July") == 0){
  		return year.concat("07");
  	} else if(date.search("August") == 0){
  		return year.concat("08");
  	} else if(date.search("September") == 0){
  		return year.concat("09");
  	} else if(date.search("October") == 0){
  		return year.concat("10");	
  	} else if(date.search("November") == 0){
  		return year.concat("11");
  	} else if(date.search("December") == 0){
		return year.concat("12");
  	}
  }

  dateChangerMonth(date:String){

  	var fDate;
  	if (date.search('January') == 0) {
  		return 1; 
  	} else if(date.search("February") == 0){
  		return 2;
  	} else if(date.search("March") == 0){
  		return 3;
  	} else if(date.search("April") == 0){
  		return 4;
  	} else if(date.search("May") == 0){
  		return 5;
  	} else if(date.search("June") == 0){
  		return 6;
  	} else if(date.search("July") == 0){
  		return 7;
  	} else if(date.search("August") == 0){
  		return 8;
  	} else if(date.search("September") == 0){
  		return 9;
  	} else if(date.search("October") == 0){
  		return 10;	
  	} else if(date.search("November") == 0){
  		return 11;
  	} else if(date.search("December") == 0){
		return 12;
  	}
  }


    dateChanger2(date:String){

    var fullDate = date.substring(0,15);

    var fDate = date.substring(8,10);
    //  console.log(fullDate);
    var fYear = fullDate.substr(-4);
    if (fullDate.substring(4,7) == 'Jan') {
      return fYear+"01"+fDate; 
    } 
    else if(fullDate.substring(4,7) == 'Feb'){
      return fYear+"02"+fDate;
   }
       else if(fullDate.substring(4,7) == 'Mar'){
      return fYear+"03"+fDate;
   }
    else if(fullDate.substring(4,7) == 'Apr'){
      return fYear+"04"+fDate;
   }   
    else if(fullDate.substring(4,7) == 'May'){
      return fYear+"05"+fDate;
   }   
    else if(fullDate.substring(4,7) == 'Jun'){
      return fYear+"06"+fDate;
   }
    else if(fullDate.substring(4,7) == 'Jul'){
      return fYear+"07"+fDate;
   }
     else if(fullDate.substring(4,7) == 'Aug'){
      return fYear+"08"+fDate;
   }  
    else if(fullDate.substring(4,7) == 'Sep'){
      return fYear+"09"+fDate;
   }   
    else if(fullDate.substring(4,7) == 'Oct'){
      return fYear+"10"+fDate;
   }   
    else if(fullDate.substring(4,7) == 'Nov'){
      return fYear+"11"+fDate;
   }   
    else if(fullDate.substring(4,7) == 'Des'){
      return fYear+"12"+fDate;
   }   
  }

    reverseDateChanger2(date:String){

    var fyear = date.substring(0,4);

    var fmonth = date.substring(4,6);
     
    var fdate = date.substr(6,8);

    if (fmonth == '01') {
      return fdate+" January "+fyear; 
    } 
    else if(fmonth == '02'){
      return fdate+" February "+fyear;
   }
       else if(fmonth == '03'){
      return fdate+" March "+fyear;
   }
    else if(fmonth == '04'){
      return fdate+" April "+fyear;
   }   
    else if(fmonth == '05'){
      return fdate+" May "+fyear;
   }   
    else if(fmonth == '06'){
      return fdate+" June "+fyear;
   }
    else if(fmonth == '07'){
      return fdate+" July "+fyear;
   }
     else if(fmonth == '08'){
      return fdate+" August "+fyear;
   }  
    else if(fmonth == '09'){
      return fdate+" September "+fyear;
   }   
    else if(fmonth == '10'){
      return fdate+" October "+fyear;
   }   
    else if(fmonth == '11'){
      return fdate+" November "+fyear;
   }   
    else if(fmonth == '12'){
      return fdate+" December "+fyear;
   }   
  }



  reverbDateChanger(date:String){
  	let year = date.substring(0,4);

  	if (date.substring(4,6) == '01') {
  		return "January".concat(" ", year);
  	} else if(date.substring(4,6) == '02'){
  		return "February".concat(" ", year);
  	} else if(date.substring(4,6) == '03'){
  		return "March".concat(" ", year);
  	} else if(date.substring(4,6) == '04'){
  		return "April".concat(" ", year);
  	} else if(date.substring(4,6) == '05'){
  		return "May".concat(" ", year);
  	} else if(date.substring(4,6) == '06'){
  		return "June".concat(" ", year);
  	} else if(date.substring(4,6) == '07'){
  		return "July".concat(" ", year);
  	} else if(date.substring(4,6) == '08'){
  		return "August".concat(" ", year);
  	} else if(date.substring(4,6) == '09'){
  		return "September".concat(" ", year);
  	} else if(date.substring(4,6) == '10'){
  		return "October".concat(" ", year);
  	} else if(date.substring(4,6) == '11'){
  		return "November".concat(" ", year);
  	} else if(date.substring(4,6) == '12'){
		return "December".concat(" ", year);
  	}
  	
  }
}