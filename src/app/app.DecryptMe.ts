import { Injectable, OnInit } from '@angular/core';
import * as CryptoJS from 'crypto-js';
import { Router } from '@angular/router';
// import { CallServiceFunction } from './callServiceFunction';
import { UserIdleService } from 'angular-user-idle';
import { fromEvent } from 'rxjs';
// import { GetusercredService } from './getusercred.service';

@Injectable({
  providedIn: 'root',
})
export class AppDecryptMe {
  constructor(public router: Router, private userIdle: UserIdleService) {}

  aesDecryptIV(cipher) {
    var passPhrase = 'abcdefghij';
    var cipher = cipher;
    var keySize = 128 / 32;
    var iterationCount = 10;
    // console.info(cipher);
    var iv = 'BLN0eyNx9PumhJ6J';
    var salt = 'cklEYL5gjMfIvoa5';

    var key = CryptoJS.PBKDF2(passPhrase, CryptoJS.enc.Hex.parse(salt), {
      keySize: keySize,
      iterations: iterationCount,
    });
    // console.log(key);

    var decrypted = CryptoJS.AES.decrypt(cipher, key, {
      iv: CryptoJS.enc.Hex.parse(iv),
    });
    // console.log(decrypted);

    if (decrypted.toString(CryptoJS.enc.Utf8) == '') {
      this.wrongUser();
    } else {
      return decrypted.toString(CryptoJS.enc.Utf8);
    }
  }

  aesDecryptSalt(cipher) {
    var passPhrase = 'abcdefghij';
    var cipher = cipher;
    var keySize = 128 / 32;
    var iterationCount = 10;

    var iv = '1FiOlmemXxT6dPVS';
    //var iv = "3FiOlmemXxT5dPVS"
    var salt = 'xmwLGUjMInJp3PgA';

    var key = CryptoJS.PBKDF2(passPhrase, CryptoJS.enc.Hex.parse(salt), {
      keySize: keySize,
      iterations: iterationCount,
    });
    // console.log(key);

    var decrypted = CryptoJS.AES.decrypt(cipher, key, {
      iv: CryptoJS.enc.Hex.parse(iv),
    });
    // console.log(decrypted.toString(CryptoJS.enc.Utf8));
    if (decrypted.toString(CryptoJS.enc.Utf8) == '') {
      this.wrongUser();
    } else {
      return decrypted.toString(CryptoJS.enc.Utf8);
    }
  }

  aesDecrypt(cipher, iv, salt) {
    var passPhrase = 'abcdefghij';
    var cipher = cipher;
    var keySize = 128 / 32;
    var iterationCount = 10;

    var iv = iv;
    var salt = salt;

    var key = CryptoJS.PBKDF2(passPhrase, CryptoJS.enc.Hex.parse(salt), {
      keySize: keySize,
      iterations: iterationCount,
    });

    var decrypted = CryptoJS.AES.decrypt(cipher, key, {
      iv: CryptoJS.enc.Hex.parse(iv),
    });

    if (decrypted.toString(CryptoJS.enc.Utf8) == '') {
      this.wrongUser();
    } else {
      return decrypted.toString(CryptoJS.enc.Utf8);
    }
  }

  wrongUser() {
    sessionStorage.removeItem('usdt');
    sessionStorage.removeItem('ekrptd1');
    sessionStorage.removeItem('ekrtdp2');
    this.router.navigateByUrl('');
  }

  startTimeOut() {
    this.userIdle.startWatching();

    // Start watching when user idle is starting.
    this.userIdle.onTimerStart().subscribe((count) => console.log(count));

    // Start watch when time is up.
    this.userIdle.onTimeout().subscribe(() => console.log('Time is up!'));
  }
  stop() {
    this.userIdle.stopTimer();
  }

  stopWatching() {
    this.userIdle.stopWatching();
  }

  startWatching() {
    this.userIdle.startWatching();
  }

  restart() {
    this.userIdle.resetTimer();
  }
  // roleId: any;
  // userId:any;
  // logout()
  // {
  //   sessionStorage.removeItem("usdt");
  //   sessionStorage.removeItem("ekrptd1");
  //   sessionStorage.removeItem("ekrtdp2");

  //   let body2 = {
  //     "roleId": 10,
  //     "userId":10,
  //     "apiName":"Logout Service",
  //     "url":"logoutService"
  //   };

  //   this.callService.getRestResult(body2).subscribe((res) =>{
  //     let serviceReturn=res.json();
  //     let statusService=serviceReturn["0"].serviceContent.status;

  //     if(statusService=="S")
  //     {
  //          // this.router.navigateByUrl("/");
  //     }
  //     /*else if(statusService=="L"){
  //       // this.router.navigateByUrl("/");
  //     }*/
  //     else{
  //       // this.openModalInformation("Failed","Process Failed",0);
  //     }

  //     this.router.navigateByUrl('/');

  //   })
  // }
}
