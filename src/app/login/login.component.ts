import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, RequestOptions } from '@angular/http';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { CallServiceFunction } from './../callServiceFunction';
import { NgForm } from '@angular/forms';
import { AppDecryptMe } from '../app.DecryptMe';
import { EncryptComponent } from '../encrypt.component';
declare function showSwal(type, title, text): any;

import * as CryptoJS from 'crypto-js';
declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  title = 'timesheet-reportBV';
  idleState = 'Not started.';
  timedOut = false;
  lastPing?: Date = null;

  constructor(
    public encrypt: EncryptComponent,
    public router: Router,
    public http: Http,
    private callService: CallServiceFunction,
    public decryptMe: AppDecryptMe
  ) {}

  bodyTag: HTMLBodyElement = document.getElementsByTagName('body')[0];

  iv: any;
  salt: any;

  serviceReturn: any;
  statusService: any;
  errMess: any;

  roleTaskList = [];
  localstorageuser: {};
  roleTaskListJson: {};
  localstoragestatus: {};
  stringLocalStorage: string;
  stringLocalStoragestatus: string;

  loginForm = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [
      Validators.required,
      //Validators.pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$")
    ]),
  });

  forgetPass = new FormGroup({
    usernameemail: new FormControl('', [Validators.required]),
  });

  ngOnDestroy() {
    // remove the the body classes
    this.bodyTag.removeAttribute('style');
  }
  Encrypt: any;
  roleName: any;
  usernameNow: any;
  passwordNow: string;
  loadingBar = 'false';
  login() {
    this.loadingBar = 'true';
    this.usernameNow = this.loginForm.controls['username'].value;
    this.passwordNow = this.loginForm.controls['password'].value;
    var Encrypt: any;
    Encrypt = this.encrypt.encryptUsingAES256(this.passwordNow).toString();
    // console.log(Encrypt);
    let body2 = {
      username: this.usernameNow,
      password: Encrypt,
      url: 'loginService',
    };
    // console.log(body2);
    this.callService.getRestResultLogin(body2).subscribe(
      (result) => {
        this.serviceReturn = result.json();
        // console.log(JSON.stringify(this.serviceReturn));
        this.statusService = this.serviceReturn['0'].serviceContent.status;

        if (this.statusService == 'S') {
          this.localstorageuser = {};
          for (
            let i = 0;
            i < this.serviceReturn['0'].serviceContent.roleTaskList.length;
            i++
          ) {
            this.roleTaskListJson = {};
            this.roleTaskListJson['taskCode'] =
              this.serviceReturn['0'].serviceContent.roleTaskList[i].taskCode;
            this.roleTaskListJson['taskName'] =
              this.serviceReturn['0'].serviceContent.roleTaskList[i].taskName;
            this.roleTaskListJson['roleTaskId'] =
              this.serviceReturn['0'].serviceContent.roleTaskList[i].roleTaskId;
            this.roleTaskListJson['taskId'] =
              this.serviceReturn['0'].serviceContent.roleTaskList[i].taskId;
            this.roleTaskList.push(this.roleTaskListJson);
          }
          this.localstorageuser['teamCode'] =
            this.serviceReturn['0'].serviceContent.teamCode;
          this.localstorageuser['userImageResult'] =
            this.serviceReturn['0'].serviceContent.userImageResult;
          this.localstorageuser['usernameResult'] =
            this.serviceReturn['0'].serviceContent.usernameResult;
          this.localstorageuser['fullnameResult'] =
            this.serviceReturn['0'].serviceContent.fullnameResult;
          this.localstorageuser['userId'] =
            this.serviceReturn['0'].serviceContent.id;
          this.localstorageuser['access_token'] =
            this.serviceReturn['0'].serviceContent.access_token;
          this.localstorageuser['refresh_token'] =
            this.serviceReturn['0'].serviceContent.refresh_token;
          this.localstorageuser['user_token'] =
            this.serviceReturn['0'].serviceContent.user_token;
          this.localstorageuser['roleNameResult'] =
            this.serviceReturn['0'].serviceContent.rolenameResult;
          this.localstorageuser['roleTaskList'] = this.roleTaskList;

          this.stringLocalStorage = JSON.stringify(this.localstorageuser);
          var Encrypt: any;
          Encrypt = this.encrypt.aesEncrypt(this.stringLocalStorage);
          var decryptSalt = this.encrypt.aesEncryptSalt(this.encrypt.salt);
          var decryptIV = this.encrypt.aesEncryptIV(this.encrypt.iv);
          console.log(decryptSalt);
          console.log(decryptIV);

          sessionStorage.setItem('usdt', Encrypt);
          sessionStorage.setItem('ekrptd1', decryptIV);
          sessionStorage.setItem('ekrtdp2', decryptSalt);

          var aesDecryptIV = this.decryptMe.aesDecryptIV(
            sessionStorage.getItem('ekrptd1')
          );
          var aesDecryptSalt = this.decryptMe.aesDecryptSalt(
            sessionStorage.getItem('ekrtdp2')
          );
          var aesDecrypt = this.decryptMe.aesDecrypt(
            sessionStorage.getItem('usdt'),
            aesDecryptIV,
            aesDecryptSalt
          );
          this.roleName = JSON.parse(aesDecrypt).roleNameResult;
          // console.log(JSON.parse(aesDecrypt).roleTaskList);

          this.statusSehatWfh();
        } else if (this.statusService == 'L') {
          $('#exampleModal-5').modal('show');
        } else {
          showSwal(
            'warning',
            'Failed Login',
            this.serviceReturn['0'].serviceContent.errorMessage.toString()
          );
          this.loadingBar = 'false';
        }
      },
      (error) => {
        alert(error);
      },
      () => {
        // 'onCompleted' callback.
        // No errors, route to new page here
        // this.router.navigateByUrl("/dailytask");
      }
    );
  }
  resumeLogin() {
    var Encrypt: any;
    Encrypt = this.encrypt.encryptUsingAES256(this.passwordNow).toString();
    let body = {
      username: this.usernameNow,
      password: Encrypt,
      url: 'kickUserLoginService',
    };
    this.callService.getRestResultLogin(body).subscribe((data) => {
      let serviceReturn = data.json();
      // console.log(JSON.stringify(serviceReturn));

      let statusService = serviceReturn['0'].serviceContent.status;
      if (statusService == 'S') {
        this.login();
      }
    });
  }

  statusWfh: any;
  statusSehat: any;
  statusWorkLength: any;
  countDown: any;
  statusSehatWfh() {
    if (
      sessionStorage.getItem('ekrptd1') != null &&
      sessionStorage.getItem('ekrtdp2') != null &&
      sessionStorage.getItem('usdt') != null
    ) {
      var aesDecryptIV = this.decryptMe.aesDecryptIV(
        sessionStorage.getItem('ekrptd1')
      );
      var aesDecryptSalt = this.decryptMe.aesDecryptSalt(
        sessionStorage.getItem('ekrtdp2')
      );
      var aesDecrypt = this.decryptMe.aesDecrypt(
        sessionStorage.getItem('usdt'),
        aesDecryptIV,
        aesDecryptSalt
      );
      this.userId = JSON.parse(aesDecrypt).userId;
    }
    if (sessionStorage.getItem('status') == null) {
      this.callService
        .getData('getWorkingService?userId=' + this.userId)
        .subscribe((dataNavbar) => {
          let serviceReturnNavbar = dataNavbar;
          let statusService = serviceReturnNavbar['0'].serviceContent.status;
          // console.log(serviceReturnNavbar['0'].serviceContent.count);
          // console.log(statusService);
          // console.log(this.localstorageuser['roleNameResult']);
          // console.log(JSON.stringify(serviceReturnNavbar));
          if (statusService == 'S') {
            if (serviceReturnNavbar['0'].serviceContent.count == 0) {
              //manager
              if (
                this.localstorageuser['roleNameResult'] == 'STAFF' ||
                this.localstorageuser['roleNameResult'] == 'ADMIN-STAFF'
              ) {
                this.router.navigateByUrl('/dailytask');
              } else {
                this.router.navigateByUrl('/dasboardmanager');
              }
            } else {
              this.localstoragestatus = {};
              this.statusWorkLength =
                serviceReturnNavbar['0'].serviceContent.serviceOutput.length;

              for (
                let i = 0;
                i <
                serviceReturnNavbar['0'].serviceContent.serviceOutput.length;
                i++
              ) {
                if (
                  serviceReturnNavbar['0'].serviceContent.serviceOutput[i]
                    .statusWFH == 'WFH'
                ) {
                  this.statusWfh = 'WFH';
                } else if (
                  serviceReturnNavbar['0'].serviceContent.serviceOutput[i]
                    .statusWFH == 'WFO'
                ) {
                  this.statusWfh = 'WFO';
                } else {
                  this.statusWfh =
                    serviceReturnNavbar['0'].serviceContent.serviceOutput[
                      i
                    ].statusWFH;
                }
                if (
                  serviceReturnNavbar['0'].serviceContent.serviceOutput[i]
                    .statusKesehatan == 'SEHAT'
                ) {
                  this.statusSehat = 'SEHAT';
                } else {
                  this.statusSehat =
                    serviceReturnNavbar['0'].serviceContent.serviceOutput[
                      i
                    ].statusKesehatan;
                }
              }
              // console.log(this.statusSehat);
              this.localstoragestatus['statusWfh'] = this.statusWfh;
              this.localstoragestatus['statusSehat'] = this.statusSehat;
              this.stringLocalStoragestatus = JSON.stringify(
                this.localstoragestatus
              );
              sessionStorage.setItem('status', this.stringLocalStoragestatus);

              //manager
              if (
                this.localstorageuser['roleNameResult'] == 'STAFF' ||
                this.localstorageuser['roleNameResult'] == 'ADMIN-STAFF'
              ) {
                this.loadingBar = 'false';
                this.router.navigateByUrl('/dailytask');
              } else {
                this.loadingBar = 'false';
                this.router.navigateByUrl('/dasboardmanager');
              }
            }
          } else {
            showSwal(
              'warning',
              'Tambah tugas',
              serviceReturnNavbar['0'].serviceContent.errorMessage.toString()
            );
          }
        });
    } else {
      //manager
      if (
        this.localstorageuser['roleNameResult'] == 'STAFF' ||
        this.localstorageuser['roleNameResult'] == 'ADMIN-STAFF'
      ) {
        this.router.navigateByUrl('/dailytask');
      } else {
        this.router.navigateByUrl('/dasboardmanager');
      }
    }
  }

  lupaPassword() {
    let username = this.forgetPass.controls['usernameemail'].value;
    // console.log(username);
    let keyConcat = username
      .concat('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
      .concat(username);
    // let rndmstrpass = this.encrypt.encryptUsingAES2561(pass).toString();
    let outString: string = '';
    let inOptions: string = keyConcat;

    for (let i = 0; i < 10; i++) {
      outString += inOptions.charAt(
        Math.floor(Math.random() * inOptions.length)
      );
    }
    let passEncry = this.encrypt.encryptUsingAES256(outString).toString();
    // console.log(rndmstrpass);
    // console.log(passEncry.concat("<R&Die>").concat(outString));

    let body = {
      username: username,
      randomString: passEncry.concat('<R&Die>').concat(outString),
      url: 'forgotPasswordService',
    };
    this.callService.getRestResultLogin(body).subscribe(
      (data) => {
        let serviceReturn = data.json();
        // console.log(serviceReturn);
        let statusService = serviceReturn['0'].serviceContent.status;
        if (statusService == 'S') {
          this.forgetPass.reset();
          showSwal(
            'success',
            'Reset Password',
            'Password berhasil di reset, silahkan cek Email'
          );
        } else {
          this.forgetPass.reset();
          showSwal(
            'warning',
            'Reset Password',
            serviceReturn['0'].serviceContent.errorMessage.toString()
          );
        }
      },
      (err) => {
        showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ');
      }
    );
  }

  adminstaff = 'ADMIN-STAFF';
  manager = 'MANAGER';
  sechead = 'SEC-HEAD';
  staff = 'STAFF';
  userId: any;
  ngOnInit(): void {
    this.bodyTag.setAttribute('style', 'background-color:powderblue');
    if (sessionStorage.getItem('usdt') != null) {
      var aesDecryptIV = this.decryptMe.aesDecryptIV(
        sessionStorage.getItem('ekrptd1')
      );
      var aesDecryptSalt = this.decryptMe.aesDecryptSalt(
        sessionStorage.getItem('ekrtdp2')
      );
      var aesDecrypt = this.decryptMe.aesDecrypt(
        sessionStorage.getItem('usdt'),
        aesDecryptIV,
        aesDecryptSalt
      );
      // console.log(JSON.parse(aesDecrypt));
      if (
        JSON.parse(aesDecrypt).roleNameResult == this.adminstaff ||
        JSON.parse(aesDecrypt).roleNameResult == this.staff
      ) {
        // this.router.navigateByUrl("/dailytask");
        // this.router.navigated = false;
        this.router.navigateByUrl('/dailytask');
        // window.location.reload();
      } else {
        this.router.navigateByUrl('/dasboardmanager');
      }
      // this.roleName = JSON.parse(aesDecrypt).roleNameResult;
      // this.router.navigateByUrl("/home");
    }
  }
}
