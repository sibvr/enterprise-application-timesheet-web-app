import { Component, OnInit, Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Http, Headers, RequestOptions } from '@angular/http';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AppDecryptMe } from "../../app.DecryptMe";
import { Observable } from 'rxjs';
import { CallServiceFunction } from './../../callServiceFunction';
import { DatePipe } from '@angular/common';
import { CekCredential } from '../../cekcredential';


declare var $: any;

@Component({
  selector: 'app-weekly-team',
  templateUrl: './weekly-team.component.html',
  styleUrls: ['./weekly-team.component.css'],
  providers: [DatePipe]
})
export class WeeklyTeamComponent implements OnInit {

  constructor(private cekCredential: CekCredential, public router: Router, private route: ActivatedRoute, public http: Http, private datePipe: DatePipe, private callService: CallServiceFunction, public decryptMe: AppDecryptMe) {

  }

  weeklyTaskForm = new FormGroup({
    week: new FormControl('1', [
      Validators.required,
    ]),
    period: new FormControl('', [
      Validators.required,
    ]),

  })


  userId: any;
  today: any;
  private sub: any;
  idMember: number;
  fullnamePerson: string;
  loadingData : boolean;
  loadingDetail : boolean;
  ngOnInit(): void {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        $('#datepicker-popup2').datepicker({
          format: "mm-yyyy",
          viewMode: "months",
          autoclose: true,
          minViewMode: "months"
        });

        var aesDecryptIV = this.decryptMe.aesDecryptIV(sessionStorage.getItem("ekrptd1"));
        var aesDecryptSalt = this.decryptMe.aesDecryptSalt(sessionStorage.getItem("ekrtdp2"));
        var aesDecrypt = this.decryptMe.aesDecrypt(sessionStorage.getItem("usdt"), aesDecryptIV, aesDecryptSalt);
        this.userId = JSON.parse(aesDecrypt).userId;


        let date = new Date();
        let month = date.getMonth();
        let day = date.getDate();
        let hour = date.getHours();
        let minute = date.getMinutes();
        let secon = date.getSeconds();
        var sekarang = new Date(date.getFullYear(), month);
        this.today = this.datePipe.transform(sekarang, 'yyyyMM');


        //get param
        this.sub = this.route.params.subscribe(par => {
          this.idMember = par['idMember'];
          // console.log("id member : " + this.idMember);

          this.fullnamePerson = par['fullname'];
          // console.log("fullname : " + this.fullnamePerson);

        });
      }
    });
  }


  flagPeriod: boolean;
  searchWeeklyTask() {
    this.loadingData = true;
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        this.flagPeriod = false;
        let flag = false;

        if (!$('#periodeSearch').first().val()) {
          // console.log("error period");
          flag = true;
          this.flagPeriod = true;
        }

        if (this.weeklyTaskForm.controls['week'].invalid) {
          flag = true;
        }

        if (!flag) {
          // console.log("submit");
          var week = this.weeklyTaskForm.controls['week'].value;

          var dateMonth1 = $('#periodeSearch').first().val().toString().substring(0, 2);
          var dateYear1 = $('#periodeSearch').first().val().toString().substring(3, 7);
          let period = dateYear1 + dateMonth1;

          let params = "?userId=" + this.userId + "&byUserId=" + this.idMember + "&byDate=" + week + period;
          this.inquiryWeeklyTask(params);

        }

      }
    });

  }


  workingHourPlan: number;
  workingHourActual: number;
  datetimeTask = [];
  planOfDay = [];
  actualOfDay = [];
  dailyTaskData = [];
  count: any;
  inquiryWeeklyTask(params) {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        let serviceReturn: any;
        let statusService: string;
        let errMess: string;
        let url = "getSumActiveDailyTasByWeekService";
        // console.log(url + params);
        //refresh array
        this.datetimeTask = [];
        this.planOfDay = [];
        this.actualOfDay = [];
        this.dailyTaskData = [];

        this.callService.getData(url + params).subscribe((res) => {

          serviceReturn = res;
          statusService = serviceReturn["0"].serviceContent.status;
          // console.log("hasil status : " + statusService);

          if (statusService == "S") {
            this.workingHourPlan = serviceReturn["0"].serviceContent.workingHourPlan;
            this.workingHourActual = serviceReturn["0"].serviceContent.workingHourActual;
            this.count = serviceReturn["0"].serviceContent.count;

            // console.log("count : " + serviceReturn["0"].serviceContent.count);
            if (serviceReturn["0"].serviceContent.count > 0) {
              for (let i = 0; i < serviceReturn["0"].serviceContent.serviceOutput.length; i++) {
                this.datetimeTask.push(serviceReturn["0"].serviceContent.serviceOutput[i].startDate);
                this.planOfDay.push(serviceReturn["0"].serviceContent.serviceOutput[i].sumPlanInDay);
                this.actualOfDay.push(serviceReturn["0"].serviceContent.serviceOutput[i].sumActualInDay);
                this.dailyTaskData.push(serviceReturn["0"].serviceContent.serviceOutput[i].detailTaskList);

              }

            }

            // console.log("datetimeTask : " + this.datetimeTask);

          } else {

          }
        },
          (err) => { this.router.navigateByUrl('/error500') }
        );

        this.loadingData = false;
      }
    });
  }

  pid = [];
  projectName = [];
  task = [];
  planHours = [];
  actualHours = [];
  listDetail = [];
  dateTask: string;
  inquiryTaskDetail(idxDaily: number) {
    this.loadingDetail = true;
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        // console.log("idxDaily : " + idxDaily);


        //refresh array
        this.pid = [];
        this.projectName = [];
        this.task = [];
        this.planHours = [];
        this.actualHours = [];
        this.listDetail = [];

        this.dateTask = this.datetimeTask[idxDaily];


        // console.log("length task : " + this.dailyTaskData[idxDaily].length);
        for (let j = 0; j < this.dailyTaskData[idxDaily].length; j++) {
          // console.log(this.dailyTaskData[idxDaily][j].pid);
          this.listDetail.push(this.dailyTaskData[idxDaily][j]);
          this.pid.push(this.dailyTaskData[idxDaily][j].pid);
          this.projectName.push(this.dailyTaskData[idxDaily][j].projectName);
          this.task.push(this.dailyTaskData[idxDaily][j].taskDetail);
          this.planHours.push(this.dailyTaskData[idxDaily][j].workingHourPlan);
          this.actualHours.push(this.dailyTaskData[idxDaily][j].workingHourActual);
        };


        this.loadingDetail = false;
      }
    });

  }



}
