import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { WeeklyTeamComponent } from './weekly-team.component';

describe('WeeklyTeamComponent', () => {
  let component: WeeklyTeamComponent;
  let fixture: ComponentFixture<WeeklyTeamComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ WeeklyTeamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeeklyTeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
