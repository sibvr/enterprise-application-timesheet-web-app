import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { CallServiceFunction } from '../callServiceFunction';
import { Router } from '@angular/router';
import { AppDecryptMe } from "../app.DecryptMe";
import { CekCredential } from '../cekcredential';

declare function showSwal(type, title, text): any;
declare var $: any;

@Component({
  selector: 'app-my-team',
  templateUrl: './my-team.component.html',
  styleUrls: ['./my-team.component.css'],
  providers: [DatePipe]
})
export class MyTeamComponent implements OnInit {

  constructor(private cekCredential: CekCredential, private datePipe: DatePipe, private callService: CallServiceFunction, public router: Router, public decryptMe: AppDecryptMe) { }

  today: any;

  listDailyTaskTeam = [];
  userFullname: any;;
  dailyDetail(id, userFullname) {
    this.loadingDaily = true;
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        this.listDailyTaskTeam = [];
        // console.log(id);
        this.callService.getData('getDailyByUserService?userId=' + this.userId + '&parameter=' + id).subscribe((data) => {
          let serviceReturn = data;
          console.log(data);
          let statusService = serviceReturn['0'].serviceContent.status;
          if (statusService == "S") {
            for (let i = 0; i < serviceReturn['0'].serviceContent.serviceOutput.length; i++) {
              this.listDailyTaskTeam.push(serviceReturn['0'].serviceContent.serviceOutput[i]);
              this.userFullname = userFullname;
            }
          }
        })

        this.loadingDaily = false;
      }
    });
  }

  teamName: string;
  idTeam: number;
  listStaff = [];
  getTeamStaff() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        this.callService.getData('getTeamService?userId=' + this.userId + '&parameter=' + this.userId).subscribe((data) => {
          let serviceReturn = data;
          // console.log(data);
          // console.log(serviceReturn);
          let statusService = serviceReturn['0'].serviceContent.status;
          if (statusService == "S") {

            this.idTeam = serviceReturn['0'].serviceContent.teamData.id;
            this.teamName = serviceReturn['0'].serviceContent.teamData.teamName;

            if (serviceReturn['0'].serviceContent.count > 0) {


              for (let i = 0; i < serviceReturn['0'].serviceContent.serviceOutput.length; i++) {
                this.listStaff.push(serviceReturn['0'].serviceContent.serviceOutput[i]);
              }
            }
            // console.log(this.listStaff)
          }
        })

        this.loadingData = false;
      }
    });
  }


  weeklyTask(id, username) {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        // console.log("id : " + id);
        // console.log("username : " + username);
        // this.router.navigateByUrl("/weeklyTeam");
        this.router.navigate(['/weeklyteam', id, username]);
      }
    });
  }


  fullname = [];
  pid = [];
  projectName = [];
  task = [];
  planHours = [];
  actualHours = [];
  listDetailTask = [];
  dateTask: string;
  inquiryGetAllDailyTaskByTeam() {
    this.loadingDetail = true;
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        let date = new Date();
        let month = date.getMonth();
        let day = date.getDate();
        let hour = date.getHours();
        let minute = date.getMinutes();
        let secon = date.getSeconds();
        var sekarang = new Date(date.getFullYear(), month, day, hour, minute, secon);
        this.today = this.datePipe.transform(sekarang, 'EEEE, MMMM d, y');
        this.callService.getData('getDailyByTeamService?userId=' + this.userId + '&parameter=' + this.idTeam).subscribe((data) => {
          let serviceReturn = data;
          let statusService = serviceReturn['0'].serviceContent.status;
          // console.log(serviceReturn);
          if (statusService == "S") {
            if (serviceReturn['0'].serviceContent.count > 0) {
              for (let i = 0; i < serviceReturn['0'].serviceContent.serviceOutput.length; i++) {
                this.listDetailTask.push(serviceReturn['0'].serviceContent.serviceOutput[i]);
              }
              // console.log(this.listDetailTask);
            }
            // // console.log(this.listStaff)
          }
        }, (err) => {
          showSwal('warning', 'Error', err);
          this.ngOnInit();
        }

        )
        this.loadingDetail = false;
      }
    });
  }


  userId: any;
  loadingData = true;
  loadingDetail :boolean;
  loadingDaily :boolean;
  ngOnInit(): void {
    this.cekCredential.getCredential((status) => {

      this.loadingData = true;
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        let date = new Date();
        let month = date.getMonth();
        let day = date.getDate();
        let hour = date.getHours();
        let minute = date.getMinutes();
        let secon = date.getSeconds();
        var sekarang = new Date(date.getFullYear(), month, day, hour, minute, secon);
        this.today = this.datePipe.transform(sekarang, 'EEEE, MMMM d, y');
        var aesDecryptIV = this.decryptMe.aesDecryptIV(sessionStorage.getItem("ekrptd1"));
        var aesDecryptSalt = this.decryptMe.aesDecryptSalt(sessionStorage.getItem("ekrtdp2"));
        var aesDecrypt = this.decryptMe.aesDecrypt(sessionStorage.getItem("usdt"), aesDecryptIV, aesDecryptSalt);
        this.userId = JSON.parse(aesDecrypt).userId;
        this.getTeamStaff();
      }
    });
  }

}
