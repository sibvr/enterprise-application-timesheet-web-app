import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { Http, Headers, RequestOptions } from '@angular/http';
@Injectable({
  providedIn: 'root'
})
export class CheckusercredService {

  constructor(public router:Router, public http:Http) { }
  jsonReturnData:any;
  jsonDataReal:any;
  checkUserCred(jsonData:string, actionName:string):string
  {
  	this.jsonDataReal={};
  	this.jsonReturnData={};
  	this.jsonDataReal = JSON.parse(jsonData);

  	for(let i=0 ; i<this.jsonDataReal.roleTaskList.length ; i++)
  	{
  		if(this.jsonDataReal.roleTaskList[i].taskCode==actionName)
      {
        // console.log("masuk if : "+actionName);
        this.jsonReturnData['taskId']=this.jsonDataReal.roleTaskList[i].taskId;
        this.jsonReturnData['roleTaskId']=this.jsonDataReal.roleTaskList[i].roleTaskId;
        this.jsonReturnData['taskCode']=this.jsonDataReal.roleTaskList[i].taskCode;
        this.jsonReturnData['statusAccess']="Y";
      }
      //console.log("Masuk Else")
    }
    
    // console.log("jsonReturnData : "+JSON.stringify(this.jsonReturnData));
  	return JSON.stringify(this.jsonReturnData);
  }	



}
