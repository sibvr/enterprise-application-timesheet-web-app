import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FormGroup, FormControl, FormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Router } from "@angular/router";
import { ActivatedRoute } from '@angular/router';
import { AppDecryptMe } from "../../app.DecryptMe";
import { CallServiceFunction } from '../../callServiceFunction';
import { AppDateChanger } from "../../app.datechanger";
import { CekCredential } from '../../cekcredential';
import { HttpClient } from '@angular/common/http';
import { EncryptComponent } from "../../encrypt.component";
import { SafeUrl } from "@angular/platform-browser";
import * as CryptoJS from 'crypto-js';

declare function showSwal(type, title, text): any;
declare var $: any;

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  constructor(private httpClient: HttpClient, public encrypt: EncryptComponent, private cekCredential: CekCredential, public http: Http, public router: Router, private callService: CallServiceFunction, public decryptMe: AppDecryptMe) { }

  flag = "true";
  image: string | SafeUrl =
    "assets/faces/face28.png";
  base64textString = [];
  formatType: any;
  max_size = 1500000;
  onUploadChange(evt: any) {
    const file = evt.target.files[0];
    this.formatType = file.type;
    // console.log(file.type);
    if (file && file.size <= this.max_size) {
      const reader = new FileReader();

      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    } else {
      alert("max size");
    }
  }
  compressImage(src, newX, newY) {
    return new Promise((res, rej) => {
      const img = new Image();
      img.src = src;
      img.onload = () => {
        const elem = document.createElement('canvas');
        elem.width = newX;
        elem.height = newY;
        const ctx = elem.getContext('2d');
        ctx.drawImage(img, 0, 0, newX, newY);
        const data = ctx.canvas.toDataURL();
        res(data);
      }
      img.onerror = error => rej(error);
    })
  }
  param: any;
  resizedBase64: any;
  handleReaderLoaded(e) {
    this.base64textString = [];
    this.param = e;
    this.flag = "false";
    this.resizedBase64 = 'data:' + this.formatType + ';base64,' + btoa(this.param.target.result);
    // console.log("asli : "+this.resizedBase64);
    this.compressImage(this.resizedBase64, 450, 450).then(compressed => {
      this.base64textString.push(compressed);
      // console.log(this.base64textString[0]);
    })



  }
  loadingBar="false";
  uploadfoto() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        this.loadingBar="true";
        let body = {
          "updateUserId": this.userId,
          "id": this.userId,
          "image": this.base64textString[0],
          "url": "uploadUserImageService"
        }
        // console.log(body);
        this.callService.putData(body).subscribe((data) => {
          let serviceReturn = data.json();
          let statusService = serviceReturn['0'].serviceContent.status;
          if (statusService == 'S') {
            this.localstorageuser = {}
            this.localstorageuser['usernameResult'] = this.usernameResult;
            this.localstorageuser['fullnameResult'] = this.settingForm.get('fullname').value;
            this.localstorageuser['userId'] = this.userId;
            this.localstorageuser['access_token'] = this.access_token;
            this.localstorageuser['refresh_token'] = this.refresh_token;
            this.localstorageuser['user_token'] = this.user_token;
            this.localstorageuser['roleNameResult'] = this.roleNameResult;
            this.localstorageuser['roleTaskList'] = this.roleTaskList;
            this.localstorageuser['teamCode'] = this.teamCode;
            this.localstorageuser['userImageResult'] = this.base64textString[0];

            this.stringLocalStorage = JSON.stringify(this.localstorageuser);
            var Encrypt: any;
            Encrypt = this.encrypt.aesEncrypt(this.stringLocalStorage);
            var decryptSalt = this.encrypt.aesEncryptSalt(this.encrypt.salt);
            var decryptIV = this.encrypt.aesEncryptIV(this.encrypt.iv);

            sessionStorage.setItem("usdt", Encrypt);
            sessionStorage.setItem("ekrptd1", decryptIV);
            sessionStorage.setItem("ekrtdp2", decryptSalt);
            window.location.reload();
          }
        });
      }
    });
  }
  userId: any;
  usernameResult: any;
  fullnameResult: any;
  access_token: any;
  refresh_token: any;
  user_token: any;
  roleNameResult: any;
  roleTaskList: any;
  userImageResult: any;
  teamCode: any;
  ngOnInit(): void {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        var aesDecryptIV = this.decryptMe.aesDecryptIV(sessionStorage.getItem("ekrptd1"));
        var aesDecryptSalt = this.decryptMe.aesDecryptSalt(sessionStorage.getItem("ekrtdp2"));
        var aesDecrypt = this.decryptMe.aesDecrypt(sessionStorage.getItem("usdt"), aesDecryptIV, aesDecryptSalt);
        this.userId = JSON.parse(aesDecrypt).userId;
        this.usernameResult = JSON.parse(aesDecrypt).usernameResult
        this.fullnameResult = JSON.parse(aesDecrypt).fullnameResult
        this.access_token = JSON.parse(aesDecrypt).access_token
        this.refresh_token = JSON.parse(aesDecrypt).refresh_token
        this.user_token = JSON.parse(aesDecrypt).user_token
        this.roleNameResult = JSON.parse(aesDecrypt).roleNameResult
        this.roleTaskList = JSON.parse(aesDecrypt).roleTaskList
        this.teamCode = JSON.parse(aesDecrypt).teamCode;
        this.userImageResult = JSON.parse(aesDecrypt).userImageResult;
        // console.log(this.userImageResult);
        if (this.userImageResult == "-" || this.userImageResult == "") {
          this.userImageResult = this.image;
        }

        this.usermanagementupdate(this.userId);
      }
    });
  }

  //Form
  settingForm = new FormGroup({
    username: new FormControl('', [
      Validators.required
    ]),
    fullname: new FormControl('', [
      Validators.required
    ]),
    email: new FormControl('', [
      Validators.required
    ]),
    phone: new FormControl('',
      [Validators.required]
    ),
    address: new FormControl('', [
      Validators.required
    ])

  });

  userEditId: number;
  managerId: any;
  roleId: any;
  usermanagementupdate(userEditId) {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {

        this.userEditId = userEditId;
        // console.log(userEditId);

        let serviceReturn: any;
        let statusService: string;

        let url = "getUserManagementService";
        let params = "?userId=" + this.userId + "&parameter=" + userEditId;

        this.callService.getData(url + params).subscribe((res) => {
          serviceReturn = res;
          statusService = serviceReturn["0"].serviceContent.status;
          // console.log("json update : " + JSON.stringify(serviceReturn));
          // console.log("hasil status : " + statusService);

          if (statusService == "S") {
            if (serviceReturn["0"].serviceContent.serviceOutput.length > 0) {

              this.settingForm.controls['fullname'].setValue(serviceReturn["0"].serviceContent.serviceOutput[0].userFullname);
              this.settingForm.controls['username'].setValue(serviceReturn["0"].serviceContent.serviceOutput[0].username);
              this.settingForm.controls['email'].setValue(serviceReturn["0"].serviceContent.serviceOutput[0].email);
              this.settingForm.controls['phone'].setValue(serviceReturn["0"].serviceContent.serviceOutput[0].phone);
              this.settingForm.controls['address'].setValue(serviceReturn["0"].serviceContent.serviceOutput[0].address);
              this.roleId = serviceReturn["0"].serviceContent.serviceOutput[0].roleId;
              this.managerId = serviceReturn["0"].serviceContent.serviceOutput[0].managerId;


            }
          }

        }, (err) => this.router.navigateByUrl('/error500'));

      }
    });

  }
  loadingprof="false";
  stringLocalStorage: string;
  localstorageuser: {};
  updateSettings() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        this.loadingprof="true";
        let body = {
          // "url": "editUserManagementDetailService",
          "url": "updateUserService",
          "roleId": parseInt(this.roleId),
          "managerId": parseInt(this.managerId),
          "username": this.settingForm.get('username').value,
          "email": this.settingForm.get('email').value,
          "id": this.userId,
          "updateUserId": this.userId,
          "flagNewRole": "N",
          "userFullname": this.settingForm.get('fullname').value,
          "phone": this.settingForm.get('phone').value,
          "address": this.settingForm.get('address').value,
          "taskCrudActiveList": []
        };

        // console.log(body);
        this.callService.putData(body).subscribe((data) => {

          let serviceReturn = data.json();
          // console.log("return : " + JSON.stringify(serviceReturn));
          let statusService = serviceReturn['0'].serviceContent.status;
          if (statusService == "S") {
            this.localstorageuser = {}
            this.localstorageuser['usernameResult'] = this.usernameResult;
            this.localstorageuser['fullnameResult'] = this.settingForm.get('fullname').value;
            this.localstorageuser['userId'] = this.userId;
            this.localstorageuser['access_token'] = this.access_token;
            this.localstorageuser['refresh_token'] = this.refresh_token;
            this.localstorageuser['user_token'] = this.user_token;
            this.localstorageuser['roleNameResult'] = this.roleNameResult;
            this.localstorageuser['roleTaskList'] = this.roleTaskList;
            this.localstorageuser['teamCode'] = this.teamCode;
            this.localstorageuser['userImageResult'] = this.userImageResult;

            this.stringLocalStorage = JSON.stringify(this.localstorageuser);
            var Encrypt: any;
            Encrypt = this.encrypt.aesEncrypt(this.stringLocalStorage);
            var decryptSalt = this.encrypt.aesEncryptSalt(this.encrypt.salt);
            var decryptIV = this.encrypt.aesEncryptIV(this.encrypt.iv);

            sessionStorage.setItem("usdt", Encrypt);
            sessionStorage.setItem("ekrptd1", decryptIV);
            sessionStorage.setItem("ekrtdp2", decryptSalt);
            window.location.reload();
            // showSwal('success', 'Perbaharui profil', 'Profil berhasil di perbaharui');
            // this.ngOnInit();
          } else {
            showSwal('warning', 'Perbaharui profil', 'Profil gagal di perbaharui');
          }
        })
      }
    });
  }


}
