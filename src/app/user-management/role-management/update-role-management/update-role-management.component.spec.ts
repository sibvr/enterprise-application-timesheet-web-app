import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { UpdateRoleManagementComponent } from './update-role-management.component';

describe('UpdateRoleManagementComponent', () => {
  let component: UpdateRoleManagementComponent;
  let fixture: ComponentFixture<UpdateRoleManagementComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateRoleManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateRoleManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
