import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FormGroup, FormControl, FormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Router } from "@angular/router";
import { AppDecryptMe } from "../../../app.DecryptMe";
import { CallServiceFunction } from '../../../callServiceFunction';
import { DatePipe } from '@angular/common';
import { AppDateChanger } from "../../../app.datechanger";

declare function showSwal(type, title, text): any;
declare var $: any;

@Component({
  selector: 'app-inquiry-role-management',
  templateUrl: './inquiry-role-management.component.html',
  styleUrls: ['./inquiry-role-management.component.css'],
  providers: [DatePipe]
})
export class InquiryRoleManagementComponent implements OnInit {

  constructor(public http: Http, public router: Router, private callService: CallServiceFunction, private datePipe: DatePipe, private dateChanger: AppDateChanger, public decryptMe: AppDecryptMe) { }

  //pagination
  p: number = 1;
  userId: any;
  datepipe: any;
  ngOnInit(): void {

    var aesDecryptIV = this.decryptMe.aesDecryptIV(sessionStorage.getItem("ekrptd1"));
    var aesDecryptSalt = this.decryptMe.aesDecryptSalt(sessionStorage.getItem("ekrtdp2"));
    var aesDecrypt = this.decryptMe.aesDecrypt(sessionStorage.getItem("usdt"), aesDecryptIV, aesDecryptSalt);
    this.userId = JSON.parse(aesDecrypt).userId;

    this.inquiryRoleList();

  }


  //inquiry filter role
  rolePicker = "null"
  roleList = []
  filterRole() {
    this.roleList = [];

    let serviceReturn: any;
    let statusService: string;

    let url = "getRoleService";
    let params = "?userId=" + this.userId + "&parameter=Y";

    this.callService.getData(url + params).subscribe((res) => {

      serviceReturn = res;
      statusService = serviceReturn["0"].serviceContent.status;

      console.log();

      if (statusService == "S") {
        if (serviceReturn["0"].serviceContent.serviceOutput.length > 0) {
          for (let i = 0; i < serviceReturn["0"].serviceContent.serviceOutput.length; i++) {
            this.roleList.push(serviceReturn["0"].serviceContent.serviceOutput[i]);
          }
        }


      }

    }, (err) => this.router.navigateByUrl('/error500'));

  }

  // INQUIRY ROLE
  roleManagement = [];
  dateRoleCreate = [];
  inquiryRoleList() {

    let serviceReturn: any;
    let statusService: string;

    this.roleManagement = [];

    let url = "getRoleManagementService";
    let params = "?userId=" + this.userId;

    this.callService.getData(url + params).subscribe((res) => {

      serviceReturn = res;
      statusService = serviceReturn["0"].serviceContent.status;

      if (statusService == "S") {

        let day: any;
        let date1: any;
        let date2: any;
        let datepipenew: any;

        console.log("length : " + serviceReturn["0"].serviceContent.serviceOutput.length);
        if (serviceReturn["0"].serviceContent.serviceOutput.length > 0) {
          for (let i = 0; i < serviceReturn["0"].serviceContent.serviceOutput.length; i++) {

            this.roleManagement.push(serviceReturn["0"].serviceContent.serviceOutput[i]);

            //change date format
            this.datepipe = this.dateChanger.reverbDateChanger(serviceReturn["0"].serviceContent.serviceOutput[i].createDatetime.toString());
            day = serviceReturn["0"].serviceContent.serviceOutput[i].createDatetime.substr(6, 2); //day
            date1 = new Date(this.datepipe);
            date2 = date1.toDateString();
            datepipenew = this.datePipe.transform(date2, 'MMMM y'); //month year

            this.roleManagement[i].createDatetime = day + " " + datepipenew; //dd MMMM y


          }

        }

        this.filterRole();
      }


    }, (err) => this.router.navigateByUrl('/error500'));

  }

  //DELETE ROLE
  deactivateRole(roleId){
    let body = {
      "roleId": roleId,
      "updateBy": this.userId,
      "url": "deactivateRoleService"
    };

    let serviceReturn: any;
    let statusService: string;

    console.log("json : "+JSON.stringify(body));

    this.callService.putData(body).subscribe((res) => {

      serviceReturn = res.json();
      statusService = serviceReturn["0"].serviceContent.status;

      console.log("res :" + JSON.stringify(serviceReturn));
      console.log("hasil status delete: " + statusService);

      if (statusService == "S") {
        showSwal('success', 'Success Delete', 'Role ' + this.roleNameDelete + " has been deleted");
        this.inquiryRoleList()

      } else {
        showSwal('warning', 'Delete Failed', 'Unable to delete role');
      }
    })

  }

  //update
  rolemanagementupdate(roleId) {
    this.router.navigateByUrl('/rolemanagementupdate/'+roleId+'');
  }

  //create
  createnewrole() {
    this.router.navigateByUrl('/rolemanagementcreate');
  }

  //Modals delete
  roleIdDelete= "";
  roleNameDelete = "";
  deleteconfirmed(roleId: string, roleName: string) {
    this.roleIdDelete = roleId;
    this.roleNameDelete = roleName;
  }

}
