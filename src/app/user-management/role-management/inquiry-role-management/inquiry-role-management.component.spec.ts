import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InquiryRoleManagementComponent } from './inquiry-role-management.component';

describe('InquiryRoleManagementComponent', () => {
  let component: InquiryRoleManagementComponent;
  let fixture: ComponentFixture<InquiryRoleManagementComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InquiryRoleManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquiryRoleManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
