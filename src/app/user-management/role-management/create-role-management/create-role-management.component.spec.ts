import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CreateRoleManagementComponent } from './create-role-management.component';

describe('CreateRoleManagementComponent', () => {
  let component: CreateRoleManagementComponent;
  let fixture: ComponentFixture<CreateRoleManagementComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateRoleManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateRoleManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
