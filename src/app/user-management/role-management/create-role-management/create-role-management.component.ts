import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FormGroup, FormControl, FormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Router } from "@angular/router";
import { ActivatedRoute } from '@angular/router';
import { AppDecryptMe } from "../../../app.DecryptMe";
import { CallServiceFunction } from '../../../callServiceFunction';
import { AppDateChanger } from "../../../app.datechanger";

declare function showSwal(type, title, text): any;
declare var $: any;

@Component({
  selector: 'app-create-role-management',
  templateUrl: './create-role-management.component.html',
  styleUrls: ['./create-role-management.component.css']
})
export class CreateRoleManagementComponent implements OnInit {

  constructor(private _fb: FormBuilder, public http: Http, public router: Router,
    private route: ActivatedRoute, private callService: CallServiceFunction,
    public decryptMe: AppDecryptMe) { }

  
  userId: any;
  ngOnInit(): void {
    var aesDecryptIV = this.decryptMe.aesDecryptIV(sessionStorage.getItem("ekrptd1"));
    var aesDecryptSalt = this.decryptMe.aesDecryptSalt(sessionStorage.getItem("ekrtdp2"));
    var aesDecrypt = this.decryptMe.aesDecrypt(sessionStorage.getItem("usdt"), aesDecryptIV, aesDecryptSalt);
    this.userId = JSON.parse(aesDecrypt).userId;
    this.inquiryRoleTemplate();
  }

  //FORM
  editDetailRoleForm = new FormGroup({
    roleName: new FormControl('', [
      Validators.required,
    ]),
    roleCode: new FormControl('', [
      Validators.required,
    ])
  })

  //NGmodel
  roleDetail: { roleName: string, roleCode: string } = { roleName: '', roleCode: '' };

  //inquiry role
  taskList = []
  listOfActionCreate = []
  listOfActionDelete = []
  listOfActionRead = []
  listOfActionUpdate = []
  inquiryRoleTemplate() {

    this.taskList = [];

    let serviceReturn: any;
    let statusService: string;

    let url = "getTaskCrudService";
    let params = "?userId=" + this.userId + "&parameter=Y";

    this.callService.getData(url + params).subscribe((res) => {

      serviceReturn = res;
      statusService = serviceReturn["0"].serviceContent.status;
      console.log("statuservice: " + statusService)
      console.log("hasil : " + JSON.stringify(serviceReturn))

      if (statusService == "S") {

        this.roleDetail.roleName = serviceReturn["0"].serviceContent.serviceOutput[0].roleName

        if (serviceReturn["0"].serviceContent.serviceOutput.length > 0) {


          for (let i = 0; i < serviceReturn["0"].serviceContent.serviceOutput.length; i++) {

            this.taskList.push(serviceReturn["0"].serviceContent.serviceOutput[i]);

            if (serviceReturn["0"].serviceContent.serviceOutput[i].createTaskActive == "N") {
              this.listOfActionCreate.push(false)
            }
            if (serviceReturn["0"].serviceContent.serviceOutput[i].createTaskActive == "Y") {
              this.listOfActionCreate.push(true)
            }

            if (serviceReturn["0"].serviceContent.serviceOutput[i].deleteTaskActive == "N") {
              this.listOfActionDelete.push(false)
            }
            if (serviceReturn["0"].serviceContent.serviceOutput[i].deleteTaskActive == "Y") {
              this.listOfActionDelete.push(true)
            }

            if (serviceReturn["0"].serviceContent.serviceOutput[i].readTaskActive == "N") {
              this.listOfActionRead.push(false);
            }
            if (serviceReturn["0"].serviceContent.serviceOutput[i].readTaskActive == "Y") {
              this.listOfActionRead.push(true);
            }
            if (serviceReturn["0"].serviceContent.serviceOutput[i].updateTaskActive == "N") {
              this.listOfActionUpdate.push(false);
            }
            if (serviceReturn["0"].serviceContent.serviceOutput[i].updateTaskActive == "Y") {
              this.listOfActionUpdate.push(true);
            }

          }
        }
      }

    }, (err) => { this.router.navigateByUrl('/error500') })

  }



  //CREATE ROLE
  createNewRole() {

    let serviceReturn: any;
    let statusService: string;


    let packOfRoleUpdate = {};
    let listOfPackRoleUpdate = [];

    //list task
    for (let i = 0; i < this.taskList.length; i++) {

      packOfRoleUpdate = {};


      packOfRoleUpdate['taskCrudId'] = this.taskList[i].taskCrudId;
      packOfRoleUpdate['createTaskId'] = this.taskList[i].createTaskId;
      packOfRoleUpdate['readTaskId'] = this.taskList[i].readTaskId;
      packOfRoleUpdate['updateTaskId'] = this.taskList[i].updateTaskId;
      packOfRoleUpdate['deleteTaskId'] = this.taskList[i].deleteTaskId;

      if (this.listOfActionUpdate[i] == false || this.listOfActionUpdate[i] == null) {
        packOfRoleUpdate['updateTaskActive'] = "N";
      }
      else {
        packOfRoleUpdate['updateTaskActive'] = "Y";
      }


      if (this.listOfActionRead[i] == false || this.listOfActionRead[i] == null) {
        packOfRoleUpdate['readTaskActive'] = "N";
      }
      else {
        packOfRoleUpdate['readTaskActive'] = "Y";
      }

      if (this.listOfActionCreate[i] == false || this.listOfActionCreate[i] == null) {
        packOfRoleUpdate['createTaskActive'] = "N";
      }
      else {
        packOfRoleUpdate['createTaskActive'] = "Y";
      }

      if (this.listOfActionDelete[i] == false || this.listOfActionDelete[i] == null) {
        packOfRoleUpdate['deleteTaskActive'] = "N";
      }
      else {
        packOfRoleUpdate['deleteTaskActive'] = "Y";
      }

      console.log("json input : " + JSON.stringify(packOfRoleUpdate));

      listOfPackRoleUpdate.push(packOfRoleUpdate);

    }


    let body = {
      "flagNewRole": "Y",
      "userId": this.userId,
      "roleCode": this.roleDetail.roleCode,
      "roleName": this.roleDetail.roleName,
      "taskCrudActiveList": listOfPackRoleUpdate,
      "url": "createRoleService"
    };

    this.callService.postData(body).subscribe((res) => {
      serviceReturn = res.json();
      statusService = serviceReturn["0"].serviceContent.status;

      console.log("ini status : " + statusService);

      if (statusService == "S") {
        showSwal('success', 'Success Create Role', 'Role ' + this.roleDetail.roleName + " was successfully created");
        this.router.navigateByUrl("/rolemanagement");
      } else {
        showSwal('warning', 'Create Role Failed', 'Unable to create role');
      }
      
    },
      (err) => { this.router.navigateByUrl('/error500') })
  }




}
