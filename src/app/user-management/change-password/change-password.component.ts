import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FormGroup, FormControl, FormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Router } from "@angular/router";
import { ActivatedRoute } from '@angular/router';
import { AppDecryptMe } from "../../app.DecryptMe";
import { CallServiceFunction } from '../../callServiceFunction';
import { AppDateChanger } from "../../app.datechanger";
import { CekCredential } from '../../cekcredential';
import { EncryptComponent } from "../../encrypt.component";
declare function showSwal(type, title, text): any;
import * as CryptoJS from 'crypto-js';
declare var $: any;

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  constructor(public encrypt: EncryptComponent, private cekCredential: CekCredential, private _fb: FormBuilder, public http: Http, public router: Router,
    private route: ActivatedRoute, private callService: CallServiceFunction,
    public decryptMe: AppDecryptMe) { }

  userId: any;
  ngOnInit(): void {
    var aesDecryptIV = this.decryptMe.aesDecryptIV(sessionStorage.getItem("ekrptd1"));
    var aesDecryptSalt = this.decryptMe.aesDecryptSalt(sessionStorage.getItem("ekrtdp2"));
    var aesDecrypt = this.decryptMe.aesDecrypt(sessionStorage.getItem("usdt"), aesDecryptIV, aesDecryptSalt);
    this.userId = JSON.parse(aesDecrypt).userId;

  }

  //Form
  changePasswordForm = new FormGroup({
    current: new FormControl('', [
      Validators.required
    ]),
    newP: new FormControl('', [
      Validators.required
    ])
  });

  newPass: string;
  prevEnc: string;
  updatePassword() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        var Encrypt: any;
        var prevPassword: any;
        this.newPass = this.changePasswordForm.get('newP').value;
        this.prevEnc = this.changePasswordForm.get('current').value;
        Encrypt = this.encrypt.encryptUsingAES256(this.newPass).toString();
        prevPassword = this.encrypt.encryptUsingAES256(this.prevEnc).toString();
        let body = {
          "updateUserId": this.userId,
          "prevPassword": prevPassword,
          "newPassword": Encrypt,
          "url": "changePasswordService"
        };
        // console.log(JSON.stringify(body))
        this.callService.putData(body).subscribe((data) => {
          let serviceReturn = data.json();
          let statusService = serviceReturn['0'].serviceContent.status;

          // console.log("ini status : " + statusService);

          if (statusService == "S") {
            showSwal('success', 'Success Change Password', "Sukses mengganti password");
            sessionStorage.clear();
            this.router.navigateByUrl("/");
          } else {
            showSwal('warning', 'Change Password Failed', 'Gagal mengganti password');
          }
        }, (err) => {
          this.router.navigateByUrl('/error500')
          this.ngOnInit();
        })
      }
    });
  }
}
