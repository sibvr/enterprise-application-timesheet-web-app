import { Pipe, PipeTransform, Injectable } from '@angular/core';
@Pipe({
    name: 'filteruser'
})

export class Inquiryuserfilterpipe implements PipeTransform {
    transform(items: any[], search: string){
        
        if (items && items.length){
            return items.filter(item =>{
                if (search && String(item.userFullname).toLowerCase().indexOf(search.toLowerCase()) === -1){
                    return false;
                }
            
                return true;
           })
    
        }
        else{
            return items;
        }
        

    }

}