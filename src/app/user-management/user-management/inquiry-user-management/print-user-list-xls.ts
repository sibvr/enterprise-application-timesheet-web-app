import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { Workbook } from 'exceljs';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable()
export class printUserListLxls {

  constructor() { }

  public exportAsExcelFile(json: any[], excelFileName: string): void {

    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    // console.log('worksheet', worksheet);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['PDLGD-FL'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    //const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });

    FileSaver.saveAs(data, fileName + new Date().getTime() + EXCEL_EXTENSION);
  }

  public generateExcel(data: any, fileName: any) {

    let workbook = new Workbook();
    //var worksheet = workbook.addWorksheet('PDLGD-FL', {properties:{defaultRowHeight:15}});
    var worksheet = workbook.addWorksheet('List User Enterprise Timesheet Web App');

    //Excel Title, Header, Data
    let title = 'List User Enterprise Timesheet Web App';



    //Add Row and formatting
    let titleRow = worksheet.addRow([title]);
    titleRow.font = { name: 'Calibri', family: 4, size: 20, underline: false, bold: true, color: { argb: '00000000' } }
    titleRow.alignment = { vertical: 'middle', horizontal: 'center' };
    worksheet.addRow([]);

    worksheet.mergeCells('A1:G1');

    let header = [];
    header.push('Nama Lengkap');
    header.push('Nama User');
    header.push('Role');
    header.push('Nama Manager');
    header.push('Email');
    header.push('No Telpon');
    header.push('Alamat');
    //Add Header Row
    let headerRow = worksheet.addRow(header);

    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'dbdbdb' },
        bgColor: { argb: 'dbdbdb' },
      }
      cell.border = { top: { style: 'double' }, left: { style: 'double' }, bottom: { style: 'double' }, right: { style: 'double' } };
      cell.alignment = { vertical: 'middle', horizontal: 'left' };
      cell.font = { bold: true, color: { argb: '00000000' } };
    })

    // Add Data and Conditional Formatting
    data.forEach(d => {
      // console.log(d);
      let row = worksheet.addRow(d);
      row.eachCell((cell, number) => {
        cell.fill = {
          type: 'pattern',
          //pattern:'darkTrellis',
          pattern: 'solid',
          fgColor: { argb: 'FFFFFFFF' },
          bgColor: { argb: 'FFFFFFFF' }
        }
        cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
        cell.alignment = { vertical: 'middle', horizontal: 'left' };
      }
      )

    });

    let objectMaxLength = []; 
    for (let i = 0; i < data.length; i++) {
      let value = <any>Object.values(data[i]);
      for (let j = 0; j < value.length; j++) {
        if (typeof value[j] == "number") {
          objectMaxLength[j] = 10;
        } else {
          objectMaxLength[j] =
            objectMaxLength[j] >= value[j].length
              ? objectMaxLength[j]
              : value[j].length;
        }
      }
    }
    // console.log(objectMaxLength);
    worksheet.getColumn(1).width = objectMaxLength[0];
    worksheet.getColumn(2).width = objectMaxLength[1];
    worksheet.getColumn(3).width = objectMaxLength[2];
    worksheet.getColumn(4).width = objectMaxLength[3];
    worksheet.getColumn(5).width = objectMaxLength[4];
    worksheet.getColumn(6).width = objectMaxLength[5];
    worksheet.getColumn(7).width = objectMaxLength[6];


    //Generate Excel File with given name
    workbook.xlsx.writeBuffer().then((data) => {
      let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      FileSaver.saveAs(blob, fileName + new Date().getTime() + EXCEL_EXTENSION);
    })
  }
}