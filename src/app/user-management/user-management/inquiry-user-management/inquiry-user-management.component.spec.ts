import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InquiryUserManagementComponent } from './inquiry-user-management.component';

describe('InquiryUserManagementComponent', () => {
  let component: InquiryUserManagementComponent;
  let fixture: ComponentFixture<InquiryUserManagementComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InquiryUserManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InquiryUserManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
