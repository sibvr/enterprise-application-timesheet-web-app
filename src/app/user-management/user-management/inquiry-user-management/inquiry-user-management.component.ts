import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, ReactiveFormsModule, FormArray } from '@angular/forms';
import { Http, JSONPBackend } from '@angular/http';
import { Router } from "@angular/router";
import { CallServiceFunction } from '../../../callServiceFunction';
import { AppDecryptMe } from "../../../app.DecryptMe";
import { printUserListLxls } from './print-user-list-xls';
import { EncryptComponent } from "../../../encrypt.component";
import { CekCredential } from '../../../cekcredential';
declare function showSwal(type, title, text): any;
declare var $: any;

@Component({
  selector: 'app-inquiry-user-management',
  templateUrl: './inquiry-user-management.component.html',
  styleUrls: ['./inquiry-user-management.component.css']
})
export class InquiryUserManagementComponent implements OnInit {


  constructor(public encrypt: EncryptComponent, private cekCredential: CekCredential, public http: Http, public router: Router, private callService: CallServiceFunction, public decryptMe: AppDecryptMe, private printUserListLxls: printUserListLxls) { }

  //pagination
  p: number = 1;
  userFiltering ="";

  //Form
  insertUserForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    userName: new FormControl('', [Validators.required]),
    role: new FormControl('', [Validators.required]),
    manager: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required]),
    pwd: new FormControl('', [Validators.required]),
    tlp: new FormControl('', [Validators.required]),
    alamat: new FormControl('', [Validators.required])
  })

  //Form
  updateUserForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    userName: new FormControl('', [Validators.required]),
    role: new FormControl('', [Validators.required]),
    roleId: new FormControl('', [Validators.required]),
    manager: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required]),
    tlp: new FormControl('', [Validators.required]),
    alamat: new FormControl('', [Validators.required])
  })


  userDetail: { userName: string, name: string, pwd: string, email: string, role: string, manager: string, tlp: string, alamat: string } = { userName: '', name: '', pwd: '', email: '', role: '', manager: '', tlp: '', alamat: '' };
  userDetailUpdate: { userName: string, name: string, pwd: string, email: string, role: string, roleName: string, manager: string, tlp: string, alamat: string } = { userName: '', name: '', pwd: '', email: '', role: '', roleName: '', manager: '', tlp: '', alamat: '' };

  userId: any;
  listUserManagement = [];
  loadingData: boolean;
  ngOnInit(): void {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.clear();
        this.router.navigateByUrl('/');
      } else {
        var aesDecryptIV = this.decryptMe.aesDecryptIV(sessionStorage.getItem("ekrptd1"));
        var aesDecryptSalt = this.decryptMe.aesDecryptSalt(sessionStorage.getItem("ekrtdp2"));
        var aesDecrypt = this.decryptMe.aesDecrypt(sessionStorage.getItem("usdt"), aesDecryptIV, aesDecryptSalt);
        this.userId = JSON.parse(aesDecrypt).userId;
        this.inquiryUserList();
      }
    });
  }


  //Modals Popup Delete
  userIdDelete: number;
  usernameDeleteSelected: string;
  deleteconfirmedvariable(userIdinput, fullname) {
    this.userIdDelete = userIdinput;
    this.usernameDeleteSelected = fullname;
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.clear();
        this.router.navigateByUrl('/');
      } else {
        this.userIdDelete = userIdinput;
        this.usernameDeleteSelected = fullname;
      }
    });
  }

  //Modals Popup Update
  userEditId: number;
  usermanagementupdate(userEditId) {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.clear();
        this.router.navigateByUrl('/');
      } else {
        // console.log(this.userDetailUpdate);
        this.userEditId = userEditId;
        // console.log(userEditId);

        let serviceReturn: any;
        let statusService: string;

        let url = "getUserManagementService";
        let params = "?userId=" + this.userId + "&parameter=" + userEditId;

        this.callService.getData(url + params).subscribe((res) => {
          serviceReturn = res;
          statusService = serviceReturn["0"].serviceContent.status;
          // console.log("json update : " + JSON.stringify(serviceReturn));
          // console.log("hasil status : " + statusService);

          if (statusService == "S") {
            if (serviceReturn["0"].serviceContent.serviceOutput.length > 0) {

              this.userEditId = serviceReturn["0"].serviceContent.serviceOutput[0].id;
              this.userDetailUpdate.name = serviceReturn["0"].serviceContent.serviceOutput[0].userFullname;
              this.userDetailUpdate.userName = serviceReturn["0"].serviceContent.serviceOutput[0].username;
              this.userDetailUpdate.role = serviceReturn["0"].serviceContent.serviceOutput[0].roleId;
              this.userDetailUpdate.roleName = serviceReturn["0"].serviceContent.serviceOutput[0].role;
              this.userDetailUpdate.manager = serviceReturn["0"].serviceContent.serviceOutput[0].managerId;
              this.userDetailUpdate.tlp = serviceReturn["0"].serviceContent.serviceOutput[0].phone;
              this.userDetailUpdate.alamat = serviceReturn["0"].serviceContent.serviceOutput[0].address;
              //refresh role
              this.changeRoleUpdate();
              // console.log("masuk managerId : " + serviceReturn["0"].serviceContent.serviceOutput[0].manager)
              if (serviceReturn["0"].serviceContent.serviceOutput[0].manager == null) {
                this.userDetailUpdate.manager = "0";
              } else {

                this.userDetailUpdate.manager = serviceReturn["0"].serviceContent.serviceOutput[0].managerId;
              }
              this.userDetailUpdate.email = serviceReturn["0"].serviceContent.serviceOutput[0].email;
              // console.log(this.userDetailUpdate.manager);
            }
          }

        }, (err) => this.router.navigateByUrl('/error500'));


      }
    });
  }





  // DELETE USER
  deleteUser(userIdDelete: number) {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.clear();
        this.router.navigateByUrl('/');
      } else {
        let body = {
          "id": userIdDelete,
          "updateUserId": this.userId,
          "status": "N",
          "url": "deactivateUserService"
        };

        let serviceReturn: any;
        let statusService: string;

        this.callService.putData(body).subscribe((res) => {

          serviceReturn = res.json();
          statusService = serviceReturn["0"].serviceContent.status;

          // console.log("res :" + JSON.stringify(serviceReturn));

          // console.log("hasil status delete: " + statusService);

          if (statusService == "S") {
            showSwal('success', 'Success Delete', 'User ' + this.usernameDeleteSelected + " berhasil dihapus");

          } else {
            showSwal('warning', 'Delete Failed', serviceReturn["0"].serviceContent.errorMessage);
          }
          this.ngOnInit();
        })
      }
    });
  }

  //INQUIRY USER
  inquiryUserList() {
    this.loadingData = true; 
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.clear();
        this.router.navigateByUrl('/');
      } else {
        this.insertUserForm.reset();
        this.listUserManagement = [];

        let serviceReturn: any;
        let statusService: string;

        let url = "getUserManagementService";
        let params = "?userId=" + this.userId + "&parameter=Y";

        this.callService.getData(url + params).subscribe((res) => {
          serviceReturn = res;
          statusService = serviceReturn["0"].serviceContent.status;
          // console.log("json : " + JSON.stringify(serviceReturn));
          // console.log("hasil status : " + statusService);

          if (statusService == "S") {
            if (serviceReturn["0"].serviceContent.serviceOutput.length > 0) {
              // this.loadingBar = "false"
              // this.typeView = "view"

              for (let i = 0; i < serviceReturn["0"].serviceContent.serviceOutput.length; i++) {
                this.listUserManagement.push(serviceReturn["0"].serviceContent.serviceOutput[i]);
              }
            }
            // console.log(this.listUserManagement);
            this.inquiryRole();
          }

        }, (err) => this.router.navigateByUrl('/error500'));

      }
    });
  }

  //CREATE User
  packOfUserUpdate = {};
  listOfPackUserUpdate = [];
  formShow = 'show';
  statusShow = 'noShow';
  passwordNow: string;
  createUser() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.clear();
        this.router.navigateByUrl('/');
      } else {
        let serviceReturn: any;
        let statusService: string;

        let emailString = this.userDetail.email;
        let splt = emailString.split("@", 2);
        if (splt[1] != "multipolar.com") {
          this.userDetail.email = splt[0].concat("@multipolar.com");
        } else {
          this.userDetail.email = this.userDetail.email;
        }
        // if (this.userDetail.manager == '-') {
        //   this.userDetail.manager = '0';
        // }
        this.passwordNow =  this.userDetail.pwd;
        var Encrypt: any;
        Encrypt = this.encrypt.encryptUsingAES256(this.passwordNow).toString();
        this.statusShow = 'show';
        this.formShow = 'noShow';
        console.log(Encrypt);
        let body = {
          "username": this.userDetail.userName,
          "password": Encrypt,
          "email": this.userDetail.email,
          "flagNewRole": "N",
          "createUserId": this.userId,
          "userFullname": this.userDetail.name,
          "taskCrudActiveList": this.listOfPackUserUpdate,
          "roleId": parseInt(this.userDetail.role),
          "managerId": parseInt(this.userDetail.manager),
          "phone": this.userDetail.tlp,
          "address": this.userDetail.alamat,
          "url": "createNewUserService"
        };

        // console.log("body update : " + JSON.stringify(body));

        this.callService.postData(body).subscribe((res) => {

          serviceReturn = res.json();
          statusService = serviceReturn["0"].serviceContent.status;

          // console.log("ini return : " + JSON.stringify(serviceReturn));

          if (statusService == "S") {
            this.insertUserForm.reset();
            this.statusShow = 'noShow';
            this.formShow = 'show';
            showSwal('success', 'Success Create User', 'User ' + this.userDetail.userName + " was successfully created");

           
          } else {
            this.statusShow = 'noShow';
            this.formShow = 'show';
            showSwal('warning', 'Create User Failed', serviceReturn["0"].serviceContent.errorMessage);
          }

          this.ngOnInit();
        }, (err) => { showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ') })
      }
    });
  }

  //UPDATE User
  updateUser() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.clear();
        this.router.navigateByUrl('/');
      } else {
        let serviceReturn: any;
        let statusService: string;

        let emailString = this.userDetailUpdate.email;
        let splt = emailString.split("@", 2);
        if (splt[1] != "multipolar.com") {
          this.userDetailUpdate.email = splt[0].concat("@multipolar.com");
        } else {
          this.userDetailUpdate.email = this.userDetailUpdate.email;
        }
        // if (this.userDetailUpdate.manager == '-') {
        //   this.userDetailUpdate.manager = '0';
        // }
        let body = {
          // "url": "editUserManagementDetailService",
          "url":"updateUserService",
          "roleId": parseInt(this.userDetailUpdate.role),
          "managerId": this.userDetailUpdate.manager,
          "username": this.userDetailUpdate.userName,
          "email": this.userDetailUpdate.email,
          "id": this.userEditId,
          "updateUserId": this.userId,
          "phone": this.userDetailUpdate.tlp,
          "address": this.userDetailUpdate.alamat,
          "flagNewRole": "N",
          "userFullname": this.userDetailUpdate.name,
          "taskCrudActiveList": []
        };

        // console.log(JSON.stringify(body));
        this.callService.putData(body).subscribe((data) => {
          let serviceReturn = data.json();
          let statusService = serviceReturn['0'].serviceContent.status;
          if (statusService == "S") {
            showSwal('success', 'Success Update', 'Berhasil mengubah user ' + this.usernameDeleteSelected);
            this.updateUserForm.reset();
        
          } else {
            showSwal('warning', 'Update Failed', serviceReturn['0'].serviceContent.errorMessage);
          }

          this.ngOnInit();
        }, (err) => { showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ') })
      }
    });
  }




  //inquiry role
  listOfRole = [];
  inquiryRole() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.clear();
        this.router.navigateByUrl('/');
      } else {
        this.listOfRole = [];

        let serviceReturn: any;
        let statusService: string;

        let url = "getRoleManagementService";
        let params = "?userId=" + this.userId + "&parameter=Y";


        this.callService.getData(url + params).subscribe((res) => {
          serviceReturn = res;
          statusService = serviceReturn["0"].serviceContent.status;
          // console.log(JSON.stringify(serviceReturn));
          if (serviceReturn["0"].serviceContent.serviceOutput.length > 0) {

            for (let i = 0; i < serviceReturn["0"].serviceContent.serviceOutput.length; i++) {
              this.listOfRole.push(serviceReturn["0"].serviceContent.serviceOutput[i]);
            }

            this.inquiryUserManagement();

          }

        }, (err) => this.router.navigateByUrl('/error500'));

      }
    });
  }

  //inquiry manager
  listofManager = [];
  listofManagerTemp = [];
  inquiryUserManagement() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.clear();
        this.router.navigateByUrl('/');
      } else {
        this.listofManager = [];
        this.listofManagerTemp = [];

        let serviceReturn: any;
        let statusService: string;

        let url = "getUserManagementService";
        let params = "?userId=" + this.userId;

        this.callService.getData(url + params).subscribe((res) => {
          serviceReturn = res;
          statusService = serviceReturn["0"].serviceContent.status;

          // console.log("return manager : " + JSON.stringify(serviceReturn));

          if (statusService == "S") {
            if (serviceReturn["0"].serviceContent.serviceOutput.length > 0) {
              for (let i = 0; i < serviceReturn["0"].serviceContent.serviceOutput.length; i++) {
                this.listofManager.push(serviceReturn["0"].serviceContent.serviceOutput[i]);
              }
            }

            // this.listofManagerTemp = this.listofManager;
            // console.log("listofManager : " + JSON.stringify(this.listofManager));

          }

        }, (err) => this.router.navigateByUrl('/error500'));

        this.loadingData = false;
      }
    });
  }



  // On change role Insert
  STAFF = "STAFF";
  ADMIN = "ADMIN-STAFF";
  MANAGER = "MANAGER";
  SECHEAD = "SEC-HEAD";
  SUPERADMIN = "SUPER-ADMIN";
  roleCode: any;
  changeRole() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.clear();
        this.router.navigateByUrl('/');
      } else {

        this.listofManagerTemp = [];

        this.roleCode = "";
        //search code Role 
        // console.log("this.listOfRole : " + JSON.stringify(this.listOfRole));
        // console.log(JSON.stringify(this.listofManager));
        // console.log(JSON.stringify(this.userDetail));
        // for (let index = 0; index < array.length; index++) {
        //   const element = array[index];

        // }
        // for (let u = 0; u < this.listOfRole.length; u++) {
        //   if ((this.listOfRole[u].roleCodeHierarchy == this.listofManager[u].roleCode) {
        //     this.listofManagerTemp.push(this.listofManager[u].roleCode);
        //   }

        // }


        for (let u = 0; u < this.listOfRole.length; u++) {
          if (this.listOfRole[u].id == this.userDetail.role) {
            this.roleCode = this.listOfRole[u].roleCodeHierarchy;
            for (let i = 0; i < this.listofManager.length; i++) {
              if (this.roleCode == this.listofManager[i].role) {
                this.listofManagerTemp.push(this.listofManager[i]);
              }

            }
          }
        }
        // console.log(this.roleCode);

        // }
        // console.log("userRoleIdSelected : " + JSON.stringify(this.roleCode));

        // console.log("manager temp : " + this.listofManagerTemp);

        // //SECHEAD
        // if (this.roleCode == this.SECHEAD || this.roleCode == this.SUPERADMIN) {
        //   let jsonManager = {};
        //   jsonManager["userId"] = 0;
        //   jsonManager["usernameResult"] = "-";
        //   this.listofManagerTemp.push(jsonManager);
        // } else {
        //   for (let i = 0; i < this.listofManager.length; i++) {

        //     //staff
        //     if (this.roleCode == this.STAFF || this.roleCode == this.ADMIN) {

        //       if (this.listofManager[i].role == this.MANAGER) {
        //         this.listofManagerTemp.push(this.listofManager[i]);
        //       }

        //     }

        //     //manager
        //     else if (this.roleCode == this.MANAGER) {

        //       if (this.listofManager[i].role == this.SECHEAD) {
        //         this.listofManagerTemp.push(this.listofManager[i]);
        //       }

        //     }

        //   }
        // }
        // console.log(this.listofManagerTemp[0].userId);
      }
    });
  }

  // On change role update
  changeRoleUpdate() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.clear();
        this.router.navigateByUrl('/');
      } else {
        this.listofManagerTemp = [];

        let roleCode = "";
        //search code Role 
        // console.log(this.userDetailUpdate);
        // console.log(this.listOfRole);

        for (let u = 0; u < this.listOfRole.length; u++) {
          if (this.listOfRole[u].id == this.userDetailUpdate.role) {
            this.roleCode = this.listOfRole[u].roleCodeHierarchy;
            for (let i = 0; i < this.listofManager.length; i++) {
              if (this.roleCode == this.listofManager[i].role) {
                this.listofManagerTemp.push(this.listofManager[i]);
              }

            }
          }
        }
        // console.log(this.roleCode);
        // for (let u = 0; u < this.listOfRole.length; u++) {
        //   if (this.listOfRole[u].roleId == this.userDetailUpdate.role) {
        //     roleCode = this.listOfRole[u].roleCode;
        //   }

        // }

        // console.log("roleCode : " + roleCode);

        // if (roleCode == this.SECHEAD || roleCode == this.SUPERADMIN) {
        //   let jsonManager = {};
        //   jsonManager["userId"] = 0;
        //   jsonManager["usernameResult"] = "-";
        //   this.listofManagerTemp.push(jsonManager);
        // } else {
        //   console.log("userRoleIdSelected : " + JSON.stringify(this.listofManager));

        //   for (let i = 0; i < this.listofManager.length; i++) {

        //     //staff
        //     if (roleCode == this.STAFF || roleCode == this.ADMIN) {

        //       if (this.listofManager[i].role == this.MANAGER) {
        //         this.listofManagerTemp.push(this.listofManager[i]);
        //       }

        //     }

        //     //manager
        //     else if (roleCode == this.MANAGER) {

        //       if (this.listofManager[i].role == this.SECHEAD) {
        //         this.listofManagerTemp.push(this.listofManager[i]);
        //       }

        //     }

        //   }
        // }
        // console.log(this.listofManagerTemp);
      }
    });
  }



  dataExcel = [];
  exportexcel(): void {
    this.dataExcel=[];
    
    // console.log(this.listUserManagement);
    for (var i = 0; i < this.listUserManagement.length; ++i) {
      let data: any[] = [];
      data.push(this.listUserManagement[i].userFullname);
      data.push(this.listUserManagement[i].username);
      data.push(this.listUserManagement[i].role);
      if (this.listUserManagement[i].manager == null || this.listUserManagement[i].manager == "") {
        data.push("-");
      } else {
        data.push(this.listUserManagement[i].manager);
      }
      data.push(this.listUserManagement[i].email);
      data.push(this.listUserManagement[i].phone);
      data.push(this.listUserManagement[i].address);
      this.dataExcel.push(data);

    }

    this.printUserListLxls.generateExcel(this.dataExcel, 'user-list',);
    // console.log("dataExcel" + this.dataExcel);

  }




}
