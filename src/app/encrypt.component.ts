import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root',
})
export class EncryptComponent {
  constructor() {}
  private keyps = CryptoJS.enc.Utf8.parse('4512631236589784');
  private ivps = CryptoJS.enc.Utf8.parse('4512631236589784');
  salt: any;
  iv: any;

  encryptUsingAES256(plaintext) {
    var plainText = plaintext;
    var encrypted = CryptoJS.AES.encrypt(
      CryptoJS.enc.Utf8.parse(JSON.stringify(plainText)),
      this.keyps,
      {
        keySize: 128 / 8,
        iv: this.ivps,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7,
      }
    );
    // console.log('Encrypted :' + encrypted);
    this.decryptUsingAES256(encrypted);
    return encrypted;
  }

  decryptUsingAES256(decString) {
    var decrypted = CryptoJS.AES.decrypt(decString, this.keyps, {
      keySize: 128 / 8,
      iv: this.ivps,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7,
    });
    // console.log('Decrypted : ' + decrypted);
    // console.log('utf8 = ' + decrypted.toString(CryptoJS.enc.Utf8));
  }

  public aesEncrypt(plaintext) {
    var plainText = plaintext;
    var passPhrase = 'abcdefghij';
    var keySize = 128 / 32;
    var iterationCount = 10;
    var enkripResult;

    /*Generate random IV*/
    this.iv = CryptoJS.lib.WordArray.random(128 / 8).toString(CryptoJS.enc.Hex);
    //// // console.log("IV: " + iv);

    /*Generate salt*/
    this.salt = CryptoJS.lib.WordArray.random(128 / 8).toString(
      CryptoJS.enc.Hex
    );
    console.log('Salt : ' + CryptoJS.enc.Hex.parse(this.salt));

    /*Generate Key*/
    var key = CryptoJS.PBKDF2(passPhrase, CryptoJS.enc.Hex.parse(this.salt), {
      keySize: keySize,
      iterations: iterationCount,
    });

    /*Encrypt*/
    var encrypted = CryptoJS.AES.encrypt(plainText, key, {
      iv: CryptoJS.enc.Hex.parse(this.iv),
    });

    return encrypted;
  }

  public aesEncryptSalt(plaintext) {
    var plainText = plaintext;
    var passPhrase = 'abcdefghij';
    var keySize = 128 / 32;
    var iterationCount = 10;
    var enkripResult;

    /*Generate IV*/
    var ivSalt = '1FiOlmemXxT6dPVS';

    /*Generate salt*/
    var saltSalt = 'xmwLGUjMInJp3PgA';

    /*Generate Key*/
    var key = CryptoJS.PBKDF2(passPhrase, CryptoJS.enc.Hex.parse(saltSalt), {
      keySize: keySize,
      iterations: iterationCount,
    });

    /*Encrypt*/
    var encrypted = CryptoJS.AES.encrypt(plainText, key, {
      iv: CryptoJS.enc.Hex.parse(ivSalt),
    });

    //// // console.log("aesEncrypt result: " + encrypted.ciphertext.toString(CryptoJS.enc.Base64));
    return (enkripResult = encrypted.ciphertext.toString(CryptoJS.enc.Base64));
  }
  public aesEncryptIV(plaintext) {
    var plainText = plaintext;
    var passPhrase = 'abcdefghij';
    var keySize = 128 / 32;
    var iterationCount = 10;
    var enkripResult;

    /*Generate random IV*/
    var ivIV = 'BLN0eyNx9PumhJ6J';

    /*Generate salt*/
    var saltIV = 'cklEYL5gjMfIvoa5';

    /*Generate Key*/
    var key = CryptoJS.PBKDF2(passPhrase, CryptoJS.enc.Hex.parse(saltIV), {
      keySize: keySize,
      iterations: iterationCount,
    });

    /*Encrypt*/
    var encrypted = CryptoJS.AES.encrypt(plainText, key, {
      iv: CryptoJS.enc.Hex.parse(ivIV),
    });

    //// // console.log("aesEncrypt result: " + encrypted.ciphertext.toString(CryptoJS.enc.Base64));
    return (enkripResult = encrypted.ciphertext.toString(CryptoJS.enc.Base64));
  }
}
