import { Component, OnInit } from '@angular/core';
import { CekCredential } from '../../cekcredential';
import { CallServiceFunction } from '../../callServiceFunction';
import { AppDecryptMe } from "../../app.DecryptMe";
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as CryptoJS from 'crypto-js';

declare function showSwal(type, title, text): any;


@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.css']
})
export class ConfigurationComponent implements OnInit {

  constructor(private cekCredential: CekCredential, public router: Router, private callService: CallServiceFunction, public decryptMe: AppDecryptMe) { }
  p: number = 1;

  updateForm = new FormGroup({
    confEdit: new FormControl('', [Validators.required])
  })

  createconfworkForm = new FormGroup({
    nameConf: new FormControl('', [Validators.required])
  })

  configEmailAdmin = new FormGroup({
    useremail: new FormControl('', [Validators.required]),
    passemail: new FormControl('', [Validators.required])
  })

  emailusr: any;
  passusr: any;
  emailusrnew = [];
  passusrnew = [];
  statusemail: any;
  // statuspass: any;
  listConfigEmail = [];
  secondListeml = [];
  secondListpsw = [];
  getConfigEmail() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        this.callService.getData('getConfigEmailService?userId=' + this.userId).subscribe((data) => {
          let serviceReturn = data;
          console.log(JSON.stringify(serviceReturn));
          let statusService = serviceReturn['0'].serviceContent.status;
          this.listConfigEmail = [];
          this.emailusrnew = [];
          this.passusrnew = [];
          this.secondListeml = [];
          this.secondListpsw = [];

          if (statusService == 'S') {
            for (let i = 0; i < serviceReturn['0'].serviceContent.serviceOutput.length; i++) {
              this.emailusr = serviceReturn['0'].serviceContent.serviceOutput[0];
              this.passusr = serviceReturn['0'].serviceContent.serviceOutput[1];
              this.listConfigEmail.push(serviceReturn['0'].serviceContent.serviceOutput);
            }

            this.emailusrnew.push(this.emailusr);
            this.passusrnew.push(this.passusr);
            for (let b = 0; b < this.emailusrnew.length; b++) {
              this.secondListeml.push(this.emailusrnew[0]);
              this.configEmailAdmin.controls['useremail'].setValue(this.secondListeml[b].configValue);
              this.statusemail = this.emailusrnew[b].active;
            }
            for (let k = 0; k < this.passusrnew.length; k++) {
              this.secondListpsw.push(this.passusrnew[0]);
              this.configEmailAdmin.controls['passemail'].setValue(this.secondListpsw[k].configValue);
              // this.statuspass = this.passusrnew[k].active;
            }
          }
        });
      }
    });
  }

  activeEmail(id) {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        let status = "";
        if (this.statusemail == 'Y') {
          status = 'N'
        } else {
          status = 'Y'
        }
        let body = {
          "updateUserId": this.userId,
          "id": id,
          "status": status,
          "url": "activationConfigService"
        }
        this.callService.putData(body).subscribe((data) => {
          let serviceReturn = data.json();
          console.log(JSON.stringify(serviceReturn));
          let statusService = serviceReturn['0'].serviceContent.status;
          if (statusService == "S") {
            if (serviceReturn['0'].serviceContent.serviceOutput.active == 'Y') {
              showSwal('success', 'konfigurasi email', 'email berhasil aktif');
            } else {
              showSwal('success', 'konfigurasi email', 'email berhasil nonaktif');
            }
            this.getConfigWorking();
          } else {
            showSwal('warning', 'Aktifasi konfigurasi', serviceReturn['0'].serviceContent.errorMessage)
          }
        }, (err) => { showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ') });
      }
    });
  }

  listid = [];
  listconfVal: any;
  idlist = [];
  idtest(id, subConf) {
    this.listid = id;

    if (subConf == 'EMAIL') {
      this.listconfVal = this.configEmailAdmin.controls['useremail'].value;
    } else {
      let testing = this.configEmailAdmin.controls['passemail'].value;
      let secret = "80cfbc5911addeb1e4fe72f6211e7a69";
      let encrypted = CryptoJS.AES.encrypt(testing, secret);
      this.listconfVal = encrypted.toString();
    }

    let ig = {
      "id": this.listid,
      "configValue": this.listconfVal
    }
    this.idlist.push(ig);
  }

  listconval = {};
  listOfConf = [];
  updateConfigEmail() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        let body = {
          "configEntity": this.idlist,
          "updateUserId": this.userId,
          "url": "updateConfigService"
        }
        console.log(JSON.stringify(body));
        this.callService.putData(body).subscribe((data) => {
          let serviceReturn = data.json();
          console.log(JSON.stringify(serviceReturn));
          let statusService = serviceReturn['0'].serviceContent.status;
          if (statusService == 'S') {
            showSwal('success', 'perbaharui email', 'perbaharui email berhasil');
            this.getConfigWorking();
          } else {
            showSwal('success', 'cek email', serviceReturn['0'].serviceContent.errorMessage)
          }
        }, (err) => { showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ') });
      }
    });
  }
  checkEmail() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        this.callService.getData('checkEmailService?userId=' + this.userId).subscribe((data) => {
          let serviceReturn = data;
          console.log(JSON.stringify(serviceReturn));
          let statusService = serviceReturn['0'].serviceContent.status;
          if (statusService == 'S') {
            showSwal('success', 'cek email', 'cek email berhasil')
          } else {
            showSwal('success', 'cek email', serviceReturn['0'].serviceContent.errorMessage)
          }
        }, (err) => { showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ') });
      }
    });
  }
  listWorkingStatusConfig = [];
  getConfigWorking() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        this.listWorkingStatusConfig = [];
        this.callService.getData('getConfigWorkingService?userId=' + this.userId).subscribe((data) => {
          let serviceReturn = data;
          // console.log(JSON.stringify(serviceReturn));
          let statusService = serviceReturn['0'].serviceContent.status;
          if (statusService == "S") {
            for (let i = 0; i < serviceReturn['0'].serviceContent.serviceOutput.length; i++) {
              this.listWorkingStatusConfig.push(serviceReturn['0'].serviceContent.serviceOutput[i]);
            }
          }
          this.getConfigEmail();
        });

      }
    });
  }

  activation(id, act) {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        if (act == 'N') {
          act = 'Y'
        } else {
          act = 'N'
        }
        let body = {
          updateUserId: this.userId,
          id: id,
          status: act,
          url: "activationConfigService"
        }
        this.callService.putData(body).subscribe((data) => {
          let serviceReturn = data.json();
          let statusService = serviceReturn['0'].serviceContent.status;
          if (statusService == "S") {
            if (serviceReturn['0'].serviceContent.serviceOutput.active == 'N') {
              showSwal('success', 'non aktif konfigurasi', 'konfigurasi berhasil non aktifkan')
            } else {
              showSwal('success', 'aktifasi konfigurasi', 'konfigurasi berhasil aktifkan')
            }
            this.getConfigWorking();
          } else {
            showSwal('warning', 'Aktifasi konfigurasi', serviceReturn['0'].serviceContent.errorMessage)
          }
        }, (err) => { showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ') });
      }
    });
  }
  tksindex: any;
  updateRemark(id, configMenu) {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        this.tksindex = id;
        this.updateForm.get("confEdit").setValue(configMenu);
      }
    });
  }
  updateDataEdit(id) {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        let body2 = {
          "configEntity": [
            {
              "configValue": this.updateForm.controls['confEdit'].value,
              "id": id
            }
          ],
          "updateUserId": this.userId,
          url: "updateConfigService"
        }
        this.callService.putData(body2).subscribe((data) => {
          let serviceReturn = data.json();
          let statusService = serviceReturn['0'].serviceContent.status;
          if (statusService == "S") {
            showSwal('success', 'update konfigurasi', 'konfigurasi berhasil diupdate');
            this.tksindex = "";
            this.getConfigWorking();
          } else {
            showSwal('warning', 'update konfigurasi', serviceReturn['0'].serviceContent.errorMessage);
            this.tksindex = "";
            this.getConfigWorking();
          }
        }, (err) => { showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ') });

      }
    });
  }
  reverbEditData() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        this.tksindex = "";
      }
    });
  }

  createconfig() {
    let body = {
      configValue: this.createconfworkForm.controls['nameConf'].value,
      createUserId: this.userId,
      url: "createConfigWorkingService"
    }
    this.callService.postData(body).subscribe((data) => {
      let serviceReturn = data.json();
      let statusService = serviceReturn['0'].serviceContent.status;
      if (statusService == 'S') {
        showSwal('success', 'tambah konfigurasi', 'konfigurasi berhasil ditambah');
        this.createconfworkForm.reset();
        this.getConfigWorking();
      } else {
        showSwal('warning', 'tambah konfigurasi', serviceReturn['0'].serviceContent.errorMessage);
      }
    }, (err) => { showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ') });
  }

  userId: any;
  ngOnInit(): void {
    var aesDecryptIV = this.decryptMe.aesDecryptIV(sessionStorage.getItem("ekrptd1"));
    var aesDecryptSalt = this.decryptMe.aesDecryptSalt(sessionStorage.getItem("ekrtdp2"));
    var aesDecrypt = this.decryptMe.aesDecrypt(sessionStorage.getItem("usdt"), aesDecryptIV, aesDecryptSalt);
    this.userId = JSON.parse(aesDecrypt).userId;

    this.getConfigWorking();
  }

}
