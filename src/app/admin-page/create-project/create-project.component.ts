import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { CallServiceFunction } from '../../callServiceFunction';
import { AppDecryptMe } from "../../app.DecryptMe";
import { FormBuilder, FormControl, FormGroup, Validators, FormArray, FormControlName } from '@angular/forms';
declare function showSwal(type, title, text): any;
import { CekCredential } from '../../cekcredential';

declare var $: any;
@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.css'],
  providers: [DatePipe]
})
export class CreateProjectComponent implements OnInit {

  constructor(private cekCredential: CekCredential, public router: Router, private datePipe: DatePipe, private callService: CallServiceFunction, public decryptMe: AppDecryptMe) { }
  p: number = 1;

  createProject = new FormGroup({
    pid: new FormControl('', [
      Validators.required
    ]),
    projectName: new FormControl('', [
      Validators.required
    ]),
    customer: new FormControl('', [
      Validators.required
    ]),
    category: new FormControl('', [
      Validators.required
    ]),
    billable: new FormControl('', [
      Validators.required
    ]),
    mandays: new FormControl('', [
      Validators.required
    ])

  })

  formDataUpload: FormData = new FormData();
  uploadFileForm = new FormGroup({
    UploadFile: new FormControl('', [
      Validators.required
    ])
  });

  updateForm = new FormGroup({
    pidedit: new FormControl('', [Validators.required]),
    nameedit: new FormControl('', [Validators.required]),
    customeredit: new FormControl('', [Validators.required])
  })

  statusBill = ["Billable", "Unbillable"];
  Pidfiltering = "";
  ProyekNamefiltering = ""
  createNewProject() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        let body = {
          "createUserId": this.userId,
          "projectList": [{
            "pid": this.createProject.controls['pid'].value,
            "projectName": this.createProject.controls['projectName'].value,
            "costumer": this.createProject.controls['customer'].value,
            "category": this.createProject.controls['category'].value,
            "bill": this.createProject.controls['billable'].value,
            "mandays": this.createProject.controls['mandays'].value,
          }],
          "url": "createProjectService"
        }
        // console.log(body);
        this.callService.postData(body).subscribe((data) => {
          let serviceReturn = data.json();
          // console.log(JSON.stringify(serviceReturn));
          let statusService = serviceReturn['0'].serviceContent.status;
          if (statusService == "S") {
            showSwal('success', 'Tambah proyek', 'Proyek berhasil ditambahkan ')
            this.createProject.reset();
            this.ngOnInit();
          }
          else {
            showSwal('warning', 'Tambah proyek', serviceReturn['0'].serviceContent.errorMessage)
          }
        }, (err) => { showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ') })
      }
    });
  }
  tksindex: number;
  updateCounter: number = 0;
  updateProject(Code, pid, projectName, costumer) {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        this.tksindex = Code;
        this.updateCounter = 1;
        this.updateForm.get("pidedit").setValue(pid);
        this.updateForm.get("nameedit").setValue(projectName);
        this.updateForm.get("customeredit").setValue(costumer);
      }
    });
  }

  updateDataEdit(category, bill, mandys) {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        let body = {
          "updateUserId": this.userId,
          "id": this.tksindex,
          "pid": this.updateForm.controls['pidedit'].value,
          "projectName": this.updateForm.controls['nameedit'].value,
          "costumer": this.updateForm.controls['customeredit'].value,
          "category": category,
          "bill": bill,
          "mandays": mandys,
          "url": "updateProjectService"
        }
        // console.log(JSON.stringify(body));
        this.callService.putData(body).subscribe((data) => {
          let serviceReturn = data.json();
          let statusService = serviceReturn['0'].serviceContent.status;
          if (statusService == "S") {
            showSwal('success', 'Perbaharui proyek', 'Proyek berhasil diperbaharui ')
            this.tksindex = -1;
            this.ngOnInit();
          } else {
            showSwal('warning', 'Perbaharui proyek', serviceReturn['0'].serviceContent.errorMessage.toString())
          }
        }, (err) => { showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ') })
      }
    });
  }
  reverbEditData() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        this.tksindex = -1;
        this.updateCounter = 0;
      }
    });
  }
  listProject = [];
  getProject() {
    this.loadingData = true;
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        this.listProject = [];
        this.callService.getData('getProjectService?userId=' + this.userId).subscribe((data) => {
          let serviceReturn = data;
          // console.log(serviceReturn);
          let statusService = serviceReturn['0'].serviceContent.status;
          if (statusService == "S") {
            if (serviceReturn['0'].serviceContent.count != 0) {
              for (let i = 0; i < serviceReturn['0'].serviceContent.serviceOutput.length; i++) {
                this.listProject.push(serviceReturn['0'].serviceContent.serviceOutput[i]);
              }
            }
          }
        })

        this.loadingData= false;
      }
    });
  }
  activationProject(projectstatus, pId) {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        let statusProject = '';
        if (projectstatus == 'Y') {
          statusProject = 'N'
        } else if (projectstatus == 'N') {
          statusProject = 'Y'
        }
        let body = {
          "updateUserId": this.userId,
          "id": pId,
          "status": statusProject,
          "url": "activationProjectService"
        }
        this.callService.putData(body).subscribe((data) => {
          let serviceReturn = data.json();
          let statusService = serviceReturn['0'].serviceContent.status;
          if (statusService == "S") {
            if (statusProject == 'N') {
              showSwal('success', 'Nonaktif proyek', 'Proyek berhasil dinonaktifkan ')
            } else { showSwal('success', 'Aktif proyek', 'Proyek berhasil diaktifkan ') }

          } else {
            if (statusProject == 'N') {
              showSwal('warning', 'Nonaktif proyek', 'Proyek gagal dinonaktifkan ')
            } else {
              showSwal('warning', 'Aktif proyek', 'Proyek gagal diaktifkan ')
            }
          }
          this.ngOnInit();
        }, (err) => { showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ') })
      }
    });
  }
  loadingBar = "false"
  onchangeUpload(event) {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        this.formDataUpload = new FormData();
        let fileList: FileList = event.target.files;
        // console.log("file length : " + fileList.length);
        if (fileList.length > 0) {
          let file: File = fileList[0];

          // console.log("file name : " + file.name);
          // console.log("file type : " + file.type);
          // console.log("file size : " + file.size);
          this.formDataUpload.append('file', file);
          // this.readytoupload =true;
        }
      }
    });
  }

  uploadFileToActivity() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        this.loadingBar = "true";
        let serviceReturn: any;
        let statusService: string;
        let errorMsg: string;
        // this.reset();
        this.callService.getRestUpload(this.formDataUpload, "uploadProjectService?userId=" + this.userId).subscribe(data => {
          serviceReturn = data.json();;
          statusService = serviceReturn["0"].serviceContent.status;
          if (statusService == "S" && serviceReturn["0"].serviceContent.errorMessage.length > 0) {
            showSwal('success', 'Sukses Upload', serviceReturn["0"].serviceContent.errorMessage.toString());
            this.uploadFileForm.reset();
            this.loadingBar = "false";
          } else if (statusService == "S" && serviceReturn["0"].serviceContent.errorMessage.length == 0) {
            showSwal('success', 'Sukses Upload', 'Berhasil mengunggah data proyek');
            this.uploadFileForm.reset();
            this.loadingBar = "false";
          } else {
            showSwal('warning', 'Upload Failed', serviceReturn["0"].serviceContent.errorMessage.toString());
            this.uploadFileForm.reset();
            this.loadingBar = "false";
          }

         
        }, (err) => {
          // console.log(err);
          showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ')
          this.uploadFileForm.reset();
          this.loadingBar = "false";
         
        });
        this.ngOnInit();
      }
    });
  }
  today: any;
  userId: any;
  baseURL: any;
  loadingData: boolean;
  ngOnInit(): void {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        let date = new Date();
        let month = date.getMonth();
        let day = date.getDate();
        let hour = date.getHours();
        let minute = date.getMinutes();
        let secon = date.getSeconds();
        var sekarang = new Date(date.getFullYear(), month, day, hour, minute, secon);
        this.today = this.datePipe.transform(sekarang, 'EEEE, MMMM d, y');
        let Dta = location.href;
        let splitDta = Dta.split("/");
        this.baseURL = splitDta[0] + splitDta[1];
        var aesDecryptIV = this.decryptMe.aesDecryptIV(sessionStorage.getItem("ekrptd1"));
        var aesDecryptSalt = this.decryptMe.aesDecryptSalt(sessionStorage.getItem("ekrtdp2"));
        var aesDecrypt = this.decryptMe.aesDecrypt(sessionStorage.getItem("usdt"), aesDecryptIV, aesDecryptSalt);
        this.userId = JSON.parse(aesDecrypt).userId;
        this.getProject();
      }
    });
  }

}
