import { Pipe, PipeTransform, Injectable } from '@angular/core';
@Pipe({
    name: 'filterpipe'
})
export class Projectfilterpipe implements PipeTransform {
    transform(items: any[], projectId: string, projectName: string){


        if(projectId != "null" || projectName!="null" ){
             if (items && items.length){
            return items.filter(item =>{
                if (projectId.toLocaleLowerCase() && String(item.pid.toLocaleLowerCase()).indexOf(projectId.toLocaleLowerCase()) === -1){
                    if(projectId=="null"){
                        // return true;
                    }
                    else{
                        return false;
                    }
                } 
                if (projectName.toLocaleLowerCase()  && String(item.projectName.toLocaleLowerCase()).indexOf(projectName.toLocaleLowerCase()) === -1){
                    if(projectName=="null"){
                        // return true;
                    }else{
                        return false;    
                    }
                    
                }
                return true;
           })
        }
        else{
            return items;
        }
        }
        else{
           if (items && items.length){
            return items.filter(item =>{
                if (projectId && String(item.pid).indexOf(projectId) === -1){
                    
                    return true;
                } 
                 if (projectName && String(item.projectName).indexOf(projectName) === -1){
                    
                    return true;
                }
                return true;
           })
        }
        else{
            return items;
        }  
        }
       
    }
}