import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { CallServiceFunction } from '../../callServiceFunction';
import { AppDecryptMe } from "../../app.DecryptMe";
import { CekCredential } from '../../cekcredential';

declare var $: any;
declare function showSwal(type, title, text): any;

@Component({
  selector: 'app-holiday-calendar',
  templateUrl: './holiday-calendar.component.html',
  styleUrls: ['./holiday-calendar.component.css'],
  providers: [DatePipe]
})
export class HolidayCalendarComponent implements OnInit {

  constructor(private cekCredential: CekCredential, public router: Router, private datePipe: DatePipe, private callService: CallServiceFunction, public decryptMe: AppDecryptMe) { }

  p: number = 1;

  //Form
  updateHolidayForm = new FormGroup({
    period: new FormControl('', [Validators.required]),
    remark: new FormControl('', [Validators.required]),
  })

  addHolidayForm = new FormGroup({
    period: new FormControl('', [Validators.required]),
    remark: new FormControl('', [Validators.required]),
  })

  periodSearch = "null";
  holidayDetail: { date: string, remark: string, } = { date: '', remark: '', };
  holidayAdd: { date: string, remark: string, } = { date: '', remark: '', };

  //upload
  formDataUpload: FormData = new FormData();
  uploadFileForm = new FormGroup({
    UploadFile: new FormControl('', [
      Validators.required
    ])
  });

  baseURL: any;
  userId: any;
  today: any;
  loadingData : boolean;
  ngOnInit(): void {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {

        let Dta = location.href;
        let splitDta = Dta.split("/");
        this.baseURL = splitDta[0] + splitDta[1];

        //Search
        $('#datepicker-popup2').datepicker({
          format: "mm-yyyy",
          viewMode: "months",
          todayHighlight: true,
          autoclose: true,
          minViewMode: "months"
        });

        //Add
        $('#datepicker-add').datepicker({
          format: "dd-mm-yyyy",
          // viewMode: "months",
          todayHighlight: true,
          autoclose: true,
          // minViewMode: "months"
        });

        //Edit
        $('#datepicker-edit').datepicker({
          format: "dd-mm-yyyy",
          // viewMode: "months",
          autoclose: true,
          // minViewMode: "months"
        });

        let date = new Date();
        let month = date.getMonth();
        let day = date.getDate();
        let hour = date.getHours();
        let minute = date.getMinutes();
        let secon = date.getSeconds();
        var sekarang = new Date(date.getFullYear(), month);
        this.today = this.datePipe.transform(sekarang, 'yyyyMM');

        var aesDecryptIV = this.decryptMe.aesDecryptIV(sessionStorage.getItem("ekrptd1"));
        var aesDecryptSalt = this.decryptMe.aesDecryptSalt(sessionStorage.getItem("ekrtdp2"));
        var aesDecrypt = this.decryptMe.aesDecrypt(sessionStorage.getItem("usdt"), aesDecryptIV, aesDecryptSalt);
        this.userId = JSON.parse(aesDecrypt).userId;

        this.inquiryHoliday();
      }
    });
  }


  listHoliday = [];
  inquiryHoliday() {

    this.loadingData = true;
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        // console.log("kok masuk?");
        let serviceReturn: any;
        let statusService: string;

        let url = "getHolidayService";
        let params = "?userId=" + this.userId;

        //refresh array
        this.listHoliday = [];

        // console.log(url + params);
        this.callService.getData(url + params).subscribe((res) => {

          serviceReturn = res;
          // console.log(serviceReturn);
          statusService = serviceReturn["0"].serviceContent.status;
          // console.log("hasil status : " + statusService);

          if (statusService == "S") {
            if (serviceReturn["0"].serviceContent.count > 0) {
              for (let i = 0; i < serviceReturn["0"].serviceContent.serviceOutput.length; i++) {
                this.listHoliday.push(serviceReturn["0"].serviceContent.serviceOutput[i]);
              }

            }

          } else {

          }
        },
          (err) => { showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ') }
        );
        this.loadingData = false;
      }
    });
  }


  flagAdd = false;
  addHoliday() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        // console.log($('#periodAdd').first().val());
        // console.log(this.addHolidayForm.controls['remark'].value);
        this.flagAdd = false;
        let flag = false;

        if (!$('#periodAdd').first().val()) {
          flag = true;
          this.flagAdd = true;
        }

        if (this.addHolidayForm.controls['remark'].invalid) {
          flag = true;
        }

        if (!flag) {

          let period = $('#periodAdd').first().val().toString();
          let splitted = period.split("-", 3);
          let strPeriod = splitted[2] + splitted[1] + splitted[0];

          // let serviceReturn: any;
          let statusService: string;
          let errMess: string;

          let dataHoliday = {};
          dataHoliday["holidayDate"] = strPeriod;
          dataHoliday["remark"] = this.addHolidayForm.controls['remark'].value;

          let holidayList = [];
          holidayList.push(dataHoliday);

          let body = {
            "createUserId": this.userId,
            "holidayList": holidayList,
            "url": "createHolidayService"
          }

          // console.log(JSON.stringify(body));
          this.callService.postData(body).subscribe((data) => {
           let serviceReturn = data.json();
            // console.log(serviceReturn);
            statusService = serviceReturn['0'].serviceContent.status;

            if (statusService == "S") {
              showSwal('success', 'Success Create', 'Data libur berhasil ditambahkan');
            }
            else {
              showSwal('warning', 'Create Failed', 'Gagal menambah data libur');
            }

          })
        } else {
          showSwal('warning', 'Create Failed', 'Gagal menambah data libur');
        }

        this.ngOnInit();
      }
    });
  }

  updateHoliday() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        let period = $('#periodEdit').first().val().toString();
        let splitted = period.split("-", 3);
        let strPeriod = splitted[2] + splitted[1] + splitted[0];

        let body = {
          "id": this.idUpdate,
          "updateUserId": this.userId,
          "holidayDate": strPeriod,
          "remark": this.holidayDetail.remark,
          "url": "updateHolidayService"
        };

        let serviceReturn: any;
        let statusService: string;

        // console.log("json : " + JSON.stringify(body));
        this.callService.putData(body).subscribe((res) => {

          serviceReturn = res.json();
          statusService = serviceReturn["0"].serviceContent.status;

          // console.log("res :" + JSON.stringify(serviceReturn));
          // console.log("hasil status delete: " + statusService);

          if (statusService == "S") {
            showSwal('success', 'Success Update', 'Data libur berhasil diubah');
            this.ngOnInit();

          } else {
            showSwal('warning', 'Update Failed', 'Gagal mengubah data libur');
          }
        })

      }
    });
  }

  deleteHoliday() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        let body = {
          "deleteId": this.idDelete,
          "updateUserId": this.userId,
          "url": "deleteHolidayService"
        };

        let serviceReturn: any;
        let statusService: string;

        // console.log("json : " + JSON.stringify(body));

        this.callService.putData(body).subscribe((res) => {

          serviceReturn = res.json();
          statusService = serviceReturn["0"].serviceContent.status;

          // console.log("res :" + JSON.stringify(serviceReturn));
          // console.log("hasil status delete: " + statusService);

          if (statusService == "S") {
            showSwal('success', 'Success Delete', 'Data libur berhasil terhapus');
            this.ngOnInit();

          } else {
            showSwal('warning', 'Delete Failed', 'Gagal menghapus data libur');
          }
        })
      }
    });
  }

  //confirm update
  idUpdate: number;
  updateconfirmed(id: number) {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        //search data
        this.idUpdate = id;
        let dateUpdate: any;
        for (let i = 0; i < this.listHoliday.length; i++) {
          if (id == this.listHoliday[i].id) {
            dateUpdate = this.listHoliday[i].holidayDate;
            this.holidayDetail.remark = this.listHoliday[i].remark;
            break;
          }

        }

        // console.log(dateUpdate);

        let splitted = dateUpdate.split(" ", 4);
        let monthInt = 0;
        var months = [
          "Jan",
          "Feb",
          "Mar",
          "Apr",
          "May",
          "Jun",
          "Jul",
          "Aug",
          "Sep",
          "Oct",
          "Nov",
          "Dec"
        ];

        for (let i = 0; i < months.length; i++) {
          if (splitted[2] == months[i]) {
            monthInt = i;
            break;
          }
        }

        //set form
        $("#datepicker-edit").datepicker().datepicker("setDate", new Date(splitted[3], monthInt, splitted[1]));
      }
    });

  }





  //confirm delete
  idDelete: number;
  deleteconfirmed(id: number) {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        // console.log("id: " + id);
        this.idDelete = id;
      }
    });
  }

  //upload form
  onchangeUpload(event) {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        this.formDataUpload = new FormData();
        let fileList: FileList = event.target.files;
        // console.log("file length : " + fileList.length);
        if (fileList.length > 0) {
          let file: File = fileList[0];

          // console.log("file name : " + file.name);
          // console.log("file type : " + file.type);
          // console.log("file size : " + file.size);
          this.formDataUpload.append('file', file);
          // this.readytoupload =true;
        }
      }
    });
  }
  //reset form upload
  reset() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        this.uploadFileForm.reset();
      }
    });
  }


  //upload
  loadingBar = false;
  uploadFileToActivity() {
    this.loadingBar = true;
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        this.loadingData = true;
        let serviceReturn: any;
        let statusService: string;
        this.reset();
        this.callService.getRestUpload(this.formDataUpload, "uploadHolidayService?userId=" + this.userId).subscribe(data => {
          serviceReturn = data.json();;
          statusService = serviceReturn["0"].serviceContent.status;
          console.log(JSON.stringify(serviceReturn));
          if (statusService == "S" && serviceReturn["0"].serviceContent.errorMessage.length > 0) {

            showSwal('success', 'Success Upload', serviceReturn["0"].serviceContent.errorMessage.toString());
            this.uploadFileForm.reset();
          }else if(statusService == "S" && serviceReturn["0"].serviceContent.errorMessage.length == 0){
            showSwal('success', 'Success Upload', 'Berhasil mengunggah data hari libur ');
            this.uploadFileForm.reset();
          } else {
            showSwal('warning', 'Upload Failed', serviceReturn["0"].serviceContent.errorMessage.toString());
            this.uploadFileForm.reset();
          }
         
        }, (err) => {
          console.log(err);
          showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ')
         
        });

        this.loadingBar = false;
        this.ngOnInit();
      }
    });
  }

}
