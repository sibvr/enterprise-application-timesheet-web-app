import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { CallServiceFunction } from '../../callServiceFunction';
import { AppDecryptMe } from "../../app.DecryptMe";
import { CekCredential } from '../../cekcredential';

declare var $: any;
declare function showSwal(type, title, text): any;

@Component({
  selector: 'app-weekly-calendar',
  templateUrl: './weekly-calendar.component.html',
  styleUrls: ['./weekly-calendar.component.css'],
  providers: [DatePipe]
})
export class WeeklyCalendarComponent implements OnInit {

  constructor(private cekCredential: CekCredential, public router: Router, private datePipe: DatePipe, private callService: CallServiceFunction, public decryptMe: AppDecryptMe) { }

  weeklyUpdate: {
    plan1: string,
    plan2: string,
    month: string,
    weekMonth: string,
    year: string
  } = { plan1: '2', plan2: '2', month: '', weekMonth: '', year: '' };


  months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];

  //form
  updateWeeklyForm = new FormGroup({
    plan1: new FormControl('', [Validators.required]),
    plan2: new FormControl('', [Validators.required]),
    month: new FormControl('', [Validators.required]),
    weekMonth: new FormControl('', [Validators.required]),
    year: new FormControl('', [
      Validators.required,
      Validators.pattern("^[0-9]*$"),
      Validators.maxLength(4),])
  })

  p = 1;
  baseURL: any;
  userId: any;
  today: any;
  loadingData : boolean;
  ngOnInit(): void {
    
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {


        let Dta = location.href;
        let splitDta = Dta.split("/");
        this.baseURL = splitDta[0] + splitDta[1];

        //Search
        $('#datepicker-popup2').datepicker({
          format: "mm-yyyy",
          viewMode: "months",
          todayHighlight: true,
          autoclose: true,
          minViewMode: "months"
        });

        //Add
        $('#datepicker-add').datepicker({
          format: "dd-mm-yyyy",
          // viewMode: "months",
          todayHighlight: true,
          autoclose: true,
          // minViewMode: "months"
        });

        //Edit
        $('#datepicker-edit').datepicker({
          format: "dd-mm-yyyy",
          // viewMode: "months",
          autoclose: true,
          // minViewMode: "months"
        });

        let date = new Date();
        let month = date.getMonth();
        let day = date.getDate();
        let hour = date.getHours();
        let minute = date.getMinutes();
        let secon = date.getSeconds();
        var sekarang = new Date(date.getFullYear(), month);
        this.today = this.datePipe.transform(sekarang, 'yyyyMM');

        var aesDecryptIV = this.decryptMe.aesDecryptIV(sessionStorage.getItem("ekrptd1"));
        var aesDecryptSalt = this.decryptMe.aesDecryptSalt(sessionStorage.getItem("ekrtdp2"));
        var aesDecrypt = this.decryptMe.aesDecrypt(sessionStorage.getItem("usdt"), aesDecryptIV, aesDecryptSalt);
        this.userId = JSON.parse(aesDecrypt).userId;

        this.inquiryWeeklyCalendar();
      }
    });
  }


  listWeekly = [];
  inquiryWeeklyCalendar() {
    this.loadingData = true;
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        // console.log("kok masuk?");
        let serviceReturn: any;
        let statusService: string;

        let url = "getCalenderService";
        let params = "?userId=" + this.userId;

        //refresh array
        this.listWeekly = [];

        // console.log(url + params);
        this.callService.getData(url + params).subscribe((res) => {

          serviceReturn = res;
          console.log(serviceReturn);
          statusService = serviceReturn["0"].serviceContent.status;
          // console.log("hasil status : " + statusService);

          if (statusService == "S") {
            if (serviceReturn["0"].serviceContent.count > 0) {
              for (let i = 0; i < serviceReturn["0"].serviceContent.serviceOutput.length; i++) {
                this.listWeekly.push(serviceReturn["0"].serviceContent.serviceOutput[i]);

              }

            }

          } else {

          }
        },
          (err) => { showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ') }
        );

        this.loadingData = false;
      }
    });
  }


  //confirm update
  idUpdate: number;
  updateconfirmed(id: number) {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        //search data
        this.idUpdate = id;
        let dateUpdate: any;
        for (let i = 0; i < this.listWeekly.length; i++) {
          if (id == this.listWeekly[i].id) {

            //set value
            var splitted = this.listWeekly[i].plan.split("-", 2);
            console.log("splitted : " + splitted[0] + "-" + splitted[1]);

            this.weeklyUpdate.plan1 = splitted[0];
            this.weeklyUpdate.plan2 = splitted[1];

            // this.weeklyUpdate.plan2 = splitted[1].toString();
            console.log("month : " + this.listWeekly[i].month);
            this.weeklyUpdate.month = this.listWeekly[i].month;
            this.weeklyUpdate.weekMonth = this.listWeekly[i].weekMonth;
            this.weeklyUpdate.year = this.listWeekly[i].year;

            break;
          }

        }


      }
    });

  }

  updateWeekly() {

    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {


        let body = {  
          "updateUserId": this.userId,
          "id": this.idUpdate,
          "weekMonth": this.weeklyUpdate.weekMonth,
          "year": this.weeklyUpdate.year,
          "month": this.weeklyUpdate.month,
          "plan": this.weeklyUpdate.plan1+"-"+this.weeklyUpdate.plan2,
          "url": "updateCalenderService"
        };

        let serviceReturn: any;
        let statusService: string;

        console.log("json : " + JSON.stringify(body));
        this.callService.putData(body).subscribe((res) => {

          serviceReturn = res.json();
          statusService = serviceReturn["0"].serviceContent.status;


          if (statusService == "S") {
            showSwal('success', 'Success Update', 'Kalender mingguan berhasil diubah');
            this.ngOnInit();

          } else {
            showSwal('warning', 'Update Failed', 'Gagal mengubah kalender mingguan');
          }
        })

      }
    });

  }





}
