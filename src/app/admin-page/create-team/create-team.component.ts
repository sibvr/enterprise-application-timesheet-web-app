import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { CallServiceFunction } from '../../callServiceFunction';
import { AppDecryptMe } from "../../app.DecryptMe";
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray, FormControlName } from '@angular/forms';
declare function showSwal(type, title, text): any;
import { CekCredential } from '../../cekcredential';

@Component({
  selector: 'app-create-team',
  templateUrl: './create-team.component.html',
  styleUrls: ['./create-team.component.css'],
  providers: [DatePipe]
})
export class CreateTeamComponent implements OnInit {

  constructor(private cekCredential: CekCredential, public router: Router, private datePipe: DatePipe, private callService: CallServiceFunction, public decryptMe: AppDecryptMe) { }

  createTeam = new FormGroup({
    kodetim: new FormControl('', [
      Validators.required
    ]),
    namatim: new FormControl('', [
      Validators.required
    ]),
    managerid: new FormControl('', [
      Validators.required
    ])
  })
  updateForm = new FormGroup({
    codeTeam: new FormControl('', [Validators.required]),
    nameTeam: new FormControl('', [Validators.required])
  })
  newManager = [];
  getNewManager() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        this.newManager = [];
        this.callService.getData('getNewManagerService?userId=' + this.userId).subscribe((data) => {
          let serviceReturn = data;
          // console.log(serviceReturn);
          let statusService = serviceReturn['0'].serviceContent.status;
          if (statusService == "S") {
            if (serviceReturn['0'].serviceContent.count != 0) {
              for (let i = 0; i < serviceReturn['0'].serviceContent.serviceOutput.length; i++) {
                this.newManager.push(serviceReturn['0'].serviceContent.serviceOutput[i]);
              }
            }
          }
        })
      }
    });
  }
  listTeam = [];
  getTeam() {
    this.loadingData = true;
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        this.listTeam = [];
        this.callService.getData('getTeamService?userId=' + this.userId).subscribe((data) => {
          let serviceReturn = data;
          // console.log(serviceReturn);
          let statusService = serviceReturn['0'].serviceContent.status;
          if (statusService == "S") {
            if (serviceReturn['0'].serviceContent.count != 0) {
              for (let i = 0; i < serviceReturn['0'].serviceContent.serviceOutput.length; i++) {
                this.listTeam.push(serviceReturn['0'].serviceContent.serviceOutput[i]);
              }
            }
            this.getNewManager();
            this.loadingData = false;
          }
        })
      }
    });
  }
  createNewTeam() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        let kodetim = this.createTeam.controls["kodetim"].value;
        let namatim = this.createTeam.controls["namatim"].value;
        let managerid = this.createTeam.controls["managerid"].value;

        let body = {
          "createUserId": this.userId,
          "teamList": [
            {
              "teamCode": kodetim,
              "teamName": namatim,
              "managerId": managerid
            }
          ],
          "url": "createTeamService"
        }
        // console.log(JSON.stringify(body));
        this.callService.postData(body).subscribe((data) => {
          let serviceReturn = data.json();
          let statusService = serviceReturn['0'].serviceContent.status;
          let errorstatus = serviceReturn['0'].serviceContent.errorMessage;
          // console.log(JSON.stringify(errorstatus));
          // console.log(serviceReturn['0'].serviceContent.errorMessage[0]);
          if (statusService == "S") {
            showSwal('success', 'Tambah tim', 'Tim berhasil ditambahkan ');
            this.createTeam.reset();
            this.ngOnInit();
          }
          else {
            showSwal('warning', 'Tambah tim', errorstatus.toString());
          }

        }, (err) => { showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus') })
      }
    });
  }
  tksindex: any;
  updateCounter: number = 0;
  updateRemark(tksinde, teamCD, teamNM) {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        this.updateCounter = 1;
        this.tksindex = tksinde;
        this.updateForm.get("codeTeam").setValue(teamCD);
        this.updateForm.get("nameTeam").setValue(teamNM);
      }
    });
  }
  reverbUpdateData() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        this.tksindex = -1;
        this.updateCounter = 0;
      }
    });
  }

  updateTeam(idmanager, id) {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        let body = {
          "updateUserId": this.userId,
          "id": id,
          "teamCode": this.updateForm.controls['codeTeam'].value,
          "teamName": this.updateForm.controls['nameTeam'].value,
          "managerId": idmanager,
          "url": "updateTeamService"
        }
        this.callService.putData(body).subscribe((data) => {
          let serviceReturn = data.json();
          let statusService = serviceReturn['0'].serviceContent.status;
          if (statusService == "S") {
            showSwal('success', 'Perbaharui tim', 'Tim berhasil diperbaharui ');
            this.tksindex = -1;
            this.ngOnInit();
          } else {
            showSwal('warning', 'Perbaharui tim', serviceReturn['0'].serviceContent.errorMessage)
          }
        }, (err) => { showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus') })
      }
    });
  }
  today: any;
  userId: any;
  loadingData: boolean;
  ngOnInit(): void {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem("usdt");
        sessionStorage.removeItem("ekrptd1");
        sessionStorage.removeItem("ekrtdp2");
        this.router.navigateByUrl('/');
      } else {
        let date = new Date();
        let month = date.getMonth();
        let day = date.getDate();
        let hour = date.getHours();
        let minute = date.getMinutes();
        let secon = date.getSeconds();
        var sekarang = new Date(date.getFullYear(), month, day, hour, minute, secon);
        this.today = this.datePipe.transform(sekarang, 'EEEE, MMMM d, y');

        var aesDecryptIV = this.decryptMe.aesDecryptIV(sessionStorage.getItem("ekrptd1"));
        var aesDecryptSalt = this.decryptMe.aesDecryptSalt(sessionStorage.getItem("ekrtdp2"));
        var aesDecrypt = this.decryptMe.aesDecrypt(sessionStorage.getItem("usdt"), aesDecryptIV, aesDecryptSalt);
        this.userId = JSON.parse(aesDecrypt).userId;
        this.getTeam();
      }
    });
  }



}
