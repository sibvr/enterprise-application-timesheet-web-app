import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DatePipe } from '@angular/common';
import { CallServiceFunction } from '../callServiceFunction';
import { AppDecryptMe } from '../app.DecryptMe';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
  FormArray,
  FormControlName,
} from '@angular/forms';
import { CekCredential } from '../cekcredential';
import { Router } from '@angular/router';

declare var $: any;
declare function showSwal(type, title, text): any;

@Component({
  selector: 'app-daily-task',
  templateUrl: './daily-task.component.html',
  styleUrls: ['./daily-task.component.css'],
  providers: [DatePipe],
})
export class DailyTaskComponent implements OnInit {
  data: Array<any>;

  constructor(
    private cekCredential: CekCredential,
    public router: Router,
    private fb: FormBuilder,
    private datePipe: DatePipe,
    private callService: CallServiceFunction,
    public decryptMe: AppDecryptMe
  ) {
    this.data = new Array<any>();

    this.addFormCreate = this.fb.group({
      taskList: this.fb.array([]),
    });

    this.getSelected();
  }

  //==========Form WFH====================================================
  addFormCreate: FormGroup;
  workingstatus = new FormGroup({
    wfoRadio: new FormControl('', [Validators.required]),
    sehatRadio: new FormControl('', [Validators.required]),
    reasonst: new FormControl('', [Validators.required]),
    reaLeave: new FormControl('', [Validators.required]),
    reasonSick: new FormControl('', [Validators.required]),
  });

  cekStatusWFHorSehat() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        this.callService
          .getData('getWorkingService?userId=' + this.userId)
          .subscribe(
            (data) => {
              let serviceReturn = data;
              let statusService = serviceReturn['0'].serviceContent.status;
              if (statusService == 'S') {
                if (serviceReturn['0'].serviceContent.count == 0) {
                  if (
                    this.statusSehat == null &&
                    (this.roleName == 'STAFF' || this.roleName == 'ADMIN-STAFF')
                  ) {
                    document.getElementById('openModalButton').click();
                  }
                  this.inquiryTask();
                } else {
                  this.inquiryTask();
                }
              }
            },
            (err) => {
              showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ');
            }
          );
      }
    });
  }

  closesehat() {
    if (sessionStorage.getItem('corona') == null) {
      document.getElementById('openModalButtoncorona').click();
    }
  }
  listWorkingKesehatanConfig = ['SEHAT', 'SAKIT'];

  statusidsakit = 'false';
  statusonsite = 'false';
  statusLeave = 'false';
  // ifsakit() {
  //   this.statusidsakit = 'true';
  // }
  ifsehat() {
    if (
      this.workingstatus.controls['sehatRadio'].value == 'SAKIT' &&
      this.workingstatus.controls['wfoRadio'].value == 'Leave'
    ) {
      this.statusidsakit = 'true';
      this.statusLeave = 'false';
    } else if (this.workingstatus.controls['sehatRadio'].value == 'SAKIT') {
      this.statusidsakit = 'true';
    } else if (
      this.workingstatus.controls['wfoRadio'].value == 'Leave' &&
      this.workingstatus.controls['sehatRadio'].value == 'SEHAT'
    ) {
      this.statusidsakit = 'false';
      this.statusLeave = 'true';
    } else {
      this.statusidsakit = 'false';
    }
  }
  ifwfh() {
    console.log(this.workingstatus.controls['wfoRadio'].value);
    if (this.workingstatus.controls['wfoRadio'].value == 'Onsite') {
      this.statusonsite = 'true';
      this.statusLeave = 'false';
    } else if (
      this.workingstatus.controls['sehatRadio'].value == 'SAKIT' &&
      this.workingstatus.controls['wfoRadio'].value == 'Leave'
    ) {
      this.statusLeave = 'false';
      this.statusonsite = 'false';
    } else if (this.workingstatus.controls['wfoRadio'].value == 'Leave') {
      this.statusLeave = 'true';
      this.statusonsite = 'false';
    } else {
      this.statusLeave = 'false';
      this.statusonsite = 'false';
    }
  }

  listWorkingStatusConfig = [];
  getConfigWorking() {
    // console.log("masuk config working");
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        this.listWorkingStatusConfig = [];
        this.callService
          .getData(
            'getConfigWorkingService?userId=' + this.userId + '&parameter=Y'
          )
          .subscribe((data) => {
            let serviceReturn = data;
            let statusService = serviceReturn['0'].serviceContent.status;
            if (statusService == 'S') {
              // console.log("inquiry konfig");
              for (
                let i = 0;
                i < serviceReturn['0'].serviceContent.serviceOutput.length;
                i++
              ) {
                this.listWorkingStatusConfig.push(
                  serviceReturn['0'].serviceContent.serviceOutput[i]
                );
              }
              this.allTaskapi();
            }
          });
      }
    });
  }

  wfh: any;
  sehat: any;
  localstoragestatus = {};
  stringLocalStoragestatus: string;
  submitStatusSehat() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        if (this.workingstatus.controls['sehatRadio'].value != '') {
          if (this.workingstatus.controls['sehatRadio'].value == 'SAKIT') {
            this.sehat = this.workingstatus.controls['sehatRadio'].value
              .concat(' - ')
              .concat(this.workingstatus.controls['reasonSick'].value);
          } else {
            this.sehat = this.workingstatus.controls['sehatRadio'].value;
          }
        }
        if (this.workingstatus.controls['wfoRadio'].value != '') {
          if (this.workingstatus.controls['wfoRadio'].value == 'Onsite') {
            this.wfh = this.workingstatus.controls['wfoRadio'].value
              .concat(' - ')
              .concat(this.workingstatus.controls['reasonst'].value);
          } else if (
            this.workingstatus.controls['wfoRadio'].value == 'Leave' &&
            this.workingstatus.controls['sehatRadio'].value == 'SEHAT'
          ) {
            this.wfh = this.workingstatus.controls['wfoRadio'].value
              .concat(' - ')
              .concat(this.workingstatus.controls['reaLeave'].value);
          } else {
            this.wfh = this.workingstatus.controls['wfoRadio'].value;
          }
        }

        let body = {
          createUserId: this.userId,
          statusWFH: this.wfh,
          statusKesehatan: this.sehat,
          url: 'createWorkingService',
        };
        // console.log(JSON.stringify(body));
        this.callService.postData(body).subscribe(
          (data) => {
            let serviceReturn = data.json();
            // console.log(JSON.stringify(serviceReturn));
            let statusService = serviceReturn['0'].serviceContent.status;
            if (statusService == 'S') {
              this.localstoragestatus = {};

              this.localstoragestatus['statusWfh'] =
                serviceReturn['0'].serviceContent.serviceOutput.statusWFH;
              this.localstoragestatus['statusSehat'] =
                serviceReturn['0'].serviceContent.serviceOutput.statusKesehatan;
              this.stringLocalStoragestatus = JSON.stringify(
                this.localstoragestatus
              );
              sessionStorage.setItem('status', this.stringLocalStoragestatus);

              if (this.workingstatus.controls['wfoRadio'].value == 'Leave') {
                this.createLeave();
              } else {
                window.location.reload();
              }
            } else {
              showSwal(
                'warning',
                'status kerja',
                serviceReturn['0'].serviceContent.errorMessage.toString()
              );
              // alert("failed Sehat Status");
            }
          },
          (err) => {
            showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ');
          }
        );
      }
    });
  }

  createLeave() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        let reacuti = this.workingstatus.controls['reaLeave'].value;
        if (reacuti == '') {
          reacuti = '-';
        }
        let date = new Date();
        let month = date.getMonth();
        let day = date.getDate();
        var sekarang = new Date(date.getFullYear(), month, day);
        let todayisthedaydate = this.datePipe.transform(sekarang, 'yMMdd');
        // 20201110
        let body = {
          createUserId: this.userId,
          taskList: [
            {
              projectId: 0,
              userId: this.userId,
              workingHourPlan: 8,
              taskDetail: reacuti,
              startDate: todayisthedaydate,
            },
          ],
          url: 'createLeaveService',
        };
        console.log(body);
        this.callService.postData(body).subscribe(
          (data) => {
            let serviceReturn = data.json();
            let statusService = serviceReturn['0'].serviceContent.status;
            if (statusService == 'S') {
              window.location.reload();
            } else {
              showSwal(
                'warning',
                'status kerja',
                serviceReturn['0'].serviceContent.errorMessage.toString()
              );
            }
          },
          (err) => {
            showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ');
          }
        );
      }
    });
  }
  //=========End Form WFH====================================================

  // ==========Form Daily Task===============================================
  markasdoneForm = new FormGroup({
    actualHour: new FormControl('', [
      Validators.required,
      Validators.min(1),
      Validators.max(24),
    ]),
  });

  btnCreate = 'false';
  btnAdd = 'true';

  statusShow = 'noShow';
  formShow = 'show';

  showlist = 'true';

  task(): FormArray {
    return this.addFormCreate.get('taskList') as FormArray;
  }

  newQuantity(): FormGroup {
    return this.fb.group({
      projectId: ['', Validators.required],
      workingHourPlan: ['', Validators.required],
      userId: this.userId,
      startDate: new FormControl('', [Validators.required]),
      taskDetail: ['', Validators.required],
    });
  }

  addTask() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        this.showlist = 'true';
        this.btnCreate = 'true';
        this.btnAdd = 'false';
        this.task().push(this.newQuantity());
        this.getPID();
      }
    });
  }

  removeQuantity(i: number) {
    this.task().removeAt(i);
  }

  codeValue: string;
  codeList = [];
  idOfPid: any;
  saveCode(e): void {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        this.idOfPid = '';
        let name = e.target.value.split('-', 1)[0];
        let list = this.codeList.filter((x) => x.name === name)[0];

        if (list != null) {
          this.idOfPid = list.id;
        }
      }
    });
  }

  listuserid = [];
  toggleChecked(ev, userid) {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        let obj = {
          userD: userid,
        };
        if (ev.target.checked) {
          this.listuserid.push(obj);
        } else {
          let removeIndex = this.listuserid.findIndex(
            (itm) => itm.userD === userid
          );
          if (removeIndex !== -1) this.listuserid.splice(removeIndex, 1);
        }
      }
    });
  }

  nameManager: any;
  listStaffTeam = [];
  query: any;
  getteamAssignTo() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        this.callService
          .getData(
            'getTeamService?userId=' + this.userId + '&parameter=' + this.userId
          )
          .subscribe(
            (data) => {
              let serviceReturn = data;
              // // console.log(JSON.stringify(serviceReturn));
              let statusService = serviceReturn['0'].serviceContent.status;
              if (statusService == 'S') {
                this.listStaffTeam = [];
                this.nameManager =
                  serviceReturn['0'].serviceContent.teamData.managerId;
                for (
                  let i = 0;
                  i < serviceReturn['0'].serviceContent.serviceOutput.length;
                  i++
                ) {
                  this.listStaffTeam.push(
                    serviceReturn['0'].serviceContent.serviceOutput[i]
                  );
                }
                // console.log(this.listStaffTeam);
                this.query = '';
                // this.listStaffTeam=[];
                this.statusShow = 'noShow';
                this.formShow = 'show';
                // this.notification();
              }
            },
            (err) => {
              showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ');
            }
          );
      }
    });
  }
  // filtering checkbox
  // Getting Selected Games and Count
  selectedStaff = [];
  selectedCount = 0;
  getSelected() {
    this.selectedStaff = this.listStaffTeam.filter((s) => {
      return s.selected;
    });
    this.selectedCount = this.selectedStaff.length;
    //alert(this.selected_games);
  }

  // Clearing All Selections
  clearSelection() {
    this.query = '';
    this.listStaffTeam = this.listStaffTeam.filter((g) => {
      g.selected = false;
      // console.log(this.listStaffTeam);
      return true;
    });

    this.getSelected();
  }

  //Delete Single Listed Game Tag
  // deleteGame(id: number) {
  //   this.query = "";
  //   this.listStaffTeam = this.listStaffTeam.filter(g => {
  //     if (g.id == id)
  //       g.selected = false;

  //     return true;
  //   });
  //   this.getSelected();
  // }

  //Clear term types by user
  clearFilter() {
    this.query = '';
  }

  flagmoreThan24 = 'false';
  actionIfFalse() {
    // console.log(this.workingHourPlan);
    if (parseInt(this.workingHourPlan) > 24) {
      this.flagmoreThan24 = 'true';
      showSwal('warning', 'jam kerja', 'maksimal rencana jam kerja 24 jam');
      this.workingHourPlan = '';
    } else if (parseInt(this.workingHourPlan) < 1) {
      this.flagmoreThan24 = 'true';
      showSwal('warning', 'jam kerja', 'minimum rencana jam kerja 1 jam');
      this.workingHourPlan = '';
    } else {
      this.flagmoreThan24 = 'false';
    }
  }

  listDatainput = [];
  items = [];
  origItems = [];
  projName = [];
  getPID() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        this.codeList = [];
        this.listDatainput = [];
        this.items = [];
        this.origItems = [];
        this.projName = [];
        this.callService
          .getData('getProjectService?userId=' + this.userId + '&parameter=Y')
          .subscribe(
            (data) => {
              let serviceReturn = data;
              // console.log(JSON.stringify(serviceReturn));
              let statusService = serviceReturn['0'].serviceContent.status;
              if (statusService == 'S') {
                for (
                  let i = 0;
                  i < serviceReturn['0'].serviceContent.serviceOutput.length;
                  i++
                ) {
                  this.listDatainput.push(
                    serviceReturn['0'].serviceContent.serviceOutput[i]
                  );
                  this.items.push(this.listDatainput[i].pid);
                  this.origItems.push(this.listDatainput[i].pid);
                  this.projName.push(this.listDatainput[i].projectName);
                  // untuk filter PID
                  let obj = {
                    id: this.listDatainput[i].id,
                    name: this.listDatainput[i].pid,
                    pid: this.listDatainput[i].projectName,
                  };
                  this.codeList.push(obj);
                  // // console.log(this.codeList);

                  // untuk filter PID
                }
                this.showlist = 'true';
                if (
                  this.roleName == this.nameRoleManager ||
                  this.roleName == this.nameRoleSecHead ||
                  this.roleName == this.nameRoleAdvisor
                ) {
                  this.getteamAssignTo();
                }
                // this.notification();
                if (this.statViewTask == 'all') {
                  this.allTaskForm = 'true';
                  this.progressForm = 'false';
                  this.doneForm = 'false';
                } else if (this.statViewTask == 'progress') {
                  this.allTaskForm = 'false';
                  this.progressForm = 'true';
                  this.doneForm = 'false';
                } else if ((this.statViewTask = 'completed')) {
                  this.allTaskForm = 'false';
                  this.progressForm = 'false';
                  this.doneForm = 'true';
                }
                // this.addTask();
                this.statusShow = 'noShow';
                this.formShow = 'show';
              }
            },
            (err) => {
              showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ');
            }
          );
      }
    });
  }

  nameRoleStaff = 'STAFF';
  nameRoleManager = 'MANAGER';
  nameRoleAdmin = 'ADMIN-STAFF';
  nameRoleSecHead = 'SEC-HEAD';
  nameRoleAdvisor = 'ADVISOR';

  loadingBar = 'false';
  mandays: any;
  taskDetail: any;
  workingHourPlan: any;
  projectId: any;
  onSubmit() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else if (status == 'K' && this.flagmoreThan24 == 'false') {
        this.loadingBar = 'true';
        let taskList = [];
        let jsonTask = {};
        let starDate = $('#mandays').first().val();
        starDate =
          starDate.substring(0, 4) +
          starDate.substring(5, 7) +
          starDate.substring(8, 10);
        // // console.log(starDate);
        if (
          this.roleName == this.nameRoleStaff ||
          this.roleName == this.nameRoleAdmin
        ) {
          jsonTask['userId'] = this.userId;
          jsonTask['projectId'] = this.idOfPid;
          jsonTask['workingHourPlan'] = this.workingHourPlan;
          jsonTask['taskDetail'] = this.taskDetail;
          jsonTask['startDate'] = starDate;
          taskList.push(jsonTask);
        } else {
          for (let i = 0; i < this.listuserid.length; i++) {
            let jsonTaskM = {};
            jsonTaskM['userId'] = this.listuserid[i].userD;
            jsonTaskM['projectId'] = this.idOfPid;
            jsonTaskM['workingHourPlan'] = this.workingHourPlan;
            jsonTaskM['taskDetail'] = this.taskDetail;
            jsonTaskM['startDate'] = starDate;
            taskList.push(jsonTaskM);
          }
        }
        // // console.log(taskList);
        let body = {
          createUserId: this.userId,
          taskList: taskList,
          url: 'createDailyService',
        };
        console.log(JSON.stringify(body));
        this.callService.postData(body).subscribe(
          (data) => {
            let serviceReturn = data.json();
            let statusService = serviceReturn['0'].serviceContent.status;
            if (statusService == 'S') {
              this.btnCreate = 'false';
              this.addFormCreate = this.fb.group({
                taskList: this.fb.array([]),
              });

              this.workingHourPlan = '';
              this.taskDetail = '';
              this.projectId = '';
              jsonTask = {};
              this.idOfPid = '';
              this.codeValue = '';
              this.listuserid = [];
              this.codeList = [];
              $('#mandays').first().val('');

              this.clearSelection();
              showSwal(
                'success',
                'Tambah tugas',
                'Tugas berhasil ditambahkan '
              );
              this.loadingBar = 'false';
              this.inquiryTask();
            } else {
              showSwal(
                'warning',
                'Tambah tugas',
                serviceReturn['0'].serviceContent.errorMessage.toString()
              );
              this.loadingBar = 'false';
            }
          },
          (err) => {
            showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ');
          }
        );
      }
    });
  }

  //===========End Form Daily Task===========================================

  //===========Filter Date================================
  listData = [];
  listProses = [];
  listDone = [];

  allTaskForm = 'true';
  progressForm = 'false';
  doneForm = 'false';

  //filter date range
  public daterange: any = {};
  public options: any = {
    locale: { format: 'YYYY/MM/DD' },
    alwaysShowCalendars: false,
    showDropdowns: true,
    // startDate: "03/27/2021",
    // endDate: "02/19/2021",
    opens: 'left',
    drops: 'auto',
    buttonClasses: 'btn btn-lg',
    cancelClass: 'btn-danger',
  };
  public selectedDate(value: any, datepicker?: any) {
    console.log('selected date');
    // any object can be passed to the selected event and it will be passed back here
    datepicker.start = value.start;
    datepicker.end = value.end;

    console.log('val : ');
    console.log(datepicker.start);
    // use passed valuable to update state
    this.daterange.start = this.datePipe.transform(
      this.daterange.start,
      'yyyyMMdd'
    );
    this.daterange.end = this.datePipe.transform(
      this.daterange.end,
      'yyyyMMdd'
    );

    console.log('start : ' + this.daterange.start);
    console.log('end : ' + this.daterange.end);
  }
  public calendarCanceled(e: any) {
    // console.log(e);
    // e.event
    // e.picker
  }

  //inquiry Filter All Task
  public inquiryTaskFilter(e: any) {
    this.loadingData = true;
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        this.addFormCreate = this.fb.group({
          taskList: this.fb.array([]),
        });

        let dateRageFilter = this.daterange.start + '-' + this.daterange.end;
        console.log('dateRageFilter : ' + dateRageFilter);
        this.listData = [];
        this.listProses = [];
        this.listDone = [];

        console.log(
          'getDailyService?userId=' +
            this.userId +
            '&parameter=' +
            dateRageFilter
        );
        this.callService
          .getData(
            'getDailyService?userId=' +
              this.userId +
              '&parameter=' +
              dateRageFilter
          )
          .subscribe(
            (data) => {
              let serviceReturn = data;
              let statusService = serviceReturn['0'].serviceContent.status;
              if (
                statusService == 'S' &&
                serviceReturn['0'].serviceContent.count > 0
              ) {
                for (
                  let i = 0;
                  i < serviceReturn['0'].serviceContent.serviceOutput.length;
                  i++
                ) {
                  this.listData.push(
                    serviceReturn['0'].serviceContent.serviceOutput[i]
                  );
                  if (
                    serviceReturn['0'].serviceContent.serviceOutput[i].status ==
                    'Y'
                  ) {
                    this.listProses.push(
                      serviceReturn['0'].serviceContent.serviceOutput[i]
                    );
                  }
                  if (
                    serviceReturn['0'].serviceContent.serviceOutput[i].status ==
                    'D'
                  ) {
                    this.listDone.push(
                      serviceReturn['0'].serviceContent.serviceOutput[i]
                    );
                  }
                }

                // console.log("List Data : " + this.listData);
                this.addTask();
              } else {
                this.addTask();
              }
            },
            (err) => {
              showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ');
            }
          );

        if (this.statViewTask == 'all') {
          this.allTaskForm = 'true';
          this.progressForm = 'false';
          this.doneForm = 'false';
        } else if (this.statViewTask == 'progress') {
          this.allTaskForm = 'false';
          this.progressForm = 'true';
          this.doneForm = 'false';
        } else if ((this.statViewTask = 'completed')) {
          this.allTaskForm = 'false';
          this.progressForm = 'false';
          this.doneForm = 'true';
        }

        this.loadingData = false;
      }
    });
  }

  // Inquiry all task
  allTaskapi() {
    // console.log("masuk allTaskapi");
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        this.addFormCreate = this.fb.group({
          taskList: this.fb.array([]),
        });

        let today = new Date(new Date().getTime() + 5 * 24 * 60 * 60 * 1000);
        let last7day = new Date(
          new Date().getTime() - 30 * 24 * 60 * 60 * 1000
        );
        // console.log("today " + today);
        // console.log("last7day " + last7day);

        let dateRageToday =
          this.datePipe.transform(last7day, 'yyyyMMdd') +
          '-' +
          this.datePipe.transform(today, 'yyyyMMdd');
        console.log('dateRageToday : ' + dateRageToday);
        this.listData = [];
        this.listProses = [];
        this.listDone = [];
        this.callService
          .getData(
            'getDailyService?userId=' +
              this.userId +
              '&parameter=' +
              dateRageToday
          )
          .subscribe(
            (data) => {
              let serviceReturn = data;
              // console.log(JSON.stringify(serviceReturn));
              let statusService = serviceReturn['0'].serviceContent.status;
              if (
                statusService == 'S' &&
                serviceReturn['0'].serviceContent.count > 0
              ) {
                for (
                  let i = 0;
                  i < serviceReturn['0'].serviceContent.serviceOutput.length;
                  i++
                ) {
                  this.listData.push(
                    serviceReturn['0'].serviceContent.serviceOutput[i]
                  );
                  if (
                    serviceReturn['0'].serviceContent.serviceOutput[i].status ==
                    'Y'
                  ) {
                    this.listProses.push(
                      serviceReturn['0'].serviceContent.serviceOutput[i]
                    );
                  }
                  if (
                    serviceReturn['0'].serviceContent.serviceOutput[i].status ==
                    'D'
                  ) {
                    this.listDone.push(
                      serviceReturn['0'].serviceContent.serviceOutput[i]
                    );
                  }
                }

                // console.log("List Data : " + this.listData);
                this.addTask();
              } else {
                this.addTask();
              }
            },
            (err) => {
              showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ');
            }
          );
        this.allTaskForm = 'true';
        this.progressForm = 'false';
        this.doneForm = 'false';
        this.loadingData = false;
      }
    });
  }
  //===========End Filter Date============================

  //===========Actual Working Hour========================
  updateCounter: number = 0;

  detailid: any;
  detailpid: any;
  detailprojectName: any;
  detailworkingHourPlan: any;
  detailtaskDetail: any;
  detailstatus: any;
  detailworkingHourActual: any;
  todayistheday: any;
  todaystart: any;
  markasdn: any;
  detailsAll(
    id,
    pid,
    projectName,
    workingHourPlan,
    workingHourActual,
    taskDetail,
    statustask,
    start
  ) {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        let date = new Date();
        let month = date.getMonth();
        let day = date.getDate();
        var sekarang = new Date(date.getFullYear(), month, day);
        let todayisthedaydate = this.datePipe.transform(sekarang, 'yMMdd');
        this.todayistheday = new Date(sekarang);
        let dt = new Date(start);
        let month1 = dt.getMonth();
        let day1 = dt.getDate();

        var startdtd = new Date(dt.getFullYear(), month1, day1);
        this.todaystart = new Date(startdtd);
        // console.log(startdtd, " ", sekarang);
        let same = sekarang.getTime() === startdtd.getTime();
        if (same) {
          this.markasdn = 'sama';
        } else if (startdtd <= sekarang) {
          this.markasdn = 'sama';
        } else if (startdtd > sekarang) {
          this.markasdn = 'beda';
        }
        // console.log(this.markasdn);

        this.markasdoneForm.reset();
        this.updateCounter = 0;
        this.detailid = id;
        this.detailpid = pid;
        this.detailprojectName = projectName;
        this.detailworkingHourPlan = workingHourPlan;
        this.detailtaskDetail = taskDetail;
        this.detailstatus = statustask;
        this.detailworkingHourActual = workingHourActual;
      }
    });
  }

  idOfPidupdate: any;
  saveCodeupdate(ev, projectid) {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        this.idOfPidupdate = '';
        if (ev.target.value == '') {
          this.idOfPidupdate = 'pidkosong';
        } else {
          console.log(ev.target.value);
          let name = ev.target.value.split('-', 1)[0];
          console.log(name);
          let list = this.codeList.filter((x) => x.name === name)[0];
          if (list == null) {
            this.idOfPidupdate = 'pidtidaksesuai';
          } else if (list != null || list != '') {
            this.idOfPidupdate = list.id;
          }
        }

        console.log(this.idOfPidupdate);
      }
    });
  }

  detail = '';
  submitUpdate(id, startdt) {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        let date = new Date(startdt);
        let month = date.getMonth();
        let day = date.getDate();
        let hour = date.getHours();
        let minute = date.getMinutes();
        let secon = date.getSeconds();
        var sekarang = new Date(date.getFullYear(), month, day);
        let strdate = this.datePipe.transform(sekarang, 'yMMdd');
        console.log(this.idOfPidupdate);
        // if (this.idOfPidupdate == "" || this.idOfPidupdate == null) {
        //   this.idOfPidupdate = this.pidUpdate;
        // } else
        if (
          this.idOfPidupdate == 'pidtidaksesuai' ||
          this.idOfPidupdate == 'pidkosong'
        ) {
          showSwal('warning', ' perbaharui tugas', 'Proyek ID tidak sesuai');
        } else {
          console.log(this.detail);
          if (this.detail == '') {
            this.detail = this.dtltsk;
          } else {
            this.detail = (<HTMLInputElement>(
              document.getElementById('myTextarea')
            )).value;
          }

          let body = {
            updateUserId: this.userId,
            id: id,
            projectId: this.idOfPidupdate,
            taskDetail: this.detail,
            startDate: strdate,
            url: 'updateDailyService',
          };
          console.log(JSON.stringify(body));
          this.callService.putData(body).subscribe(
            (data) => {
              let serviceReturn = data.json();
              let statusService = serviceReturn['0'].serviceContent.status;
              if (statusService == 'S') {
                showSwal(
                  'success',
                  ' Perbaharui tugas',
                  'Tugas berhasil diperbaharui '
                );
                this.tksindex = -1;
                this.codeValueupdate = '';
                this.detail = '';
                this.inquiryTask();
              } else {
                showSwal(
                  'warning',
                  'Perbaharui tugas',
                  serviceReturn['0'].serviceContent.errorMessage
                );
                // this.codeValueupdate = "";
                // this.detail="";
              }
            },
            (err) => {
              showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ');
            }
          );
        }
      }
    });
  }

  codeValueupdate = '';
  tksindex: any;
  dtltsk: any;
  updateRemark(tksinde, projectId, projectname, detailtask) {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        this.updateCounter = 1;
        this.tksindex = tksinde;
        this.codeValueupdate = projectname;
        this.idOfPidupdate = projectId;
        this.dtltsk = detailtask;
        // console.log(this.pidUpdate);
      }
    });
  }
  remark(e) {
    console.log(e.target.value);
    this.dtltsk = e.target.value;
  }
  reverbUpdateData() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        this.tksindex = -1;
        this.updateCounter = 0;
      }
    });
  }
  markAsDone() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        let body = {
          updateUserId: this.userId,
          id: this.detailid,
          workingHourActual: this.markasdoneForm.controls['actualHour'].value,
          taskDetail: this.detailtaskDetail,
          url: 'doneDailyService',
        };
        console.log(JSON.stringify(body));
        this.callService.putData(body).subscribe(
          (data) => {
            let serviceReturn = data.json();
            let statusService = serviceReturn['0'].serviceContent.status;
            if (statusService == 'S') {
              showSwal(
                'success',
                'Tugas selesai',
                'Tugas berhasil diselesaikan '
              );
              this.inquiryTask();
            } else {
              showSwal(
                'warning',
                'Tugas gagal',
                serviceReturn['0'].serviceContent.errorMessage
              );
            }
          },
          (err) => {
            showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ');
          }
        );
      }
    });
  }

  deleteid = '';
  beforedeleteTask(deleteId) {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        this.deleteid = deleteId;
      }
    });
  }
  deleteTask(deleteidafter) {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        let body = {
          updateUserId: this.userId,
          deleteId: deleteidafter,
          url: 'deleteDailyService',
        };

        this.callService.putData(body).subscribe(
          (data) => {
            let serviceReturn = data.json();
            let statusService = serviceReturn['0'].serviceContent.status;
            if (statusService == 'S') {
              showSwal('success', 'Hapus tugas', 'Tugas berhasil dihapus ');
              this.inquiryTask();
            } else {
              showSwal(
                'warning',
                'Hapus tugas',
                serviceReturn['0'].serviceContent.errorMessage
              );
            }
          },
          (err) => {
            showSwal('connection', 'Koneksi bermasalah', 'Koneksi terputus ');
          }
        );
      }
    });
  }
  //===========Close Atual Working Hour==================

  //===========Tab View==================================
  //View All Task
  statViewTask = 'all';
  allTask() {
    this.statViewTask = 'all';
    this.loadingData = true;
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        this.addFormCreate = this.fb.group({
          taskList: this.fb.array([]),
        });
        // this.listStaffTeam = [];
        this.task().push(this.newQuantity());
        this.getPID();
        this.allTaskForm = 'true';
        this.progressForm = 'false';
        this.doneForm = 'false';

        this.loadingData = false;
      }
    });
  }

  //View Progress Task
  inprogress() {
    this.statViewTask = 'progress';
    this.loadingData = true;
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        this.addFormCreate = this.fb.group({
          taskList: this.fb.array([]),
        });
        // this.listStaffTeam = [];
        this.task().push(this.newQuantity());
        this.getPID();
        this.allTaskForm = 'false';
        this.progressForm = 'true';
        this.doneForm = 'false';

        this.loadingData = false;
      }
    });
  }

  //View Compelete Task
  completed() {
    this.statViewTask = 'completed';
    this.loadingData = true;
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        this.addFormCreate = this.fb.group({
          taskList: this.fb.array([]),
        });
        // this.listStaffTeam = [];
        this.task().push(this.newQuantity());
        this.getPID();
        this.allTaskForm = 'false';
        this.progressForm = 'false';
        this.doneForm = 'true';

        this.loadingData = false;
      }
    });
  }
  //===========Close Tab View============================

  inquiryTask() {
    this.cekCredential.getCredential((status) => {
      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        if (
          this.roleName != this.nameRoleManager ||
          this.roleName != this.nameRoleSecHead
        ) {
          if (
            sessionStorage.getItem('corona') == null &&
            sessionStorage.getItem('status') != null
          ) {
            document.getElementById('openModalButtoncorona').click();
          }
        }

        this.codeList = [];
        this.showlist = 'true';

        let date = new Date();
        let month = date.getMonth();
        let day = date.getDate();
        let hour = date.getHours();
        let minute = date.getMinutes();
        let secon = date.getSeconds();
        var sekarang = new Date(
          date.getFullYear(),
          month,
          day,
          hour,
          minute,
          secon
        );
        this.today = this.datePipe.transform(sekarang, 'EEEE, MMMM d, y');
        this.updateCounter = 0;
        this.tksindex = -1;
        this.getConfigWorking();
      }
    });
  }

  closemodal() {
    sessionStorage.setItem('corona', 'setCorona');
  }

  today: any;
  userId: any;
  roleName: any;
  statusSehat: any;
  statusWfh: any;
  loadingData = true;
  p = 1;
  ngOnInit(): void {
    this.cekCredential.getCredential((status) => {
      this.loadingData = true;

      if (status == 'R') {
        sessionStorage.removeItem('usdt');
        sessionStorage.removeItem('ekrptd1');
        sessionStorage.removeItem('ekrtdp2');
        this.router.navigateByUrl('/');
      } else {
        this.statViewTask = 'all';
        this.statusShow = 'show';
        this.formShow = 'noShow';
        let nowDate = new Date();
        nowDate.setDate(nowDate.getDate() - 5);
        $('#datepicker-popup1').datepicker({
          enableOnReadonly: true,
          todayHighlight: true,
          format: 'yyyy-mm-dd',
          startDate: new Date(),
          autoclose: true,
        });
        $('#datepicker-popup').datepicker({
          enableOnReadonly: true,
          todayHighlight: true,
          startDate: nowDate,
          format: 'yyyy-mm-dd',
          autoclose: true,
        });

        if (sessionStorage.getItem('status') != null) {
          var berhasilIV = sessionStorage.getItem('status');
          this.statusSehat = JSON.parse(berhasilIV).statusSehat;
          this.statusWfh = JSON.parse(berhasilIV).statusWfh;
        }

        var aesDecryptIV = this.decryptMe.aesDecryptIV(
          sessionStorage.getItem('ekrptd1')
        );
        var aesDecryptSalt = this.decryptMe.aesDecryptSalt(
          sessionStorage.getItem('ekrtdp2')
        );
        var aesDecrypt = this.decryptMe.aesDecrypt(
          sessionStorage.getItem('usdt'),
          aesDecryptIV,
          aesDecryptSalt
        );
        this.userId = JSON.parse(aesDecrypt).userId;
        this.roleName = JSON.parse(aesDecrypt).roleNameResult;

        if (
          this.roleName != this.nameRoleManager ||
          this.roleName != this.nameRoleSecHead
        ) {
          this.cekStatusWFHorSehat();
        } else {
          this.inquiryTask();
        }

        let date = new Date();
        let month = date.getMonth();
        let day = date.getDate();
        var sekarang = new Date(date.getFullYear(), month, day);

        //date range
        this.daterange.start = this.datePipe.transform(sekarang, 'yyyyMMdd');
        this.daterange.end = this.datePipe.transform(sekarang, 'yyyyMMdd');
      }
    });
  }
}
