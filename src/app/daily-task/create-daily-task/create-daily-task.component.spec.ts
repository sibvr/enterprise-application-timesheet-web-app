import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CreateDailyTaskComponent } from './create-daily-task.component';

describe('CreateDailyTaskComponent', () => {
  let component: CreateDailyTaskComponent;
  let fixture: ComponentFixture<CreateDailyTaskComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateDailyTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateDailyTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
