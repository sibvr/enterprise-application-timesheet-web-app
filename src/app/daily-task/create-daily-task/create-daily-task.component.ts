import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DatePipe } from '@angular/common';
import { CallServiceFunction } from '../../callServiceFunction';
import { FormBuilder, FormControl, FormGroup, Validators, FormArray } from '@angular/forms';
declare var $: any;
@Component({
  selector: 'app-create-daily-task',
  templateUrl: './create-daily-task.component.html',
  styleUrls: ['./create-daily-task.component.css'],
  providers: [DatePipe]
})
export class CreateDailyTaskComponent implements OnInit {

  filteredArray = ['AB', 'SD', 'DF', 'FG'];
  inputValue = "";
  name = 'Angular 4';
  myDropDown: string;
  items = [];
  origItems = [];
  listItems = [];
  origProj = [];
  listDatainput = [];
  @ViewChild('selectList') selectList: ElementRef;

  constructor(private fb: FormBuilder, private datePipe: DatePipe, private callService: CallServiceFunction) {

  }
  public codeValue: string;

  codeList = [];

  idOfPid: any;
  public saveCode(e): void {
    let name = e.target.value.split(" ", 1)[0];
    let list = this.codeList.filter(x => x.name === name)[0];
    this.idOfPid = list.id;
  }
  click() {
    // console.log(this.idOfPid);
  }

  onChangeofOptions(newGov) {
    // console.log(newGov);
  }

  filterItem(event) {
    // console.log(event);
    if (!event) {
      this.items = this.origItems;
    } // when nothing has typed*/   
    if (typeof event === 'string') {
      this.items = this.origItems.filter(a => a.toLowerCase()
        .startsWith(event.toLowerCase()));
    }
    this.selectList.nativeElement.size = this.items.length + 1;
  }
  projName = [];
  getPID() {
    this.listDatainput = [];
    this.listItems = [];
    this.items = [];
    this.origItems = [];
    this.projName = [];
    this.callService.getData('getProjectService?userId=85').subscribe((data) => {
      let serviceReturn = data;
      // console.log(serviceReturn);
      let statusService = serviceReturn['0'].serviceContent.status;
      if (statusService == "S") {
        for (let i = 0; i < serviceReturn['0'].serviceContent.serviceOutput.length; i++) {
          this.listDatainput.push(serviceReturn['0'].serviceContent.serviceOutput[i]);
          this.items.push(this.listDatainput[i].pid);
          this.origItems.push(this.listDatainput[i].pid);
          this.projName.push(this.listDatainput[i].projectName);
          let obj = {
            "id": this.listDatainput[i].id,
            "name": this.listDatainput[i].pid,
            "pid": this.listDatainput[i].projectName

          }
          this.codeList.push(obj);
        }
      }
    })

  }
  ngOnInit(): void {
    this.getPID();
  }

}
